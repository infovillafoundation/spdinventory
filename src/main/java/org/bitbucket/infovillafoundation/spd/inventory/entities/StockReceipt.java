package org.bitbucket.infovillafoundation.spd.inventory.entities;

import org.bitbucket.infovillafoundation.spd.inventory.converters.StockReceiptStatusConverter;
import org.bitbucket.infovillafoundation.spd.inventory.models.StockReceiptStatus;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Sandah Aung on 15/12/14.
 */

@Entity
@Table(name = "receipts")
public class StockReceipt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "received_date")
    @Temporal(TemporalType.DATE)
    private Date receivedDate;

    @Column(name = "particular")
    private String particular;

    @Column(name = "quantity")
    private double receivedQuantity;

    @Column(name = "value")
    private double receivedValue;

    @Column(name = "opening_bought_status")
    @Convert(converter = StockReceiptStatusConverter.class)
    private StockReceiptStatus stockReceiptStatus;

    public StockReceipt() {
        stockReceiptStatus = StockReceiptStatus.BOUGHT;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public String getParticular() {
        return particular;
    }

    public void setParticular(String particular) {
        this.particular = particular;
    }

    public double getReceivedQuantity() {
        return receivedQuantity;
    }

    public void setReceivedQuantity(double receivedQuantity) {
        this.receivedQuantity = receivedQuantity;
    }

    public double getReceivedValue() {
        return receivedValue;
    }

    public void setReceivedValue(double receivedValue) {
        this.receivedValue = receivedValue;
    }

    public StockReceiptStatus getStockReceiptStatus() {
        return stockReceiptStatus;
    }

    public void setStockReceiptStatus(StockReceiptStatus stockReceiptStatus) {
        this.stockReceiptStatus = stockReceiptStatus;
    }
}
