package org.bitbucket.infovillafoundation.spd.inventory.models;


import io.datafx.controller.injection.scopes.FlowScoped;
import org.bitbucket.infovillafoundation.spd.inventory.entities.Stock;

/**
 * Created by Sandah Aung on 26/12/14.
 */


@FlowScoped
public class StockContainer {
    private Stock stock;

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }
}
