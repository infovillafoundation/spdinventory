package org.bitbucket.infovillafoundation.spd.inventory.converters;

import org.bitbucket.infovillafoundation.spd.inventory.models.StockReceiptStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by Sandah Aung on 1/1/15.
 */

@Converter
public class StockReceiptStatusConverter implements AttributeConverter<StockReceiptStatus, String> {
    @Override
    public String convertToDatabaseColumn(StockReceiptStatus attribute) {
        if (attribute == StockReceiptStatus.OPENING)
            return "opening";
        else
            return "bought";
    }

    @Override
    public StockReceiptStatus convertToEntityAttribute(String dbData) {
        if (dbData.equals("opening"))
            return StockReceiptStatus.OPENING;
        else
            return StockReceiptStatus.BOUGHT;
    }
}
