package org.bitbucket.infovillafoundation.spd.inventory.daos;

import org.bitbucket.infovillafoundation.spd.inventory.entities.Stock;
import org.bitbucket.infovillafoundation.spd.inventory.entities.StockReceipt;

import javax.persistence.EntityManager;

/**
 * Created by Sandah Aung on 17/12/14.
 */
public class StockReceiptDao extends Dao {

    public static void saveStockReceipt(StockReceipt stockReceipt) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stockReceipt);
        em.getTransaction().commit();
        em.close();
    }

    public static void saveStockWithStockReceipt(Stock stock, StockReceipt stockReceipt) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stockReceipt);
        em.merge(stock);
        em.getTransaction().commit();
        em.close();
    }

    public static void updateStockReceipt(StockReceipt stockReceipt) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(stockReceipt);
        em.getTransaction().commit();
        em.close();
    }

    public static void updateStockWithStockReceipt(Stock stock, StockReceipt stockReceipt) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(stockReceipt);
        em.merge(stock);
        em.getTransaction().commit();
        em.close();
    }

    public static void deleteStockReceipt(StockReceipt stockReceipt) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.contains(stockReceipt) ? stockReceipt : em.merge(stockReceipt));
        em.getTransaction().commit();
        em.close();
    }

    public static void deleteStockReceiptWithStock(StockReceipt stockReceipt, Stock stock) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.contains(stockReceipt) ? stockReceipt : em.merge(stockReceipt));
        em.merge(stock);
        em.getTransaction().commit();
        em.close();
    }
}
