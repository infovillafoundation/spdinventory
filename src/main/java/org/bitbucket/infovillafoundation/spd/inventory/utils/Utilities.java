package org.bitbucket.infovillafoundation.spd.inventory.utils;

import org.bitbucket.infovillafoundation.spd.inventory.entities.Stock;
import org.bitbucket.infovillafoundation.spd.inventory.models.RepeatableItemWithStock;
import org.bitbucket.infovillafoundation.spd.inventory.models.UniqueItemWithStock;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 21/1/15.
 */
public class Utilities {

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy");

    public static List<RepeatableItemWithStock> fromItemWithStockList(List<UniqueItemWithStock> uniqueItemWithStocks) {
        List<RepeatableItemWithStock> repeatableItemWithStocks = new ArrayList<>();
        for (UniqueItemWithStock uniqueItemWithStock : uniqueItemWithStocks) {
            List<Stock> stocks = uniqueItemWithStock.getItem().getStocks();
            for (Stock stock : stocks) {
                RepeatableItemWithStock repeatableItemWithStock = new RepeatableItemWithStock();
                repeatableItemWithStock.setStock(stock);
                repeatableItemWithStocks.add(repeatableItemWithStock);
            }
        }
        return repeatableItemWithStocks;
    }

    public static String fromDate(Date date) {
        return simpleDateFormat.format(date);
    }

    public static LocalDate toLocalDateFromUtilDate(Date date) {
        Instant instant = Instant.ofEpochMilli(date.getTime());
        LocalDate localDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
        return localDate;
    }

    public static Date toUtilDateFromLocalDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static int toYearMonthFromDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int yearMonth = year * 13 + month + 1;
        return yearMonth;
    }
}
