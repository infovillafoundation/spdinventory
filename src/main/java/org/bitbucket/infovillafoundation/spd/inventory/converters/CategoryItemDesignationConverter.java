package org.bitbucket.infovillafoundation.spd.inventory.converters;

import org.bitbucket.infovillafoundation.spd.inventory.models.CategoryItemDesignation;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by Sandah Aung on 1/1/15.
 */

@Converter
public class CategoryItemDesignationConverter implements AttributeConverter<CategoryItemDesignation, String> {
    @Override
    public String convertToDatabaseColumn(CategoryItemDesignation attribute) {
        if (attribute == CategoryItemDesignation.NO)
            return "no";
        else
            return "yes";
    }

    @Override
    public CategoryItemDesignation convertToEntityAttribute(String dbData) {
        if (dbData.equals("no"))
            return CategoryItemDesignation.NO;
        else
            return CategoryItemDesignation.YES;
    }
}
