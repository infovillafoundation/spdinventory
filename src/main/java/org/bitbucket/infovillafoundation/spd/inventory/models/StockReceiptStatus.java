package org.bitbucket.infovillafoundation.spd.inventory.models;

/**
 * Created by Sandah Aung on 1/1/15.
 */
public enum StockReceiptStatus {
    OPENING, BOUGHT;
}
