package org.bitbucket.infovillafoundation.spd.inventory.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Sandah Aung on 15/12/14.
 */

@NamedQueries({
        @NamedQuery(name = "MonthlyAmountReport.findAll",
                query = "SELECT e FROM MonthlyAmountReport e"),
        @NamedQuery(name = "MonthlyAmountReport.findByYearAndMonthInMonths",
                query = "SELECT e FROM MonthlyAmountReport e  WHERE e.yearAndMonthInMonths = :yearAndMonthInMonths"),
        @NamedQuery(name = "MonthlyAmountReport.findByReportDate",
                query = "SELECT e FROM MonthlyAmountReport e  WHERE e.reportDate = :reportDate"),
        @NamedQuery(name = "MonthlyAmountReport.findByItemAndYearAndMonthInMonths",
                query = "SELECT e FROM MonthlyAmountReport e  WHERE e.item = :item AND e.yearAndMonthInMonths = :yearAndMonthInMonths")
})

@Entity
@Table(name = "monthly_amount_reports")
public class MonthlyAmountReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "report_date")
    @Temporal(TemporalType.DATE)
    private Date reportDate;

    @Column(name = "ym")
    private int yearAndMonthInMonths;

    @Column(name = "opening_balance")
    private double openingBalance;

    @Column(name = "buy")
    private double amountBought;

    @Column(name = "draw")
    private double amountDrawn;

    @Column(name = "closing_balance")
    private double closingBalance;

    @OneToOne
    @JoinColumn(name = "item_id")
    private Item item;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public int getYearAndMonthInMonths() {
        return yearAndMonthInMonths;
    }

    public void setYearAndMonthInMonths(int yearAndMonthInMonths) {
        this.yearAndMonthInMonths = yearAndMonthInMonths;
    }

    public double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(double openingBalance) {
        this.openingBalance = openingBalance;
    }

    public double getAmountBought() {
        return amountBought;
    }

    public void setAmountBought(double amountBought) {
        this.amountBought = amountBought;
    }

    public double getAmountDrawn() {
        return amountDrawn;
    }

    public void setAmountDrawn(double amountDrawn) {
        this.amountDrawn = amountDrawn;
    }

    public double getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(double closingBalance) {
        this.closingBalance = closingBalance;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
