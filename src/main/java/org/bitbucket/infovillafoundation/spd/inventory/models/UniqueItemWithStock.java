package org.bitbucket.infovillafoundation.spd.inventory.models;

import org.bitbucket.infovillafoundation.spd.inventory.entities.Item;
import org.bitbucket.infovillafoundation.spd.inventory.utils.Utilities;

/**
 * Created by Sandah Aung on 20/12/14.
 */
public class UniqueItemWithStock extends ItemWithStock {
    private Item item;

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public String getName() {
        if (item == null || item.getActiveStock() == null)
            return null;
        return item.getName();
    }

    public String getPrice() {
        if (item == null || item.getActiveStock() == null)
            return null;
        return item.getActiveStock().getPriceOfReceivedStock() + "";
    }

    public String getReceivedDate() {
        if (item == null || item.getActiveStock() == null)
            return null;
        return Utilities.fromDate(item.getActiveStock().getReceivedDate());
    }

    public String getReceivedQuantity() {
        if (item == null || item.getActiveStock() == null)
            return null;
        return item.getActiveStock().getQuantityOfReceivedStock() + " " + item.getActiveStock().getUnit();
    }

    public String getReceivedValue() {
        if (item == null || item.getActiveStock() == null)
            return null;
        return item.getActiveStock().getValueOfReceivedStock() + "";
    }

    public String getIssuedQuantity() {
        if (item == null || item.getActiveStock() == null)
            return null;
        return item.getActiveStock().getQuantityOfIssuedStock() + " " + item.getActiveStock().getUnit();
    }

    public String getIssuedValue() {
        if (item == null || item.getActiveStock() == null)
            return null;
        return item.getActiveStock().getValueOfIssuedStock() + "";
    }

    public String getBalancedQuantity() {
        if (item == null || item.getActiveStock() == null)
            return null;
        return item.getActiveStock().getQuantityOfBalancedStock() + " " + item.getActiveStock().getUnit();
    }

    public String getBalancedValue() {
        if (item == null || item.getActiveStock() == null)
            return null;
        return item.getActiveStock().getValueOfBalancedStock() + "";
    }
}
