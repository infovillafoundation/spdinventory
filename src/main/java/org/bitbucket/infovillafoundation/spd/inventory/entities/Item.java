package org.bitbucket.infovillafoundation.spd.inventory.entities;

import org.bitbucket.infovillafoundation.spd.inventory.converters.CategoryItemDesignationConverter;
import org.bitbucket.infovillafoundation.spd.inventory.models.CategoryItemDesignation;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandah Aung on 15/12/14.
 */

@NamedQueries({
        @NamedQuery(name = "Item.findAll",
                query = "SELECT e FROM Item e"),
        @NamedQuery(name = "Item.findAllTopLevel",
                query = "SELECT e FROM Item e WHERE e.parentItem IS NULL"),
        @NamedQuery(name = "Item.findByName",
                query = "SELECT e FROM Item e WHERE e.name = :name"),
        @NamedQuery(name = "Item.findByNameLike",
                query = "SELECT e FROM Item e WHERE e.name LIKE :name")
})

@Entity
@Table(name = "items")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @Column(name = "associated_unit")
    private String associatedUnit;

    private String description;

    @Column(name = "category_item")
    @Convert(converter = CategoryItemDesignationConverter.class)
    private CategoryItemDesignation categoryItem;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "parent_id")
    private Item parentItem;

    @OneToMany(mappedBy = "parentItem", cascade = {CascadeType.REFRESH, CascadeType.PERSIST})
    private List<Item> childItems;

    @OneToMany(mappedBy = "item", fetch = FetchType.LAZY)
    private List<Stock> stocks;

    @OneToOne(mappedBy = "activeStockOwner")
    private Stock activeStock;

    public Item() {
        if (childItems == null)
            childItems = new ArrayList<>();
        if (stocks == null)
            stocks = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAssociatedUnit() {
        return associatedUnit;
    }

    public void setAssociatedUnit(String associatedUnit) {
        this.associatedUnit = associatedUnit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CategoryItemDesignation getCategoryItem() {
        return categoryItem;
    }

    public void setCategoryItem(CategoryItemDesignation categoryItem) {
        this.categoryItem = categoryItem;
    }

    public Item getParentItem() {
        return parentItem;
    }

    public void setParentItem(Item parentItem) {
        this.parentItem = parentItem;
    }

    public List<Item> getChildItems() {
        return childItems;
    }

    public void setChildItems(List<Item> childItems) {
        this.childItems = childItems;
    }

    public List<Stock> getStocks() {
        return stocks;
    }

    public void setStocks(List<Stock> stocks) {
        this.stocks = stocks;
    }

    public Stock getActiveStock() {
        return activeStock;
    }

    public void setActiveStock(Stock activeStock) {
        this.activeStock = activeStock;
    }

    public boolean isStockLow() {
        if (activeStock == null)
            return false;
        return activeStock.isStockLow();
    }
}
