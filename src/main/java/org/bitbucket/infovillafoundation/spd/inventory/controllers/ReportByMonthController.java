package org.bitbucket.infovillafoundation.spd.inventory.controllers;

import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.action.BackAction;
import io.datafx.controller.flow.context.ActionHandler;
import io.datafx.controller.flow.context.FlowActionHandler;
import io.datafx.controller.util.VetoException;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.bitbucket.infovillafoundation.spd.inventory.Main;
import org.bitbucket.infovillafoundation.spd.inventory.daos.MonthlyAmountReportDao;
import org.bitbucket.infovillafoundation.spd.inventory.daos.MonthlyQuantityCombinedReportDao;
import org.bitbucket.infovillafoundation.spd.inventory.daos.MonthlyQuantityReportDao;
import org.bitbucket.infovillafoundation.spd.inventory.entities.MonthlyAmountReport;
import org.bitbucket.infovillafoundation.spd.inventory.entities.MonthlyQuantityCombinedReport;
import org.bitbucket.infovillafoundation.spd.inventory.entities.MonthlyQuantityReport;
import org.bitbucket.infovillafoundation.spd.inventory.models.MonthlyAmountWithItem;
import org.bitbucket.infovillafoundation.spd.inventory.models.MonthlyQuantityCombinedWithItem;
import org.bitbucket.infovillafoundation.spd.inventory.models.MonthlyQuantityWithItem;
import org.bitbucket.infovillafoundation.spd.inventory.utils.Utilities;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 16/12/14.
 */

@FXMLController(value = "/fxml/report_by_month.fxml", title = "Report By Month")
public class ReportByMonthController {

    @ActionHandler
    private FlowActionHandler handler;

    @FXML
    private VBox pane;

    @FXML
    @BackAction
    private Button backButton;

    @FXML
    @ActionTrigger("saveReport")
    private Button saveReportButton;

    @FXML
    private DatePicker monthlyQuantityDatePicker;

    @FXML
    @ActionTrigger("loadMonthlyQuantitiesByDate")
    private Button monthlyQuantitiesByDateButton;

    @FXML
    @ActionTrigger("loadAllMonthlyQuantities")
    private Button allMonthlyQuantitiesButton;

    @FXML
    private TableView<MonthlyQuantityWithItem> monthlyQuantityTable;

    @FXML
    private DatePicker monthlyCombinedQuantityDatePicker;

    @FXML
    @ActionTrigger("loadMonthlyCombinedQuantitiesByDate")
    private Button monthlyCombinedQuantitiesByDateButton;

    @FXML
    @ActionTrigger("loadAllMonthlyCombinedQuantities")
    private Button allMonthlyCombinedQuantitiesButton;

    @FXML
    private TableView<MonthlyQuantityCombinedWithItem> monthlyCombinedQuantityTable;

    @FXML
    private DatePicker monthlyAmountDatePicker;

    @FXML
    @ActionTrigger("loadMonthlyAmountsByDate")
    private Button monthlyAmountsByDateButton;

    @FXML
    @ActionTrigger("loadAllMonthlyAMounts")
    private Button allMonthlyAmountsButton;

    @FXML
    private TableView<MonthlyAmountWithItem> monthlyAmountTable;

    @PostConstruct
    public void init() {
        setupBackShortcutListener();

        loadAllMonthlyQuantities();
        loadAllMonthlyCombinedQuantities();
        loadAllMonthlyAmounts();
    }

    private void setupBackShortcutListener() {
        final KeyCombination ctrlB = new KeyCodeCombination(KeyCode.B, KeyCombination.CONTROL_DOWN);
        pane.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (ctrlB.match(event)) {
                    try {
                        handler.navigateBack();
                    } catch (VetoException e) {
                    } catch (FlowException e) {
                    }
                }
            }
        });
    }

    private List<MonthlyQuantityWithItem> getMonthlyQuantitiesByDate(Date date) {
        List<MonthlyQuantityReport> monthlyQuantityReports = MonthlyQuantityReportDao.getMonthlyQuantityReportsByDate(date);
        List<MonthlyQuantityWithItem> monthlyQuantities = new ArrayList<>();
        for (MonthlyQuantityReport monthlyQuantityReport : monthlyQuantityReports) {
            MonthlyQuantityWithItem monthlyQuantityWithItem = new MonthlyQuantityWithItem(monthlyQuantityReport);
            monthlyQuantities.add(monthlyQuantityWithItem);
        }
        return monthlyQuantities;
    }

    @ActionMethod("loadMonthlyQuantitiesByDate")
    public void loadMonthlyQuantitiesByDate() {
        Date date = Utilities.toUtilDateFromLocalDate(monthlyQuantityDatePicker.getValue());
        List<MonthlyQuantityWithItem> monthlyQuantities = getMonthlyQuantitiesByDate(date);
        loadMonthlyQuantitiesIntoTable(monthlyQuantities);
    }

    private List<MonthlyQuantityWithItem> getAllMonthlyQuantities() {
        List<MonthlyQuantityReport> monthlyQuantityReports = MonthlyQuantityReportDao.getAllMonthlyQuantityReports();
        List<MonthlyQuantityWithItem> monthlyQuantities = new ArrayList<>();
        for (MonthlyQuantityReport monthlyQuantityReport : monthlyQuantityReports) {
            MonthlyQuantityWithItem monthlyQuantityWithItem = new MonthlyQuantityWithItem(monthlyQuantityReport);
            monthlyQuantities.add(monthlyQuantityWithItem);
        }
        return monthlyQuantities;
    }

    @ActionMethod("loadAllMonthlyQuantities")
    public void loadAllMonthlyQuantities() {
        List<MonthlyQuantityWithItem> monthlyQuantities = getAllMonthlyQuantities();
        loadMonthlyQuantitiesIntoTable(monthlyQuantities);
        monthlyQuantityDatePicker.setValue(null);
    }

    private void loadMonthlyQuantitiesIntoTable(List<MonthlyQuantityWithItem> monthlyQuantities) {
        monthlyQuantityTable.getItems().setAll(monthlyQuantities);
    }

    private List<MonthlyQuantityCombinedWithItem> getMonthlyCombinedQuantitiesByDate(Date date) {
        List<MonthlyQuantityCombinedReport> monthlyQuantityCombinedReports = MonthlyQuantityCombinedReportDao.getMonthlyQuantityCombinedReportsByDate(date);
        List<MonthlyQuantityCombinedWithItem> monthlyCombinedQuantities = new ArrayList<>();
        for (MonthlyQuantityCombinedReport monthlyQuantityCombinedReport : monthlyQuantityCombinedReports) {
            MonthlyQuantityCombinedWithItem monthlyQuantityCombinedWithItem = new MonthlyQuantityCombinedWithItem(monthlyQuantityCombinedReport);
            monthlyCombinedQuantities.add(monthlyQuantityCombinedWithItem);
        }
        return monthlyCombinedQuantities;
    }

    @ActionMethod("loadMonthlyCombinedQuantitiesByDate")
    public void loadMonthlyCombinedQuantitiesByDate() {
        Date date = Utilities.toUtilDateFromLocalDate(monthlyCombinedQuantityDatePicker.getValue());
        List<MonthlyQuantityCombinedWithItem> monthlyCombinedQuantities = getMonthlyCombinedQuantitiesByDate(date);
        loadMonthlyCombinedQuantitiesIntoTable(monthlyCombinedQuantities);
    }

    private List<MonthlyQuantityCombinedWithItem> getAllMonthlyCombinedQuantities() {
        List<MonthlyQuantityCombinedReport> monthlyQuantityCombinedReports = MonthlyQuantityCombinedReportDao.getAllMonthlyQuantityCombinedReports();
        List<MonthlyQuantityCombinedWithItem> monthlyCombinedQuantities = new ArrayList<>();
        for (MonthlyQuantityCombinedReport monthlyQuantityCombinedReport : monthlyQuantityCombinedReports) {
            MonthlyQuantityCombinedWithItem monthlyQuantityCombinedWithItem = new MonthlyQuantityCombinedWithItem(monthlyQuantityCombinedReport);
            monthlyCombinedQuantities.add(monthlyQuantityCombinedWithItem);
        }
        return monthlyCombinedQuantities;
    }

    @ActionMethod("loadAllMonthlyCombinedQuantities")
    public void loadAllMonthlyCombinedQuantities() {
        List<MonthlyQuantityCombinedWithItem> monthlyCombinedQuantities = getAllMonthlyCombinedQuantities();
        loadMonthlyCombinedQuantitiesIntoTable(monthlyCombinedQuantities);
        monthlyCombinedQuantityDatePicker.setValue(null);
    }

    private void loadMonthlyAmountsIntoTable(List<MonthlyAmountWithItem> monthlyAmounts) {
        monthlyAmountTable.getItems().setAll(monthlyAmounts);
    }

    private List<MonthlyAmountWithItem> getMonthlyAmountsByDate(Date date) {
        List<MonthlyAmountReport> monthlyAmountReports = MonthlyAmountReportDao.getMonthlyAmountReportsByDate(date);
        List<MonthlyAmountWithItem> monthlyAmounts = new ArrayList<>();
        for (MonthlyAmountReport monthlyAmountReport : monthlyAmountReports) {
            MonthlyAmountWithItem monthlyAmountWithItem = new MonthlyAmountWithItem(monthlyAmountReport);
            monthlyAmounts.add(monthlyAmountWithItem);
        }
        return monthlyAmounts;
    }

    @ActionMethod("loadMonthlyAmountsByDate")
    public void loadMonthlyAmountsByDate() {
        Date date = Utilities.toUtilDateFromLocalDate(monthlyAmountDatePicker.getValue());
        List<MonthlyAmountWithItem> monthlyAmounts = getMonthlyAmountsByDate(date);
        loadMonthlyAmountsIntoTable(monthlyAmounts);
    }

    private List<MonthlyAmountWithItem> getAllMonthlyAmounts() {
        List<MonthlyAmountReport> monthlyAmountReports = MonthlyAmountReportDao.getAllMonthlyAmountReports();
        List<MonthlyAmountWithItem> monthlyAmounts = new ArrayList<>();
        for (MonthlyAmountReport monthlyAmountReport : monthlyAmountReports) {
            MonthlyAmountWithItem monthlyAmountWithItem = new MonthlyAmountWithItem(monthlyAmountReport);
            monthlyAmounts.add(monthlyAmountWithItem);
        }
        return monthlyAmounts;
    }

    @ActionMethod("loadAllMonthlyAmounts")
    public void loadAllMonthlyAmounts() {
        List<MonthlyAmountWithItem> monthlyAmounts = getAllMonthlyAmounts();
        loadMonthlyAmountsIntoTable(monthlyAmounts);
        monthlyAmountDatePicker.setValue(null);
    }

    private void loadMonthlyCombinedQuantitiesIntoTable(List<MonthlyQuantityCombinedWithItem> monthlyCombinedQuantities) {
        monthlyCombinedQuantityTable.getItems().setAll(monthlyCombinedQuantities);
    }

    @ActionMethod("saveReport")
    public void saveReport() {
        HSSFWorkbook workbook = new HSSFWorkbook();

        createMonthlyQuantitySheet(workbook);
        createMonthlyQuantityCombinedSheet(workbook);
        createMonthlyAmountSheet(workbook);

        final FileChooser imageFileChooser = new FileChooser();
        imageFileChooser.setTitle("Choose Location for Monthly Reports");
        imageFileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );

        imageFileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Excel", "*.xls"));

        Stage chooserStage = Main.stage;
        File file = imageFileChooser.showOpenDialog(chooserStage);

        try {
            FileOutputStream out = new FileOutputStream(file);
            workbook.write(out);
            out.close();
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
    }

    private void createMonthlyQuantitySheet(HSSFWorkbook workbook) {
        HSSFSheet sheet = workbook.createSheet("Monthly Quantities Per Stock");
        HSSFCellStyle normalStyle = workbook.createCellStyle();
        normalStyle.setAlignment(CellStyle.ALIGN_CENTER);
        sheet.setHorizontallyCenter(true);
        sheet.setVerticallyCenter(true);
        HSSFCellStyle boldStyle = workbook.createCellStyle();
        boldStyle.setAlignment(CellStyle.ALIGN_CENTER);
        HSSFFont font = workbook.createFont();
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        boldStyle.setFont(font);
        HSSFCellStyle cellStyle = boldStyle;
        createMonthlyQuantityPerStockTitleRow(sheet, cellStyle);

        cellStyle = normalStyle;

        List<MonthlyQuantityWithItem> monthlyQuantities;

        if (monthlyQuantityDatePicker.getValue() == null) {
            monthlyQuantities = getAllMonthlyQuantities();
        } else {
            Date date = Utilities.toUtilDateFromLocalDate(monthlyQuantityDatePicker.getValue());
            monthlyQuantities = getMonthlyQuantitiesByDate(date);
        }

        for (int i = 0; i < monthlyQuantities.size(); i++) {
            Row currentRow = sheet.createRow(i + 1);
            org.apache.poi.ss.usermodel.Cell numberDataCell = currentRow.createCell(0);
            numberDataCell.setCellStyle(cellStyle);
            numberDataCell.setCellValue(i + 1);

            MonthlyQuantityWithItem monthlyQuantityWithItem = monthlyQuantities.get(i);

            org.apache.poi.ss.usermodel.Cell itemNameDataCell = currentRow.createCell(1);
            itemNameDataCell.setCellStyle(cellStyle);
            itemNameDataCell.setCellValue(monthlyQuantityWithItem.getItemName());
            org.apache.poi.ss.usermodel.Cell reportDateDataCell = currentRow.createCell(2);
            reportDateDataCell.setCellStyle(cellStyle);
            reportDateDataCell.setCellValue(monthlyQuantityWithItem.getReportDate());
            org.apache.poi.ss.usermodel.Cell unitDataCell = currentRow.createCell(3);
            unitDataCell.setCellStyle(cellStyle);
            unitDataCell.setCellValue(monthlyQuantityWithItem.getUnit());
            org.apache.poi.ss.usermodel.Cell openingBalanceDataCell = currentRow.createCell(4);
            openingBalanceDataCell.setCellStyle(cellStyle);
            openingBalanceDataCell.setCellValue(monthlyQuantityWithItem.getOpeningBalance());
            org.apache.poi.ss.usermodel.Cell rateDataCell = currentRow.createCell(5);
            rateDataCell.setCellStyle(cellStyle);
            rateDataCell.setCellValue(monthlyQuantityWithItem.getRate());
            org.apache.poi.ss.usermodel.Cell buyDataCell = currentRow.createCell(6);
            buyDataCell.setCellStyle(cellStyle);
            buyDataCell.setCellValue(monthlyQuantityWithItem.getQuantityBought());
            org.apache.poi.ss.usermodel.Cell drawDataCell = currentRow.createCell(7);
            drawDataCell.setCellStyle(cellStyle);
            drawDataCell.setCellValue(monthlyQuantityWithItem.getQuantityDrawn());
            org.apache.poi.ss.usermodel.Cell closingBalanceDataCell = currentRow.createCell(8);
            closingBalanceDataCell.setCellStyle(cellStyle);
            closingBalanceDataCell.setCellValue(monthlyQuantityWithItem.getClosingBalance());
        }
    }

    private void createMonthlyQuantityPerStockTitleRow(HSSFSheet sheet, CellStyle cellStyle) {
        Row titleRow = sheet.createRow(0);
        org.apache.poi.ss.usermodel.Cell numberCell = titleRow.createCell(0);
        numberCell.setCellStyle(cellStyle);
        numberCell.setCellValue("No");
        org.apache.poi.ss.usermodel.Cell itemNameCell = titleRow.createCell(1);
        itemNameCell.setCellStyle(cellStyle);
        itemNameCell.setCellValue("Item Name");
        org.apache.poi.ss.usermodel.Cell reportDateCell = titleRow.createCell(2);
        reportDateCell.setCellStyle(cellStyle);
        reportDateCell.setCellValue("Report Date");
        org.apache.poi.ss.usermodel.Cell unitCell = titleRow.createCell(3);
        unitCell.setCellStyle(cellStyle);
        unitCell.setCellValue("Unit");
        org.apache.poi.ss.usermodel.Cell openingBalanceCell = titleRow.createCell(4);
        openingBalanceCell.setCellStyle(cellStyle);
        openingBalanceCell.setCellValue("Opening Balance");
        org.apache.poi.ss.usermodel.Cell rateCell = titleRow.createCell(5);
        rateCell.setCellStyle(cellStyle);
        rateCell.setCellValue("Rate");
        org.apache.poi.ss.usermodel.Cell buyCell = titleRow.createCell(6);
        buyCell.setCellStyle(cellStyle);
        buyCell.setCellValue("Buy");
        org.apache.poi.ss.usermodel.Cell drawCell = titleRow.createCell(7);
        drawCell.setCellStyle(cellStyle);
        drawCell.setCellValue("Draw");
        org.apache.poi.ss.usermodel.Cell closingBalanceCell = titleRow.createCell(8);
        closingBalanceCell.setCellStyle(cellStyle);
        closingBalanceCell.setCellValue("Closing Balance");
    }

    private void createMonthlyQuantityCombinedSheet(HSSFWorkbook workbook) {
        HSSFSheet sheet = workbook.createSheet("Monthly Quantities Per Item");
        HSSFCellStyle normalStyle = workbook.createCellStyle();
        normalStyle.setAlignment(CellStyle.ALIGN_CENTER);
        sheet.setHorizontallyCenter(true);
        sheet.setVerticallyCenter(true);
        HSSFCellStyle boldStyle = workbook.createCellStyle();
        boldStyle.setAlignment(CellStyle.ALIGN_CENTER);
        HSSFFont font = workbook.createFont();
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        boldStyle.setFont(font);
        HSSFCellStyle cellStyle = boldStyle;
        createMonthlyQuantityPerItemTitleRow(sheet, cellStyle);

        cellStyle = normalStyle;

        List<MonthlyQuantityCombinedWithItem> monthlyCombinedQuantities;

        if (monthlyCombinedQuantityDatePicker.getValue() == null) {
            monthlyCombinedQuantities = getAllMonthlyCombinedQuantities();
        } else {
            Date date = Utilities.toUtilDateFromLocalDate(monthlyCombinedQuantityDatePicker.getValue());
            monthlyCombinedQuantities = getMonthlyCombinedQuantitiesByDate(date);
        }

        for (int i = 0; i < monthlyCombinedQuantities.size(); i++) {
            Row currentRow = sheet.createRow(i + 1);
            org.apache.poi.ss.usermodel.Cell numberDataCell = currentRow.createCell(0);
            numberDataCell.setCellStyle(cellStyle);
            numberDataCell.setCellValue(i + 1);

            MonthlyQuantityCombinedWithItem monthlyQuantityCombinedWithItem = monthlyCombinedQuantities.get(i);

            org.apache.poi.ss.usermodel.Cell itemNameDataCell = currentRow.createCell(1);
            itemNameDataCell.setCellStyle(cellStyle);
            itemNameDataCell.setCellValue(monthlyQuantityCombinedWithItem.getItemName());
            org.apache.poi.ss.usermodel.Cell reportDateDataCell = currentRow.createCell(2);
            reportDateDataCell.setCellStyle(cellStyle);
            reportDateDataCell.setCellValue(monthlyQuantityCombinedWithItem.getReportDate());
            org.apache.poi.ss.usermodel.Cell unitDataCell = currentRow.createCell(3);
            unitDataCell.setCellStyle(cellStyle);
            unitDataCell.setCellValue(monthlyQuantityCombinedWithItem.getUnit());
            org.apache.poi.ss.usermodel.Cell openingBalanceDataCell = currentRow.createCell(4);
            openingBalanceDataCell.setCellStyle(cellStyle);
            openingBalanceDataCell.setCellValue(monthlyQuantityCombinedWithItem.getOpeningBalance());
            org.apache.poi.ss.usermodel.Cell buyDataCell = currentRow.createCell(5);
            buyDataCell.setCellStyle(cellStyle);
            buyDataCell.setCellValue(monthlyQuantityCombinedWithItem.getQuantityBought());
            org.apache.poi.ss.usermodel.Cell drawDataCell = currentRow.createCell(6);
            drawDataCell.setCellStyle(cellStyle);
            drawDataCell.setCellValue(monthlyQuantityCombinedWithItem.getQuantityDrawn());
            org.apache.poi.ss.usermodel.Cell closingBalanceDataCell = currentRow.createCell(7);
            closingBalanceDataCell.setCellStyle(cellStyle);
            closingBalanceDataCell.setCellValue(monthlyQuantityCombinedWithItem.getClosingBalance());
        }
    }

    private void createMonthlyQuantityPerItemTitleRow(HSSFSheet sheet, CellStyle cellStyle) {
        Row titleRow = sheet.createRow(0);
        org.apache.poi.ss.usermodel.Cell numberCell = titleRow.createCell(0);
        numberCell.setCellStyle(cellStyle);
        numberCell.setCellValue("No");
        org.apache.poi.ss.usermodel.Cell itemNameCell = titleRow.createCell(1);
        itemNameCell.setCellStyle(cellStyle);
        itemNameCell.setCellValue("Item Name");
        org.apache.poi.ss.usermodel.Cell reportDateCell = titleRow.createCell(2);
        reportDateCell.setCellStyle(cellStyle);
        reportDateCell.setCellValue("Report Date");
        org.apache.poi.ss.usermodel.Cell unitCell = titleRow.createCell(3);
        unitCell.setCellStyle(cellStyle);
        unitCell.setCellValue("Unit");
        org.apache.poi.ss.usermodel.Cell openingBalanceCell = titleRow.createCell(4);
        openingBalanceCell.setCellStyle(cellStyle);
        openingBalanceCell.setCellValue("Opening Balance");
        org.apache.poi.ss.usermodel.Cell buyCell = titleRow.createCell(5);
        buyCell.setCellStyle(cellStyle);
        buyCell.setCellValue("Buy");
        org.apache.poi.ss.usermodel.Cell drawCell = titleRow.createCell(6);
        drawCell.setCellStyle(cellStyle);
        drawCell.setCellValue("Draw");
        org.apache.poi.ss.usermodel.Cell closingBalanceCell = titleRow.createCell(7);
        closingBalanceCell.setCellStyle(cellStyle);
        closingBalanceCell.setCellValue("Closing Balance");
    }

    private void createMonthlyAmountSheet(HSSFWorkbook workbook) {
        HSSFSheet sheet = workbook.createSheet("Monthly Amounts");
        HSSFCellStyle normalStyle = workbook.createCellStyle();
        normalStyle.setAlignment(CellStyle.ALIGN_CENTER);
        sheet.setHorizontallyCenter(true);
        sheet.setVerticallyCenter(true);
        HSSFCellStyle boldStyle = workbook.createCellStyle();
        boldStyle.setAlignment(CellStyle.ALIGN_CENTER);
        HSSFFont font = workbook.createFont();
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        boldStyle.setFont(font);
        HSSFCellStyle cellStyle = boldStyle;
        createMonthlyAmountTitleRow(sheet, cellStyle);

        cellStyle = normalStyle;

        List<MonthlyAmountWithItem> monthlyAmounts;

        if (monthlyAmountDatePicker.getValue() == null) {
            monthlyAmounts = getAllMonthlyAmounts();
        } else {
            Date date = Utilities.toUtilDateFromLocalDate(monthlyAmountDatePicker.getValue());
            monthlyAmounts = getMonthlyAmountsByDate(date);
        }

        for (int i = 0; i < monthlyAmounts.size(); i++) {
            Row currentRow = sheet.createRow(i + 1);
            org.apache.poi.ss.usermodel.Cell numberDataCell = currentRow.createCell(0);
            numberDataCell.setCellStyle(cellStyle);
            numberDataCell.setCellValue(i + 1);

            MonthlyAmountWithItem monthlyAmountWithItem = monthlyAmounts.get(i);

            org.apache.poi.ss.usermodel.Cell itemNameDataCell = currentRow.createCell(1);
            itemNameDataCell.setCellStyle(cellStyle);
            itemNameDataCell.setCellValue(monthlyAmountWithItem.getItemName());
            org.apache.poi.ss.usermodel.Cell reportDateDataCell = currentRow.createCell(2);
            reportDateDataCell.setCellStyle(cellStyle);
            reportDateDataCell.setCellValue(monthlyAmountWithItem.getReportDate());
            org.apache.poi.ss.usermodel.Cell openingBalanceDataCell = currentRow.createCell(3);
            openingBalanceDataCell.setCellStyle(cellStyle);
            openingBalanceDataCell.setCellValue(monthlyAmountWithItem.getOpeningBalance());
            org.apache.poi.ss.usermodel.Cell buyDataCell = currentRow.createCell(4);
            buyDataCell.setCellStyle(cellStyle);
            buyDataCell.setCellValue(monthlyAmountWithItem.getAmountBought());
            org.apache.poi.ss.usermodel.Cell drawDataCell = currentRow.createCell(5);
            drawDataCell.setCellStyle(cellStyle);
            drawDataCell.setCellValue(monthlyAmountWithItem.getAmountDrawn());
            org.apache.poi.ss.usermodel.Cell closingBalanceDataCell = currentRow.createCell(6);
            closingBalanceDataCell.setCellStyle(cellStyle);
            closingBalanceDataCell.setCellValue(monthlyAmountWithItem.getClosingBalance());
        }
    }

    private void createMonthlyAmountTitleRow(HSSFSheet sheet, CellStyle cellStyle) {
        Row titleRow = sheet.createRow(0);
        org.apache.poi.ss.usermodel.Cell numberCell = titleRow.createCell(0);
        numberCell.setCellStyle(cellStyle);
        numberCell.setCellValue("No");
        org.apache.poi.ss.usermodel.Cell itemNameCell = titleRow.createCell(1);
        itemNameCell.setCellStyle(cellStyle);
        itemNameCell.setCellValue("Item Name");
        org.apache.poi.ss.usermodel.Cell reportDateCell = titleRow.createCell(2);
        reportDateCell.setCellStyle(cellStyle);
        reportDateCell.setCellValue("Report Date");
        org.apache.poi.ss.usermodel.Cell openingBalanceCell = titleRow.createCell(3);
        openingBalanceCell.setCellStyle(cellStyle);
        openingBalanceCell.setCellValue("Opening Balance");
        org.apache.poi.ss.usermodel.Cell buyCell = titleRow.createCell(4);
        buyCell.setCellStyle(cellStyle);
        buyCell.setCellValue("Buy");
        org.apache.poi.ss.usermodel.Cell drawCell = titleRow.createCell(5);
        drawCell.setCellStyle(cellStyle);
        drawCell.setCellValue("Draw");
        org.apache.poi.ss.usermodel.Cell closingBalanceCell = titleRow.createCell(6);
        closingBalanceCell.setCellStyle(cellStyle);
        closingBalanceCell.setCellValue("Closing Balance");
    }
}
