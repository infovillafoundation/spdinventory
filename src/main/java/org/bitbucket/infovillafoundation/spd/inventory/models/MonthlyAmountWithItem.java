package org.bitbucket.infovillafoundation.spd.inventory.models;

import org.bitbucket.infovillafoundation.spd.inventory.entities.MonthlyAmountReport;
import org.bitbucket.infovillafoundation.spd.inventory.utils.Utilities;

/**
 * Created by Sandah Aung on 31/1/15.
 */
public class MonthlyAmountWithItem {

    private MonthlyAmountReport monthlyAmountReport;

    public MonthlyAmountWithItem(MonthlyAmountReport monthlyAmountReport) {
        this.monthlyAmountReport = monthlyAmountReport;
    }

    public String getItemName() {
        return monthlyAmountReport.getItem().getName();
    }

    public String getReportDate() {
        return Utilities.fromDate(monthlyAmountReport.getReportDate());
    }

    public String getOpeningBalance() {
        return monthlyAmountReport.getOpeningBalance() + " ";
    }

    public String getAmountBought() {
        return monthlyAmountReport.getAmountBought() + " ";
    }

    public String getAmountDrawn() {
        return monthlyAmountReport.getAmountDrawn() + " ";
    }

    public String getClosingBalance() {
        return monthlyAmountReport.getClosingBalance() + " ";
    }
}
