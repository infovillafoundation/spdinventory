package org.bitbucket.infovillafoundation.spd.inventory.models;

import org.bitbucket.infovillafoundation.spd.inventory.entities.MonthlyQuantityCombinedReport;
import org.bitbucket.infovillafoundation.spd.inventory.utils.Utilities;

/**
 * Created by Sandah Aung on 31/1/15.
 */
public class MonthlyQuantityCombinedWithItem {

    private MonthlyQuantityCombinedReport monthlyQuantityCombinedReport;

    public MonthlyQuantityCombinedWithItem(MonthlyQuantityCombinedReport monthlyQuantityCombinedReport) {
        this.monthlyQuantityCombinedReport = monthlyQuantityCombinedReport;
    }

    public String getItemName() {
        return monthlyQuantityCombinedReport.getItem().getName();
    }

    public String getReportDate() {
        return Utilities.fromDate(monthlyQuantityCombinedReport.getReportDate());
    }

    public String getUnit() {
        return monthlyQuantityCombinedReport.getUnit();
    }

    public String getOpeningBalance() {
        return monthlyQuantityCombinedReport.getOpeningBalance() + " " + getUnit();
    }

    public String getQuantityBought() {
        return monthlyQuantityCombinedReport.getQuantityBought() + " " + getUnit();
    }

    public String getQuantityDrawn() {
        return monthlyQuantityCombinedReport.getQuantityDrawn() + " " + getUnit();
    }

    public String getClosingBalance() {
        return monthlyQuantityCombinedReport.getClosingBalance() + " " + getUnit();
    }
}
