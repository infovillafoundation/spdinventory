package org.bitbucket.infovillafoundation.spd.inventory.daos;

import org.bitbucket.infovillafoundation.spd.inventory.entities.Stock;
import org.bitbucket.infovillafoundation.spd.inventory.entities.StockIssue;

import javax.persistence.EntityManager;

/**
 * Created by Sandah Aung on 17/12/14.
 */
public class StockIssueDao extends Dao {

    public static void saveStockIssue(StockIssue stockIssue) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stockIssue);
        em.getTransaction().commit();
        em.close();
    }

    public static void saveStockWithStockIssue(Stock stock, StockIssue stockIssue) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stockIssue);
        em.merge(stock);
        em.getTransaction().commit();
        em.close();
    }

    public static void updateStockIssue(StockIssue stockIssue) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(stockIssue);
        em.getTransaction().commit();
        em.close();
    }

    public static void updateStockWithStockIssue(Stock stock, StockIssue stockIssue) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(stockIssue);
        em.merge(stock);
        em.getTransaction().commit();
        em.close();
    }

    public static void deleteStockIssue(StockIssue stockIssue) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.contains(stockIssue) ? stockIssue : em.merge(stockIssue));
        em.getTransaction().commit();
        em.close();
    }

    public static void deleteStockIssueWithStock(StockIssue stockIssue, Stock stock) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.contains(stockIssue) ? stockIssue : em.merge(stockIssue));
        em.merge(stock);
        em.getTransaction().commit();
        em.close();
    }
}
