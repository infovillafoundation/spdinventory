package org.bitbucket.infovillafoundation.spd.inventory.models;

import org.bitbucket.infovillafoundation.spd.inventory.entities.DailyAmountReport;
import org.bitbucket.infovillafoundation.spd.inventory.utils.Utilities;

/**
 * Created by Sandah Aung on 31/1/15.
 */
public class DailyAmountWithItem {

    private DailyAmountReport dailyAmountReport;

    public DailyAmountWithItem(DailyAmountReport dailyAmountReport) {
        this.dailyAmountReport = dailyAmountReport;
    }

    public String getItemName() {
        return dailyAmountReport.getItem().getName();
    }

    public String getReportDate() {
        return Utilities.fromDate(dailyAmountReport.getReportDate());
    }

    public String getOpeningBalance() {
        return dailyAmountReport.getOpeningBalance() + " ";
    }

    public String getAmountBought() {
        return dailyAmountReport.getAmountBought() + " ";
    }

    public String getAmountDrawn() {
        return dailyAmountReport.getAmountDrawn() + " ";
    }

    public String getClosingBalance() {
        return dailyAmountReport.getClosingBalance() + " ";
    }
}
