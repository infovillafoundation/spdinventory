package org.bitbucket.infovillafoundation.spd.inventory.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Sandah Aung on 15/12/14.
 */

@NamedQueries({
        @NamedQuery(name = "DailyQuantityReport.findAll",
                query = "SELECT e FROM DailyQuantityReport e"),
        @NamedQuery(name = "DailyQuantityReport.findByReportDateDesc",
                query = "SELECT e FROM DailyQuantityReport e ORDER BY e.reportDate DESC"),
        @NamedQuery(name = "DailyQuantityReport.findByReportDate",
                query = "SELECT e FROM DailyQuantityReport e WHERE e.reportDate = :reportDate"),
        @NamedQuery(name = "DailyQuantityReport.findByStockAndReportDate",
                query = "SELECT e FROM DailyQuantityReport e WHERE e.stock = :stock AND e.reportDate = :reportDate"),
        @NamedQuery(name = "DailyQuantityReport.findByStockIdAndReportDate",
                query = "SELECT e FROM DailyQuantityReport e WHERE e.stock.id = :stockId AND e.reportDate = :reportDate")
})

@Entity
@Table(name = "daily_quantity_reports")
public class DailyQuantityReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "report_date")
    @Temporal(TemporalType.DATE)
    private Date reportDate;

    private String unit;

    @Column(name = "opening_balance")
    private double openingBalance;

    private double rate;

    @Column(name = "buy")
    private double quantityBought;

    @Column(name = "draw")
    private double quantityDrawn;

    @Column(name = "closing_balance")
    private double closingBalance;

    @OneToOne
    @JoinColumn(name = "stock_id")
    private Stock stock;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(double openingBalance) {
        this.openingBalance = openingBalance;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getQuantityBought() {
        return quantityBought;
    }

    public void setQuantityBought(double quantityBought) {
        this.quantityBought = quantityBought;
    }

    public double getQuantityDrawn() {
        return quantityDrawn;
    }

    public void setQuantityDrawn(double quantityDrawn) {
        this.quantityDrawn = quantityDrawn;
    }

    public double getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(double closingBalance) {
        this.closingBalance = closingBalance;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }
}
