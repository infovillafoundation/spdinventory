package org.bitbucket.infovillafoundation.spd.inventory.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Sandah Aung on 25/12/14.
 */
@Entity(name = "issues")
public class StockIssue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "issued_date")
    @Temporal(TemporalType.DATE)
    private Date issuedDate;

    @Column(name = "particular")
    private String particular;

    @Column(name = "allocated_use")
    private double allocatedUse;

    @Column(name = "allocated_use_value")
    private double allocatedUseValue;

    @Column(name = "actual_use")
    private double actualUse;

    @Column(name = "actual_use_value")
    private double actualUseValue;

    @Column(name = "income_from_difference")
    private double incomeFromDifference;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(Date issuedDate) {
        this.issuedDate = issuedDate;
    }

    public String getParticular() {
        return particular;
    }

    public void setParticular(String particular) {
        this.particular = particular;
    }

    public double getAllocatedUse() {
        return allocatedUse;
    }

    public void setAllocatedUse(double allocatedUse) {
        this.allocatedUse = allocatedUse;
    }

    public double getAllocatedUseValue() {
        return allocatedUseValue;
    }

    public void setAllocatedUseValue(double allocatedUseValue) {
        this.allocatedUseValue = allocatedUseValue;
    }

    public double getActualUse() {
        return actualUse;
    }

    public void setActualUse(double actualUse) {
        this.actualUse = actualUse;
    }

    public double getActualUseValue() {
        return actualUseValue;
    }

    public void setActualUseValue(double actualUseValue) {
        this.actualUseValue = actualUseValue;
    }

    public double getIncomeFromDifference() {
        return incomeFromDifference;
    }

    public void setIncomeFromDifference(double incomeFromDifference) {
        this.incomeFromDifference = incomeFromDifference;
    }
}
