package org.bitbucket.infovillafoundation.spd.inventory.uicomponents;

import javafx.scene.control.TextField;
import org.controlsfx.control.textfield.TextFields;

/**
 * Created by Sandah Aung on 12/1/15.
 */
public class ClearableTextField extends TextField {
    public static TextField createClearableTextField() {
        return TextFields.createClearableTextField();
    }
}