package org.bitbucket.infovillafoundation.spd.inventory.models;

import org.bitbucket.infovillafoundation.spd.inventory.entities.Stock;
import org.bitbucket.infovillafoundation.spd.inventory.utils.Utilities;

/**
 * Created by Sandah Aung on 21/1/15.
 */
public class RepeatableItemWithStock extends ItemWithStock {
    private Stock stock;

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public String getName() {
        if (stock == null || stock.getItem() == null)
            return null;
        return stock.getItem().getName();
    }

    public String getPrice() {
        if (stock == null || stock.getItem() == null)
            return null;
        return stock.getPriceOfReceivedStock() + "";
    }

    public String getReceivedDate() {
        if (stock == null || stock.getItem() == null)
            return null;
        return Utilities.fromDate(stock.getReceivedDate());
    }

    public String getReceivedQuantity() {
        if (stock == null || stock.getItem() == null)
            return null;
        return stock.getQuantityOfReceivedStock() + " " + stock.getUnit();
    }

    public String getReceivedValue() {
        if (stock == null || stock.getItem() == null)
            return null;
        return stock.getValueOfReceivedStock() + "";
    }

    public String getIssuedQuantity() {
        if (stock == null || stock.getItem() == null)
            return null;
        return stock.getQuantityOfIssuedStock() + " " + stock.getUnit();
    }

    public String getIssuedValue() {
        if (stock == null || stock.getItem() == null)
            return null;
        return stock.getValueOfIssuedStock() + "";
    }

    public String getBalancedQuantity() {
        if (stock == null || stock.getItem() == null)
            return null;
        return stock.getQuantityOfBalancedStock() + " " + stock.getUnit();
    }

    public String getBalancedValue() {
        if (stock == null || stock.getItem() == null)
            return null;
        return stock.getValueOfBalancedStock() + "";
    }
}
