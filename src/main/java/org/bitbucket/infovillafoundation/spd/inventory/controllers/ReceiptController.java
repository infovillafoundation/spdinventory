package org.bitbucket.infovillafoundation.spd.inventory.controllers;

import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.action.BackAction;
import io.datafx.controller.flow.context.ActionHandler;
import io.datafx.controller.flow.context.FlowActionHandler;
import io.datafx.controller.util.VetoException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.util.StringConverter;
import org.bitbucket.infovillafoundation.spd.inventory.daos.*;
import org.bitbucket.infovillafoundation.spd.inventory.entities.*;
import org.bitbucket.infovillafoundation.spd.inventory.models.StockContainer;
import org.bitbucket.infovillafoundation.spd.inventory.utils.Utilities;
import org.controlsfx.control.MasterDetailPane;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Created by Sandah Aung on 26/12/14.
 */

@FXMLController(value = "/fxml/receipts.fxml", title = "Receipt")
public class ReceiptController {

    @ActionHandler
    private FlowActionHandler handler;

    @FXML
    private MasterDetailPane pane;

    @FXML
    private GridPane saveEditReceiptGrid;

    @FXML
    private GridPane showReceiptGrid;

    @FXML
    private GridPane confirmDeleteReceiptGrid;

    @Inject
    private StockContainer stockContainer;

    @FXML
    @BackAction
    private Button backButton;

    @FXML
    @ActionTrigger("loadNewReceiptForm")
    private Button newReceiptButton;

    @FXML
    @ActionTrigger("loadEditReceiptForm")
    private Button editReceiptButton;

    @FXML
    @ActionTrigger("loadDeleteReceiptForm")
    private Button deleteReceiptButton;

    @FXML
    private Text itemName;

    @FXML
    private Text stockReceivedDate;

    @FXML
    private TableView<StockReceipt> receiptTableView;

    @FXML
    @ActionTrigger("loadReceiptDetails")
    private MenuItem detailsMenuItem;

    @FXML
    @ActionTrigger("setAsActiveStock")
    private MenuItem setAsActiveStockMenuItem;

    @FXML
    private Text allocateReceiptText;

    @FXML
    private DatePicker receivedDateDatePicker;

    @FXML
    private TextField particularInput;

    @FXML
    private TextField receivedQuantityInput;

    @FXML
    @ActionTrigger("createStockReceipt")
    private Button createReceiptButton;

    @FXML
    @ActionTrigger("updateStockReceipt")
    private Button saveReceiptButton;

    @FXML
    @ActionTrigger("clearStockReceipt")
    private Button clearReceiptButton;

    @FXML
    private Text receivedDateText;

    @FXML
    private Text particularText;

    @FXML
    private Text receivedQuantityText;

    @FXML
    private Text receivedValueText;

    @FXML
    @ActionTrigger("createActualUse")
    private Button createActualUseButton;

    @FXML
    @ActionTrigger("clearActualUse")
    private Button clearActualUseButton;

    @FXML
    @ActionTrigger("confirmDeleteStockReceipt")
    private Button confirmDeleteReceiptButton;

    @FXML
    @ActionTrigger("cancelDeleteStockReceipt")
    private Button cancelDeleteReceiptButton;

    private StockReceipt currentStockReceipt;

    @PostConstruct
    public void init() {
        itemName.setText(stockContainer.getStock().getItem().getName());
        stockReceivedDate.setText(Utilities.fromDate(stockContainer.getStock().getReceivedDate()));
        receiptTableView.getItems().setAll(stockContainer.getStock().getStockReceipts());
        setupBackShortcutListener();
        setupRowClickListener();
        loadNewStockReceiptForm();
        setupDateConverter();
        setupValidators();
    }

    private void setupRowClickListener() {
        receiptTableView
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(new ChangeListener<StockReceipt>() {
                                 @Override
                                 public void changed(ObservableValue<? extends StockReceipt> observable, StockReceipt oldValue, StockReceipt newValue) {
                                     currentStockReceipt = newValue;
                                     if (currentStockReceipt == null) {
                                         loadNewStockReceiptForm();
                                         editReceiptButton.setVisible(false);
                                         deleteReceiptButton.setVisible(false);
                                     } else {
                                         loadStockReceiptDetails();
                                         if (isDeletable()) {
                                             editReceiptButton.setVisible(true);
                                             deleteReceiptButton.setVisible(true);
                                         }
                                     }

                                 }
                             }

                );
    }

    private void setupValidators() {

        receivedDateDatePicker.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (receivedDateDatePicker.getValue() == null) {
                    receivedDateDatePicker.setStyle("-fx-border-color: red");
                } else {
                    receivedDateDatePicker.setStyle("-fx-border-color: null");
                }
            }
        });

        receivedQuantityInput.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (receivedQuantityInput.getText().isEmpty())
                    receivedQuantityInput.setStyle("-fx-border-color: red");
                else
                    receivedQuantityInput.setStyle("-fx-border-color: null");
            }
        });
    }

    private void setupBackShortcutListener() {
        final KeyCombination ctrlB = new KeyCodeCombination(KeyCode.B, KeyCombination.CONTROL_DOWN);
        pane.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (ctrlB.match(event)) {
                    try {
                        handler.navigateBack();
                    } catch (VetoException e) {
                    } catch (FlowException e) {
                    }
                }
            }
        });
    }

    private void setupDateConverter() {
        String pattern = "dd/M/yyyy";
        receivedDateDatePicker.setPromptText(pattern.toLowerCase());
        receivedDateDatePicker.setConverter(new StringConverter<LocalDate>() {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateTimeFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateTimeFormatter);

                } else {
                    return null;
                }
            }
        });
    }


    @ActionMethod("loadStockReceiptDetails")
    public void loadStockReceiptDetails() {
        loadStockReceiptDetailsGrid();
        loadStockReceiptIntoShowGrid();
    }

    private void loadStockReceiptDetailsGrid() {
        enableThisGrid(showReceiptGrid);
    }

    private void enableThisGrid(GridPane grid) {
        saveEditReceiptGrid.setVisible(false);
        showReceiptGrid.setVisible(false);
        confirmDeleteReceiptGrid.setVisible(false);
        grid.setVisible(true);
        grid.toFront();
    }

    private void loadStockReceiptIntoShowGrid() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy");
        receivedDateText.setText(simpleDateFormat.format(currentStockReceipt.getReceivedDate()));
        particularText.setText(currentStockReceipt.getParticular());
        receivedQuantityText.setText(currentStockReceipt.getReceivedQuantity() + "");
        receivedValueText.setText(currentStockReceipt.getReceivedValue() + "");
    }

    @ActionMethod("loadNewReceiptForm")
    public void loadNewStockReceiptForm() {
        loadNewStockReceiptGrid();
        prepareNewStockReceipt();
    }

    private void prepareNewStockReceipt() {
        receivedDateDatePicker.setValue(Utilities.toLocalDateFromUtilDate(new Date()));
        receivedDateDatePicker.setDisable(true);
        allocateReceiptText.setText("New Stock Receipt");
        saveReceiptButton.setVisible(false);
        createReceiptButton.setVisible(true);
        createReceiptButton.toFront();
    }

    private void loadNewStockReceiptGrid() {
        enableThisGrid(saveEditReceiptGrid);
        clearFields();
    }

    @ActionMethod("clearStockReceipt")
    public void clear() {
        clearFields();
    }

    private void clearFields() {
        receivedDateDatePicker.setValue(null);
        receivedDateDatePicker.setDisable(false);
        particularInput.clear();
        receivedQuantityInput.clear();
        clearFormWarnings();
    }


    @ActionMethod("createStockReceipt")
    public void createStockReceipt() {

        if (validateBeforeFormSubmission()) {

            StockReceipt stockReceipt = new StockReceipt();
            loadFormDataIntoStockReceipt(stockReceipt);
            Stock stock = stockContainer.getStock();
            stock.setQuantityOfReceivedStock(stock.getQuantityOfReceivedStock() + stockReceipt.getReceivedQuantity());
            stock.setValueOfReceivedStock(stock.getQuantityOfReceivedStock() * stock.getPriceOfReceivedStock());
            stock.setQuantityOfBalancedStock(stock.getQuantityOfBalancedStock() + stockReceipt.getReceivedQuantity());
            stock.setValueOfBalancedStock(stock.getQuantityOfBalancedStock() * stock.getPriceOfReceivedStock());
            stockReceipt.setReceivedValue(stockReceipt.getReceivedQuantity() * stock.getPriceOfReceivedStock());
            stock.getStockReceipts().add(stockReceipt);
            StockReceiptDao.saveStockWithStockReceipt(stock, stockReceipt);
            refreshTable();
            currentStockReceipt = stockReceipt;
            receiptTableView.getSelectionModel().select(stockReceipt);

            Item item = stock.getItem();

            DailyQuantityReport dailyQuantityReport = DailyQuantityReportDao.getDailyQuantityReportByStockAndDate(stock, stockReceipt.getReceivedDate());
            boolean updateDailyQuantityReport = true;
            if (dailyQuantityReport == null) {
                updateDailyQuantityReport = false;
                dailyQuantityReport = new DailyQuantityReport();
                dailyQuantityReport.setReportDate(stockReceipt.getReceivedDate());
                dailyQuantityReport.setUnit(stock.getUnit());
                dailyQuantityReport.setRate(stock.getPriceOfReceivedStock());
                dailyQuantityReport.setStock(stock);
            }
            dailyQuantityReport.setQuantityBought(dailyQuantityReport.getQuantityBought() + stockReceipt.getReceivedQuantity());
            dailyQuantityReport.setClosingBalance(dailyQuantityReport.getOpeningBalance() + dailyQuantityReport.getQuantityBought() - dailyQuantityReport.getQuantityDrawn());
            if (updateDailyQuantityReport)
                DailyQuantityReportDao.updateDailyQuantityReport(dailyQuantityReport);
            else
                DailyQuantityReportDao.saveDailyQuantityReport(dailyQuantityReport);

            DailyAmountReport dailyAmountReport = DailyAmountReportDao.getDailyAmountReportByDateAndItem(stockReceipt.getReceivedDate(), item);
            boolean updateDailyAmountReport = true;
            if (dailyAmountReport == null) {
                updateDailyAmountReport = false;
                dailyAmountReport = new DailyAmountReport();
                dailyAmountReport.setReportDate(stockReceipt.getReceivedDate());
                dailyAmountReport.setItem(item);
            }
            dailyAmountReport.setAmountBought(dailyAmountReport.getAmountBought() + stockReceipt.getReceivedQuantity() * dailyQuantityReport.getRate());
            dailyAmountReport.setClosingBalance(dailyAmountReport.getOpeningBalance() + dailyAmountReport.getAmountBought() - dailyAmountReport.getAmountDrawn());
            if (updateDailyAmountReport)
                DailyAmountReportDao.updateDailyAmountReport(dailyAmountReport);
            else
                DailyAmountReportDao.updateDailyAmountReport(dailyAmountReport);

            DailyQuantityCombinedReport dailyQuantityCombinedReport = DailyQuantityCombinedReportDao.getDailyQuantityCombinedReportByDateAndItemAndUnit(stockReceipt.getReceivedDate(), item, stock.getUnit());
            boolean updateDailyQuantityCombinedReport = true;
            if (dailyQuantityCombinedReport == null) {
                updateDailyQuantityCombinedReport = false;
                dailyQuantityCombinedReport = new DailyQuantityCombinedReport();
                dailyQuantityCombinedReport.setItem(item);
                dailyQuantityCombinedReport.setReportDate(stockReceipt.getReceivedDate());
                dailyQuantityCombinedReport.setUnit(stock.getUnit());
            }
            if (!updateDailyQuantityReport)
                dailyQuantityCombinedReport.getSourceDailyQuantityReports().add(dailyQuantityReport);
            dailyQuantityCombinedReport.setQuantityBought(dailyQuantityCombinedReport.getQuantityBought() + stockReceipt.getReceivedQuantity());
            dailyQuantityCombinedReport.setClosingBalance(dailyQuantityCombinedReport.getOpeningBalance() + dailyQuantityCombinedReport.getQuantityBought() - dailyQuantityCombinedReport.getQuantityDrawn());
            if (updateDailyQuantityCombinedReport)
                DailyQuantityCombinedReportDao.updateDailyQuantityCombinedReport(dailyQuantityCombinedReport);
            else
                DailyQuantityCombinedReportDao.saveDailyQuantityCombinedReport(dailyQuantityCombinedReport);

            int yearMonth = Utilities.toYearMonthFromDate(stockReceipt.getReceivedDate());

            MonthlyQuantityReport monthlyQuantityReport = MonthlyQuantityReportDao.getMonthlyQuantityReportsByStockAndYearAndMonthInMonths(stock, yearMonth);
            boolean updateMonthlyQuantityReport = true;
            if (monthlyQuantityReport == null) {
                updateMonthlyQuantityReport = false;
                monthlyQuantityReport = new MonthlyQuantityReport();
                monthlyQuantityReport.setReportDate(stockReceipt.getReceivedDate());
                monthlyQuantityReport.setUnit(stock.getUnit());
                monthlyQuantityReport.setYearAndMonthInMonths(yearMonth);
                monthlyQuantityReport.setRate(stock.getPriceOfReceivedStock());
                monthlyQuantityReport.setStock(stock);
            }
            monthlyQuantityReport.setQuantityBought(monthlyQuantityReport.getQuantityBought() + stockReceipt.getReceivedQuantity());
            monthlyQuantityReport.setClosingBalance(monthlyQuantityReport.getOpeningBalance() + monthlyQuantityReport.getQuantityBought() - monthlyQuantityReport.getQuantityDrawn());
            if (updateMonthlyQuantityReport)
                MonthlyQuantityReportDao.updateMonthlyQuantityReport(monthlyQuantityReport);
            else
                MonthlyQuantityReportDao.saveMonthlyQuantityReport(monthlyQuantityReport);

            MonthlyAmountReport monthlyAmountReport = MonthlyAmountReportDao.getMonthlyAmountReportByItemAndYearAndMonthInMonths(item, yearMonth);
            boolean updateMonthlyAmountReport = true;
            if (monthlyAmountReport == null) {
                updateMonthlyAmountReport = false;
                monthlyAmountReport = new MonthlyAmountReport();
                monthlyAmountReport.setReportDate(stockReceipt.getReceivedDate());
                monthlyAmountReport.setItem(item);
                monthlyAmountReport.setYearAndMonthInMonths(yearMonth);
            }
            monthlyAmountReport.setAmountBought(monthlyAmountReport.getAmountBought() + stockReceipt.getReceivedQuantity() * dailyQuantityReport.getRate());
            monthlyAmountReport.setClosingBalance(monthlyAmountReport.getOpeningBalance() + monthlyAmountReport.getAmountBought() - monthlyAmountReport.getAmountDrawn());
            if (updateMonthlyAmountReport)
                MonthlyAmountReportDao.updateMonthlyAmountReport(monthlyAmountReport);
            else
                MonthlyAmountReportDao.saveMonthlyAmountReport(monthlyAmountReport);

            MonthlyQuantityCombinedReport monthlyQuantityCombinedReport = MonthlyQuantityCombinedReportDao.getMonthlyQuantityCombinedReportsByItemAndYearAndMonthInMonthsAndUnit(item, yearMonth, stock.getUnit());
            boolean updateMonthlyQuantityCombinedReport = true;
            if (monthlyQuantityCombinedReport == null) {
                updateMonthlyQuantityCombinedReport = false;
                monthlyQuantityCombinedReport = new MonthlyQuantityCombinedReport();
                monthlyQuantityCombinedReport.setReportDate(stockReceipt.getReceivedDate());
                monthlyQuantityCombinedReport.setUnit(stock.getUnit());
                monthlyQuantityCombinedReport.setYearAndMonthInMonths(yearMonth);
                monthlyQuantityCombinedReport.setItem(item);
            }
            monthlyQuantityCombinedReport.setQuantityBought(monthlyQuantityCombinedReport.getQuantityBought() + stockReceipt.getReceivedQuantity());
            monthlyQuantityCombinedReport.setClosingBalance(monthlyQuantityCombinedReport.getOpeningBalance() + monthlyQuantityCombinedReport.getQuantityBought() - monthlyQuantityCombinedReport.getQuantityDrawn());
            if (updateMonthlyQuantityReport)
                monthlyQuantityCombinedReport.getSourceMonthlyQuantityReports().add(monthlyQuantityReport);
            if (updateMonthlyQuantityCombinedReport)
                MonthlyQuantityCombinedReportDao.updateMonthlyQuantityCombinedReport(monthlyQuantityCombinedReport);
            else
                MonthlyQuantityCombinedReportDao.saveMonthlyQuantityCombinedReport(monthlyQuantityCombinedReport);
        }
    }

    @ActionMethod("updateStockReceipt")
    public void updateStockReceipt() {

        if (validateBeforeFormSubmission()) {

            StockReceipt stockReceiptToUpdate = currentStockReceipt;
            double previousStockReceiptQuantity = stockReceiptToUpdate.getReceivedQuantity();
            loadFormDataIntoStockReceipt(stockReceiptToUpdate);
            double newStockReceiptQuantity = stockReceiptToUpdate.getReceivedQuantity();
            Stock stock = stockContainer.getStock();
            stockReceiptToUpdate.setReceivedValue(stockReceiptToUpdate.getReceivedQuantity() * stock.getPriceOfReceivedStock());
            double actualUseDifference = newStockReceiptQuantity - previousStockReceiptQuantity;

            stock.setQuantityOfReceivedStock(stock.getQuantityOfReceivedStock() + actualUseDifference);
            stock.setValueOfReceivedStock(stock.getQuantityOfReceivedStock() * stock.getPriceOfReceivedStock());
            stock.setQuantityOfBalancedStock(stock.getQuantityOfBalancedStock() + actualUseDifference);
            stock.setValueOfBalancedStock(stock.getQuantityOfBalancedStock() * stock.getPriceOfReceivedStock());

            StockReceiptDao.updateStockWithStockReceipt(stock, stockReceiptToUpdate);
            refreshTable();
            receiptTableView.getSelectionModel().select(stockReceiptToUpdate);
            loadStockReceiptDetails();

            Item item = stock.getItem();
            double difference = previousStockReceiptQuantity - newStockReceiptQuantity;

            DailyQuantityReport dailyQuantityReport = DailyQuantityReportDao.getDailyQuantityReportByStockAndDate(stock, stockReceiptToUpdate.getReceivedDate());
            dailyQuantityReport.setQuantityBought(dailyQuantityReport.getQuantityBought() - difference);
            dailyQuantityReport.setClosingBalance(dailyQuantityReport.getOpeningBalance() + dailyQuantityReport.getQuantityBought() - dailyQuantityReport.getQuantityDrawn());
            DailyQuantityReportDao.updateDailyQuantityReport(dailyQuantityReport);

            DailyAmountReport dailyAmountReport = DailyAmountReportDao.getDailyAmountReportByDateAndItem(stockReceiptToUpdate.getReceivedDate(), item);
            dailyAmountReport.setAmountBought(dailyAmountReport.getAmountBought() - difference * dailyQuantityReport.getRate());
            dailyAmountReport.setClosingBalance(dailyAmountReport.getOpeningBalance() + dailyAmountReport.getAmountBought() - dailyAmountReport.getAmountDrawn());
            DailyAmountReportDao.updateDailyAmountReport(dailyAmountReport);

            DailyQuantityCombinedReport dailyQuantityCombinedReport = DailyQuantityCombinedReportDao.getDailyQuantityCombinedReportByDateAndItemAndUnit(stockReceiptToUpdate.getReceivedDate(), item, stock.getUnit());
            dailyQuantityCombinedReport.setQuantityBought(dailyQuantityCombinedReport.getQuantityBought() - difference);
            dailyQuantityCombinedReport.setClosingBalance(dailyQuantityCombinedReport.getOpeningBalance() + dailyQuantityCombinedReport.getQuantityBought() - dailyQuantityCombinedReport.getQuantityDrawn());
            DailyQuantityCombinedReportDao.updateDailyQuantityCombinedReport(dailyQuantityCombinedReport);

            int yearMonth = Utilities.toYearMonthFromDate(stockReceiptToUpdate.getReceivedDate());

            MonthlyQuantityReport monthlyQuantityReport = MonthlyQuantityReportDao.getMonthlyQuantityReportsByStockAndYearAndMonthInMonths(stock, yearMonth);
            monthlyQuantityReport.setQuantityBought(monthlyQuantityReport.getQuantityBought() - difference);
            monthlyQuantityReport.setClosingBalance(monthlyQuantityReport.getOpeningBalance() + monthlyQuantityReport.getQuantityBought() - monthlyQuantityReport.getQuantityDrawn());
            MonthlyQuantityReportDao.updateMonthlyQuantityReport(monthlyQuantityReport);

            MonthlyAmountReport monthlyAmountReport = MonthlyAmountReportDao.getMonthlyAmountReportByItemAndYearAndMonthInMonths(item, yearMonth);
            monthlyAmountReport.setAmountBought(monthlyAmountReport.getAmountBought() - difference * dailyQuantityReport.getRate());
            monthlyAmountReport.setClosingBalance(monthlyAmountReport.getOpeningBalance() + monthlyAmountReport.getAmountBought() - monthlyAmountReport.getAmountDrawn());
            MonthlyAmountReportDao.updateMonthlyAmountReport(monthlyAmountReport);

            MonthlyQuantityCombinedReport monthlyQuantityCombinedReport = MonthlyQuantityCombinedReportDao.getMonthlyQuantityCombinedReportsByItemAndYearAndMonthInMonthsAndUnit(item, yearMonth, stock.getUnit());
            monthlyQuantityCombinedReport.setQuantityBought(monthlyQuantityCombinedReport.getQuantityBought() - difference);
            monthlyQuantityCombinedReport.setClosingBalance(monthlyQuantityCombinedReport.getOpeningBalance() + monthlyQuantityCombinedReport.getQuantityBought() - monthlyQuantityCombinedReport.getQuantityDrawn());
            MonthlyQuantityCombinedReportDao.updateMonthlyQuantityCombinedReport(monthlyQuantityCombinedReport);
        }
    }

    private void clearFormWarnings() {
        receivedDateDatePicker.setStyle("-fx-border-color: null");
        particularInput.setStyle("-fx-border-color: null");
        receivedQuantityInput.setStyle("-fx-border-color: null");
    }

    private boolean validateBeforeFormSubmission() {
        boolean isErrorFree = true;

        if (receivedDateDatePicker.getValue() == null) {
            receivedDateDatePicker.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }
        if (receivedQuantityInput.getText().isEmpty()) {
            receivedQuantityInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        return isErrorFree;
    }

    private void loadFormDataIntoStockReceipt(StockReceipt stockReceipt) {
        Date issuedDateUtilDate = Date.from(receivedDateDatePicker.getValue().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        stockReceipt.setReceivedDate(issuedDateUtilDate);
        stockReceipt.setParticular(particularInput.getText());
        stockReceipt.setReceivedQuantity(Double.parseDouble(receivedQuantityInput.getText()));
    }

    private void loadStockDataIntoForm(StockReceipt stockReceipt) {
        Instant instant = Instant.ofEpochMilli(stockReceipt.getReceivedDate().getTime());
        LocalDate localDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
        receivedDateDatePicker.setValue(localDate);
        particularInput.setText(stockReceipt.getParticular());
        receivedQuantityInput.setText(stockReceipt.getReceivedQuantity() + "");
    }

    private void refreshTable() {
        receiptTableView.getItems().clear();
        receiptTableView.getItems().setAll(stockContainer.getStock().getStockReceipts());
    }

    @ActionMethod("loadEditReceiptForm")
    public void loadEditStockReceiptForm() {
        prepareEditStockReceipt();
        loadNewStockReceiptGrid();
        loadStockDataIntoForm(currentStockReceipt);
    }

    private void prepareEditStockReceipt() {
        allocateReceiptText.setText("Edit Stock Receipt");
        receivedDateDatePicker.setDisable(true);
        createReceiptButton.setVisible(false);
        saveReceiptButton.setVisible(true);
        saveReceiptButton.toFront();
    }

    @ActionMethod("loadDeleteReceiptForm")
    public void loadDeleteReceiptForm() {
        loadDeleteReceiptGrid();
    }

    private void loadDeleteReceiptGrid() {
        enableThisGrid(confirmDeleteReceiptGrid);
    }

    @ActionMethod("confirmDeleteStockReceipt")
    public void confirmDeleteReceipt() {
        Stock stock = stockContainer.getStock();
        stock.getStockReceipts().remove(currentStockReceipt);
        double valueToAdjust = currentStockReceipt.getReceivedQuantity();

        stock.setQuantityOfReceivedStock(stock.getQuantityOfReceivedStock() - valueToAdjust);
        stock.setValueOfReceivedStock(stock.getQuantityOfReceivedStock() * stock.getPriceOfReceivedStock());
        stock.setQuantityOfBalancedStock(stock.getQuantityOfBalancedStock() - valueToAdjust);
        stock.setValueOfBalancedStock(stock.getQuantityOfBalancedStock() * stock.getPriceOfReceivedStock());

        Date receivedDate = currentStockReceipt.getReceivedDate();
        double receivedQuantity = currentStockReceipt.getReceivedQuantity();

        StockReceiptDao.deleteStockReceiptWithStock(currentStockReceipt, stock);
        refreshTable();
        loadNewStockReceiptForm();

        Item item = stock.getItem();

        DailyQuantityReport dailyQuantityReport = DailyQuantityReportDao.getDailyQuantityReportByStockAndDate(stock, receivedDate);
        dailyQuantityReport.setQuantityBought(dailyQuantityReport.getQuantityBought() - receivedQuantity);
        dailyQuantityReport.setClosingBalance(dailyQuantityReport.getOpeningBalance() + dailyQuantityReport.getQuantityBought() - dailyQuantityReport.getQuantityDrawn());
        DailyQuantityReportDao.updateDailyQuantityReport(dailyQuantityReport);

        DailyAmountReport dailyAmountReport = DailyAmountReportDao.getDailyAmountReportByDateAndItem(receivedDate, item);
        dailyAmountReport.setAmountBought(dailyAmountReport.getAmountBought() - receivedQuantity * dailyQuantityReport.getRate());
        dailyAmountReport.setClosingBalance(dailyAmountReport.getOpeningBalance() + dailyAmountReport.getAmountBought() - dailyAmountReport.getAmountDrawn());
        DailyAmountReportDao.updateDailyAmountReport(dailyAmountReport);

        DailyQuantityCombinedReport dailyQuantityCombinedReport = DailyQuantityCombinedReportDao.getDailyQuantityCombinedReportByDateAndItemAndUnit(receivedDate, item, stock.getUnit());
        dailyQuantityCombinedReport.setQuantityBought(dailyQuantityCombinedReport.getQuantityBought() - receivedQuantity);
        dailyQuantityCombinedReport.setClosingBalance(dailyQuantityCombinedReport.getOpeningBalance() + dailyQuantityCombinedReport.getQuantityBought() - dailyQuantityCombinedReport.getQuantityDrawn());
        DailyQuantityCombinedReportDao.updateDailyQuantityCombinedReport(dailyQuantityCombinedReport);

        int yearMonth = Utilities.toYearMonthFromDate(receivedDate);

        MonthlyQuantityReport monthlyQuantityReport = MonthlyQuantityReportDao.getMonthlyQuantityReportsByStockAndYearAndMonthInMonths(stock, yearMonth);
        monthlyQuantityReport.setQuantityBought(monthlyQuantityReport.getQuantityBought() - receivedQuantity);
        monthlyQuantityReport.setClosingBalance(monthlyQuantityReport.getOpeningBalance() + monthlyQuantityReport.getQuantityBought() - monthlyQuantityReport.getQuantityDrawn());
        MonthlyQuantityReportDao.updateMonthlyQuantityReport(monthlyQuantityReport);

        MonthlyAmountReport monthlyAmountReport = MonthlyAmountReportDao.getMonthlyAmountReportByItemAndYearAndMonthInMonths(item, yearMonth);
        monthlyAmountReport.setAmountBought(monthlyAmountReport.getAmountBought() - receivedQuantity * dailyQuantityReport.getRate());
        monthlyAmountReport.setClosingBalance(monthlyAmountReport.getOpeningBalance() + monthlyAmountReport.getAmountBought() - monthlyAmountReport.getAmountDrawn());
        MonthlyAmountReportDao.updateMonthlyAmountReport(monthlyAmountReport);

        MonthlyQuantityCombinedReport monthlyQuantityCombinedReport = MonthlyQuantityCombinedReportDao.getMonthlyQuantityCombinedReportsByItemAndYearAndMonthInMonthsAndUnit(item, yearMonth, stock.getUnit());
        monthlyQuantityCombinedReport.setQuantityBought(monthlyQuantityCombinedReport.getQuantityBought() - receivedQuantity);
        monthlyQuantityCombinedReport.setClosingBalance(monthlyQuantityCombinedReport.getOpeningBalance() + monthlyQuantityCombinedReport.getQuantityBought() - monthlyQuantityCombinedReport.getQuantityDrawn());
        MonthlyQuantityCombinedReportDao.updateMonthlyQuantityCombinedReport(monthlyQuantityCombinedReport);
    }

    @ActionMethod("cancelDeleteStockReceipt")
    public void cancelDeleteStockReceipt() {
        loadStockReceiptDetails();
    }

    private boolean isDeletable() {
        return currentStockReceipt.getReceivedDate().equals(Utilities.toUtilDateFromLocalDate(Utilities.toLocalDateFromUtilDate(new Date())));
    }
}
