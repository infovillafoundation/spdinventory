package org.bitbucket.infovillafoundation.spd.inventory.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 15/12/14.
 */

@NamedQueries({
        @NamedQuery(name = "MonthlyQuantityCombinedReport.findAll",
                query = "SELECT e FROM MonthlyQuantityCombinedReport e"),
        @NamedQuery(name = "MonthlyQuantityCombinedReport.findByYearAndMonthInMonths",
                query = "SELECT e FROM MonthlyQuantityCombinedReport e  WHERE e.yearAndMonthInMonths = :yearAndMonthInMonths"),
        @NamedQuery(name = "MonthlyQuantityCombinedReport.findByReportDate",
                query = "SELECT e FROM MonthlyQuantityCombinedReport e  WHERE e.reportDate = :reportDate"),
        @NamedQuery(name = "MonthlyQuantityCombinedReport.findByItemAndYearAndMonthInMonthsAndUnit",
                query = "SELECT e FROM MonthlyQuantityCombinedReport e  WHERE e.item = :item AND e.yearAndMonthInMonths = :yearAndMonthInMonths And e.unit = :unit")
})

@Entity
@Table(name = "monthly_quantity_combined_reports")
public class MonthlyQuantityCombinedReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "report_date")
    @Temporal(TemporalType.DATE)
    private Date reportDate;

    @Column(name = "ym")
    private int yearAndMonthInMonths;

    private String unit;

    @Column(name = "opening_balance")
    private double openingBalance;

    @Column(name = "buy")
    private double quantityBought;

    @Column(name = "draw")
    private double quantityDrawn;

    @Column(name = "closing_balance")
    private double closingBalance;

    @OneToMany
    @JoinTable(
            name = "monthly_quantity_reports_references",
            joinColumns = {@JoinColumn(name = "combined_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "monthly_id", referencedColumnName = "id")}
    )
    private List<MonthlyQuantityReport> sourceMonthlyQuantityReports;

    @OneToOne
    @JoinColumn(name = "item_id")
    private Item item;

    public MonthlyQuantityCombinedReport() {
        if (sourceMonthlyQuantityReports == null)
            sourceMonthlyQuantityReports = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getYearAndMonthInMonths() {
        return yearAndMonthInMonths;
    }

    public void setYearAndMonthInMonths(int yearAndMonthInMonths) {
        this.yearAndMonthInMonths = yearAndMonthInMonths;
    }

    public double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(double openingBalance) {
        this.openingBalance = openingBalance;
    }

    public double getQuantityBought() {
        return quantityBought;
    }

    public void setQuantityBought(double quantityBought) {
        this.quantityBought = quantityBought;
    }

    public double getQuantityDrawn() {
        return quantityDrawn;
    }

    public void setQuantityDrawn(double quantityDrawn) {
        this.quantityDrawn = quantityDrawn;
    }

    public double getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(double closingBalance) {
        this.closingBalance = closingBalance;
    }

    public List<MonthlyQuantityReport> getSourceMonthlyQuantityReports() {
        return sourceMonthlyQuantityReports;
    }

    public void setSourceMonthlyQuantityReports(List<MonthlyQuantityReport> sourceMonthlyQuantityReports) {
        this.sourceMonthlyQuantityReports = sourceMonthlyQuantityReports;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
