package org.bitbucket.infovillafoundation.spd.inventory.daos;

import org.bitbucket.infovillafoundation.spd.inventory.entities.DailyAmountReport;
import org.bitbucket.infovillafoundation.spd.inventory.entities.Item;
import org.bitbucket.infovillafoundation.spd.inventory.entities.Stock;
import org.bitbucket.infovillafoundation.spd.inventory.entities.StockReceipt;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 17/12/14.
 */
public class DailyAmountReportDao extends Dao {

    public static List<DailyAmountReport> getAllDailyAmountReports() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<DailyAmountReport> query = em.createNamedQuery("DailyAmountReport.findAll", DailyAmountReport.class);
        List<DailyAmountReport> dailyAmountReports = null;
        try {
            dailyAmountReports = query.getResultList();
        } catch (NoResultException e) {
        }
        return dailyAmountReports;
    }

    public static List<DailyAmountReport> getDailyAmountReportsByDate(Date reportDate) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<DailyAmountReport> query = em.createNamedQuery("DailyAmountReport.findByReportDate", DailyAmountReport.class);
        query.setParameter("reportDate", reportDate);
        List<DailyAmountReport> dailyAmountReports = null;
        try {
            dailyAmountReports = query.getResultList();
        } catch (NoResultException e) {
        }
        return dailyAmountReports;
    }

    public static DailyAmountReport getDailyAmountReportByDateAndItem(Date reportDate, Item item) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<DailyAmountReport> query = em.createNamedQuery("DailyAmountReport.findByReportDateAndItem", DailyAmountReport.class);
        query.setParameter("reportDate", reportDate);
        query.setParameter("item", item);
        DailyAmountReport dailyAmountReport = null;
        try {
            dailyAmountReport = query.getSingleResult();
        } catch (NoResultException e) {

        }
        return dailyAmountReport;
    }

    public static List<Item> getItemsByNameLike(String searchTerm) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Item> query = em.createNamedQuery("Item.findByNameLike", Item.class);
        query.setParameter("name", "%" + searchTerm + "%");
        List<Item> items = query.getResultList();
        return items;
    }

    public static void saveDailyAmountReport(DailyAmountReport dailyAmountReport) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(dailyAmountReport);
        em.getTransaction().commit();
        em.close();
    }

    public static void saveItemWithStock(Item item, Stock stock) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stock);
        em.persist(item);
        em.getTransaction().commit();
        em.close();
    }

    public static void saveItemWithStockAndStockReceipt(Item item, Stock stock, StockReceipt stockReceipt) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stockReceipt);
        em.persist(stock);
        em.persist(item);
        em.getTransaction().commit();
        em.close();
    }

    public static void updateDailyAmountReport(DailyAmountReport dailyAmountReport) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(dailyAmountReport);
        em.getTransaction().commit();
        em.close();
    }

    public static void deleteItem(Item item) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.contains(item) ? item : em.merge(item));
        em.getTransaction().commit();
        em.close();
    }
}
