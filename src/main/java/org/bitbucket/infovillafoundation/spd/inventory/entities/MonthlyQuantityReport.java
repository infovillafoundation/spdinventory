package org.bitbucket.infovillafoundation.spd.inventory.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Sandah Aung on 15/12/14.
 */

@NamedQueries({
        @NamedQuery(name = "MonthlyQuantityReport.findAll",
                query = "SELECT e FROM MonthlyQuantityReport e"),
        @NamedQuery(name = "MonthlyQuantityReport.findByYearAndMonthInMonthsDesc",
                query = "SELECT e FROM MonthlyQuantityReport e ORDER BY yearAndMonthInMonths DESC"),
        @NamedQuery(name = "MonthlyQuantityReport.findByReportDate",
                query = "SELECT e FROM MonthlyQuantityReport e WHERE e.reportDate = :reportDate"),
        @NamedQuery(name = "MonthlyQuantityReport.findByYearAndMonthInMonths",
                query = "SELECT e FROM MonthlyQuantityReport e WHERE yearAndMonthInMonths = :yearAndMonthInMonths"),
        @NamedQuery(name = "MonthlyQuantityReport.findByStockAndYearAndMonthInMonths",
                query = "SELECT e FROM MonthlyQuantityReport e WHERE e.stock = :stock AND yearAndMonthInMonths = :yearAndMonthInMonths")
})

@Entity
@Table(name = "monthly_quantity_reports")
public class MonthlyQuantityReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "report_date")
    @Temporal(TemporalType.DATE)
    private Date reportDate;

    @Column(name = "ym")
    private int yearAndMonthInMonths;

    private String unit;

    @Column(name = "opening_balance")
    private double openingBalance;

    private double rate;

    @Column(name = "buy")
    private double quantityBought;

    @Column(name = "draw")
    private double quantityDrawn;

    @Column(name = "closing_balance")
    private double closingBalance;

    @OneToOne
    @JoinColumn(name = "stock_id")
    private Stock stock;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public int getYearAndMonthInMonths() {
        return yearAndMonthInMonths;
    }

    public void setYearAndMonthInMonths(int yearAndMonthInMonths) {
        this.yearAndMonthInMonths = yearAndMonthInMonths;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(double openingBalance) {
        this.openingBalance = openingBalance;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getQuantityBought() {
        return quantityBought;
    }

    public void setQuantityBought(double quantityBought) {
        this.quantityBought = quantityBought;
    }

    public double getQuantityDrawn() {
        return quantityDrawn;
    }

    public void setQuantityDrawn(double quantityDrawn) {
        this.quantityDrawn = quantityDrawn;
    }

    public double getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(double closingBalance) {
        this.closingBalance = closingBalance;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }
}
