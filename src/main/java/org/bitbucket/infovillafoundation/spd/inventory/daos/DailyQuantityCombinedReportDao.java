package org.bitbucket.infovillafoundation.spd.inventory.daos;

import org.bitbucket.infovillafoundation.spd.inventory.entities.DailyQuantityCombinedReport;
import org.bitbucket.infovillafoundation.spd.inventory.entities.Item;
import org.bitbucket.infovillafoundation.spd.inventory.entities.Stock;
import org.bitbucket.infovillafoundation.spd.inventory.entities.StockReceipt;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 17/12/14.
 */
public class DailyQuantityCombinedReportDao extends Dao {

    public static List<DailyQuantityCombinedReport> getAllDailyQuantityCombinedReports() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<DailyQuantityCombinedReport> query = em.createNamedQuery("DailyQuantityCombinedReport.findAll", DailyQuantityCombinedReport.class);
        List<DailyQuantityCombinedReport> dailyQuantityCombinedReports = query.getResultList();
        return dailyQuantityCombinedReports;
    }

    public static List<DailyQuantityCombinedReport> getDailyQuantityCombinedReportsByDate(Date reportDate) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<DailyQuantityCombinedReport> query = em.createNamedQuery("DailyQuantityCombinedReport.findByReportDate", DailyQuantityCombinedReport.class);
        query.setParameter("reportDate", reportDate);
        List<DailyQuantityCombinedReport> dailyQuantityCombinedReports = query.getResultList();
        return dailyQuantityCombinedReports;
    }

    public static DailyQuantityCombinedReport getDailyQuantityCombinedReportsByDateAndItem(Date reportDate, Item item) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<DailyQuantityCombinedReport> query = em.createNamedQuery("DailyQuantityCombinedReport.findByReportDateAndItem", DailyQuantityCombinedReport.class);
        query.setParameter("reportDate", reportDate);
        query.setParameter("item", item);
        DailyQuantityCombinedReport dailyQuantityCombinedReport = null;
        try {
            dailyQuantityCombinedReport = query.getSingleResult();
        } catch (NoResultException e) {

        }
        return dailyQuantityCombinedReport;
    }

    public static DailyQuantityCombinedReport getDailyQuantityCombinedReportByDateAndItemAndUnit(Date reportDate, Item item, String unit) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<DailyQuantityCombinedReport> query = em.createNamedQuery("DailyQuantityCombinedReport.findByReportDateAndItemAndUnit", DailyQuantityCombinedReport.class);
        query.setParameter("reportDate", reportDate);
        query.setParameter("item", item);
        query.setParameter("unit", unit);
        DailyQuantityCombinedReport dailyQuantityCombinedReport = null;
        try {
            dailyQuantityCombinedReport = query.getSingleResult();
        } catch (NoResultException e) {

        }
        return dailyQuantityCombinedReport;
    }

    public static void saveDailyQuantityCombinedReport(DailyQuantityCombinedReport dailyQuantityCombinedReport) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(dailyQuantityCombinedReport);
        em.getTransaction().commit();
        em.close();
    }

    public static void saveItemWithStock(Item item, Stock stock) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stock);
        em.persist(item);
        em.getTransaction().commit();
        em.close();
    }

    public static void saveItemWithStockAndStockReceipt(Item item, Stock stock, StockReceipt stockReceipt) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stockReceipt);
        em.persist(stock);
        em.persist(item);
        em.getTransaction().commit();
        em.close();
    }

    public static void updateDailyQuantityCombinedReport(DailyQuantityCombinedReport dailyQuantityCombinedReport) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(dailyQuantityCombinedReport);
        em.getTransaction().commit();
        em.close();
    }

    public static void deleteItem(Item item) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.contains(item) ? item : em.merge(item));
        em.getTransaction().commit();
        em.close();
    }
}
