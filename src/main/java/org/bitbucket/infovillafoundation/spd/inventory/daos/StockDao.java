package org.bitbucket.infovillafoundation.spd.inventory.daos;

import org.bitbucket.infovillafoundation.spd.inventory.entities.Item;
import org.bitbucket.infovillafoundation.spd.inventory.entities.Stock;
import org.bitbucket.infovillafoundation.spd.inventory.entities.StockReceipt;

import javax.persistence.EntityManager;

/**
 * Created by Sandah Aung on 17/12/14.
 */
public class StockDao extends Dao {

    public static void saveStock(Stock stock) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Item item = stock.getItem();
        em.merge(item);
        em.persist(stock.getStockReceipts().get(0));
        em.persist(stock);
        em.getTransaction().commit();
        em.close();
    }

    public static void updateStock(Stock stock) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(stock);
        em.getTransaction().commit();
        em.close();
    }

    public static void updateStockWithStockReceipt(Stock stock, StockReceipt stockReceipt) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stockReceipt);
        em.merge(stock);
        em.getTransaction().commit();
        em.close();
    }

    public static void deleteStock(Stock stock) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.contains(stock) ? stock : em.merge(stock));
        em.getTransaction().commit();
        em.close();
    }
}
