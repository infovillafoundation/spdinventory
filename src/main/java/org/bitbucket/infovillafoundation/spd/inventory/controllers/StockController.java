package org.bitbucket.infovillafoundation.spd.inventory.controllers;

import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.action.BackAction;
import io.datafx.controller.flow.context.ActionHandler;
import io.datafx.controller.flow.context.FlowActionHandler;
import io.datafx.controller.util.VetoException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.bitbucket.infovillafoundation.spd.inventory.daos.*;
import org.bitbucket.infovillafoundation.spd.inventory.entities.*;
import org.bitbucket.infovillafoundation.spd.inventory.models.ItemContainer;
import org.bitbucket.infovillafoundation.spd.inventory.models.OpenCloseStatus;
import org.bitbucket.infovillafoundation.spd.inventory.models.StockContainer;
import org.bitbucket.infovillafoundation.spd.inventory.models.StockReceiptStatus;
import org.bitbucket.infovillafoundation.spd.inventory.utils.Utilities;
import org.controlsfx.control.MasterDetailPane;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 26/12/14.
 */

@FXMLController(value = "/fxml/stocks.fxml", title = "Stocks")
public class StockController {

    @ActionHandler
    private FlowActionHandler handler;

    @FXML
    private MasterDetailPane pane;

    @FXML
    private GridPane showStockGrid;

    @FXML
    private GridPane addStockGrid;

    @FXML
    private GridPane fillStockGrid;

    @FXML
    private GridPane confirmDeleteStockGrid;

    @FXML
    @ActionTrigger("confirmDeleteStock")
    private Button confirmDeleteStockButton;

    @FXML
    @ActionTrigger("cancelDeleteStock")
    private Button cancelDeleteStockButton;


    @Inject
    private ItemContainer itemContainer;

    @Inject
    private StockContainer stockContainer;

    @FXML
    @BackAction
    private Button backButton;

    @FXML
    @ActionTrigger("loadNewStockForm")
    private Button newStockButton;

    @FXML
    @ActionTrigger("loadDetailedReport")
    private Button detailedReportButton;

    @FXML
    @ActionTrigger("loadFillStockForm")
    private Button fillStockButton;

    @FXML
    @ActionTrigger("loadEditStockForm")
    private Button editStockButton;

    @FXML
    @ActionTrigger("loadConfirmDeleteStock")
    private Button deleteStockButton;

    @FXML
    private Text itemName;

    @FXML
    private TableView<Stock> stockTableView;

    @FXML
    private TableColumn<Stock, Date> stockReceivedDateColumn;

    @FXML
    @ActionTrigger("loadStockDetails")
    private MenuItem detailsMenuItem;

    @FXML
    @ActionTrigger("loadStockReceipts")
    private MenuItem receiptsMenuItem;

    @FXML
    @ActionTrigger("setAsActiveStock")
    private MenuItem setAsActiveStockMenuItem;

    @FXML
    @ActionTrigger("closeStock")
    private MenuItem closeStockMenuItem;

    @FXML
    private Text addStockText;

    @FXML
    private TextField quantityInput;

    @FXML
    private TextField requiredQuantityInput;

    @FXML
    private TextField unitInput;

    @FXML
    private TextField priceInput;

    @FXML
    private TextField remarkInput;

    @FXML
    private CheckBox isOpeningStockCheckBox;

    @FXML
    private DatePicker receivedDateDatePicker;

    @FXML
    private ContextMenu tableContextMenu;

    @FXML
    @ActionTrigger("createStock")
    private Button createButton;

    @FXML
    @ActionTrigger("updateStock")
    private Button saveButton;

    @FXML
    @ActionTrigger("clear")
    private Button clearButton;

    @FXML
    private Text receivedDateText;

    @FXML
    private Text requiredLevelText;

    @FXML
    private Text receivedQuantityText;

    @FXML
    private Text unitText;

    @FXML
    private Text receivedPriceText;

    @FXML
    private Text receivedValueText;

    @FXML
    private Text issuedQuantityText;

    @FXML
    private Text issuedValueText;

    @FXML
    private Text balancedQuantityText;

    @FXML
    private Text balancedValueText;

    @FXML
    private Text activeStockText;

    @FXML
    private Text openCloseText;

    @FXML
    private Text remarkText;

    @FXML
    private DatePicker filledDateDatePicker;

    @FXML
    private TextField particularInput;

    @FXML
    private TextField filledQuantityInput;

    @FXML
    @ActionTrigger("createFilledStock")
    private Button fillStockCreateButton;

    @FXML
    @ActionTrigger("clearFilledStock")
    private Button fillStockClearButton;

    private Stock currentStock;


    @PostConstruct
    public void init() {
        itemName.setText(itemContainer.getItem().getName());
        loadTable();
        setupRowClickListener();
        loadNewStockForm();
        setupTableDoubleClickListener();
        setCellFactories();
        setupDateConverter();
        setupBackShortcutListener();
        setupContextMenuListeners();
        setupValidators();
        detectNullActiveStock();
    }

    private void setCellFactories() {
        stockReceivedDateColumn.setCellFactory(new Callback<TableColumn<Stock, Date>, TableCell<Stock, Date>>() {

            @Override
            public TableCell<Stock, Date> call(TableColumn<Stock, Date> p) {
                return new TableCell<Stock, Date>() {

                    @Override
                    protected void updateItem(Date item, boolean empty) {
                        super.updateItem(item, empty);

                        if (!empty) {
                            setText(Utilities.fromDate(item));
                        } else {
                            setText(null);
                        }
                    }

                };
            }

        });
    }

    private void detectNullActiveStock() {
        if (itemContainer.getItem().getActiveStock() == null)
            stockTableView.setStyle("-fx-border-color: red");
        else
            stockTableView.setStyle("-fx-border-color: null");
    }

    private void loadTable() {
        stockTableView.getItems().setAll(itemContainer.getItem().getStocks());
        stockTableView.getSelectionModel().select(stockContainer.getStock());
    }

    private void setupBackShortcutListener() {
        final KeyCombination ctrlB = new KeyCodeCombination(KeyCode.B, KeyCombination.CONTROL_DOWN);
        pane.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (ctrlB.match(event)) {
                    try {
                        handler.navigateBack();
                    } catch (VetoException e) {
                    } catch (FlowException e) {
                    }
                }
            }
        });
    }

    private void setupTableDoubleClickListener() {
        stockTableView.setRowFactory(tv -> {
            TableRow<Stock> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Stock rowData = row.getItem();
                    loadStockIssueController(rowData);
                }
            });
            return row;
        });
    }

    private void setupRowClickListener() {
        stockTableView
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(new ChangeListener<Stock>() {
                                 @Override
                                 public void changed(ObservableValue<? extends Stock> observable, Stock oldValue, Stock newValue) {
                                     currentStock = newValue;
                                     if (currentStock == null) {
                                         loadNewStockForm();
                                         editStockButton.setVisible(false);
                                         fillStockButton.setVisible(false);
                                     } else {
                                         loadStockDetails();
                                         editStockButton.setVisible(true);
                                         fillStockButton.setVisible(true);
                                         enableDisableDeleteButton(newValue);
                                     }

                                 }
                             }

                );
    }

    private void setupValidators() {

        receivedDateDatePicker.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (receivedDateDatePicker.getValue() == null) {
                    receivedDateDatePicker.setStyle("-fx-border-color: red");
                } else {
                    receivedDateDatePicker.setStyle("-fx-border-color: null");
                }
            }
        });

        quantityInput.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (quantityInput.getText().isEmpty())
                    quantityInput.setStyle("-fx-border-color: red");
                else
                    quantityInput.setStyle("-fx-border-color: null");
            }
        });

        requiredQuantityInput.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (requiredQuantityInput.getText().isEmpty())
                    requiredQuantityInput.setStyle("-fx-border-color: red");
                else
                    requiredQuantityInput.setStyle("-fx-border-color: null");
            }
        });

        unitInput.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (unitInput.getText().isEmpty())
                    unitInput.setStyle("-fx-border-color: red");
                else
                    unitInput.setStyle("-fx-border-color: null");
            }
        });

        priceInput.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (priceInput.getText().isEmpty())
                    priceInput.setStyle("-fx-border-color: red");
                else
                    priceInput.setStyle("-fx-border-color: null");
            }
        });
    }

    private void setupDateConverter() {
        String pattern = "dd/M/yyyy";
        receivedDateDatePicker.setPromptText(pattern.toLowerCase());
        receivedDateDatePicker.setConverter(new StringConverter<LocalDate>() {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateTimeFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateTimeFormatter);

                } else {
                    return null;
                }
            }
        });
    }

    private void enableDisableDeleteButton(Stock stock) {
        if (isDeletable(stock)) {
            deleteStockButton.setVisible(true);
        } else {
            deleteStockButton.setVisible(false);
        }
    }

    private void setupContextMenuListeners() {
        tableContextMenu.setOnShowing(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                OpenCloseStatus openCloseStatus = currentStock.getOpenCloseStatus();
                if (openCloseStatus == OpenCloseStatus.CLOSE)
                    closeStockMenuItem.setText("Reopen Stock");
            }
        });
    }

    @ActionMethod("loadStockDetails")
    public void loadStockDetails() {
        loadStockDetailsGrid();
        loadStockIntoShowGrid();
    }

    @ActionMethod("loadConfirmDeleteStock")
    public void loadDeleteStockForm() {
        loadDeleteStockGrid();
    }

    private void loadDeleteStockGrid() {
        enableThisGrid(confirmDeleteStockGrid);
    }

    private void loadStockDetailsGrid() {
        enableThisGrid(showStockGrid);
    }

    private void enableThisGrid(GridPane grid) {
        addStockGrid.setVisible(false);
        showStockGrid.setVisible(false);
        fillStockGrid.setVisible(false);
        confirmDeleteStockGrid.setVisible(false);
        grid.setVisible(true);
        grid.toFront();
    }

    private void loadStockIntoShowGrid() {
        receivedDateText.setText(Utilities.fromDate(currentStock.getReceivedDate()));
        requiredLevelText.setText(currentStock.getRequiredStockLevel() + " " + currentStock.getUnit());
        receivedQuantityText.setText(currentStock.getQuantityOfReceivedStock() + " " + currentStock.getUnit());
        unitText.setText(currentStock.getUnit());
        receivedPriceText.setText(currentStock.getPriceOfReceivedStock() + "");
        receivedValueText.setText(currentStock.getValueOfReceivedStock() + "");
        issuedQuantityText.setText(currentStock.getQuantityOfIssuedStock() + "");
        issuedValueText.setText(currentStock.getValueOfIssuedStock() + "");
        balancedQuantityText.setText(currentStock.getQuantityOfBalancedStock() + "");
        balancedValueText.setText(currentStock.getValueOfBalancedStock() + "");
        activeStockText.setText(currentStock.getActiveStockOwner() == null ? "No" : "Yes");
        openCloseText.setText(currentStock.getOpenCloseStatus() == OpenCloseStatus.OPEN ? "Open" : "Close");
        remarkText.setText(currentStock.getRemark());
    }

    @ActionMethod("loadNewStockForm")
    public void loadNewStockForm() {
        prepareNewStock();
        loadNewStockGrid();
    }

    private void prepareNewStock() {
        addStockText.setText("Add Stock");
        receivedDateDatePicker.setValue(Utilities.toLocalDateFromUtilDate(new Date()));
        saveButton.setVisible(false);
        createButton.setVisible(true);
        createButton.toFront();
    }

    private void loadNewStockGrid() {
        enableThisGrid(addStockGrid);
        unitInput.setText(itemContainer.getItem().getAssociatedUnit());
        requiredQuantityInput.setText("0");
        receivedDateDatePicker.setValue(Utilities.toLocalDateFromUtilDate(new Date()));
    }

    @ActionMethod("clear")
    public void clear() {
        clearFields();
    }

    private void clearFields() {
        receivedDateDatePicker.setValue(null);
        quantityInput.clear();
        requiredQuantityInput.clear();
        unitInput.clear();
        priceInput.clear();
        remarkInput.clear();
        isOpeningStockCheckBox.setSelected(false);
        clearFormWarnings();
    }

    @ActionMethod("createStock")
    public void createStock() {

        boolean isOpeningStock = isOpeningStockCheckBox.selectedProperty().getValue().booleanValue();

        if (validateBeforeFormSubmission()) {
            Stock stock = new Stock();
            loadFormDataIntoStock(stock);
            stock.setValueOfReceivedStock(stock.getQuantityOfReceivedStock() * stock.getPriceOfReceivedStock());
            stock.setQuantityOfBalancedStock(stock.getQuantityOfReceivedStock());
            stock.setValueOfBalancedStock(stock.getQuantityOfBalancedStock() * stock.getPriceOfReceivedStock());
            Item item = itemContainer.getItem();
            stock.setItem(itemContainer.getItem());
            StockReceipt stockReceipt = new StockReceipt();
            if (isOpeningStock) {
                stockReceipt.setStockReceiptStatus(StockReceiptStatus.OPENING);
                stockReceipt.setParticular("Opening Balance");
            } else {
                stockReceipt.setParticular("Initial Stock");
            }
            stockReceipt.setReceivedQuantity(stock.getQuantityOfReceivedStock());
            stockReceipt.setReceivedDate(stock.getReceivedDate());
            stock.getStockReceipts().add(stockReceipt);
            List<Stock> stocks = item.getStocks();
            stocks.add(stock);
            item.setStocks(stocks);
            StockDao.saveStock(stock);

            refreshTable();
            currentStock = stock;
            stockTableView.getSelectionModel().select(stock);
            clearFields();

            if (item.getStocks().size() == 1)
                setAsActiveStock();

            if (item.getStocks().size() == 1) {
                if (isOpeningStock) {
                    DailyQuantityReport dailyQuantityReport = new DailyQuantityReport();
                    dailyQuantityReport.setReportDate(stock.getReceivedDate());
                    dailyQuantityReport.setOpeningBalance(stock.getQuantityOfReceivedStock());
                    dailyQuantityReport.setUnit(stock.getUnit());
                    dailyQuantityReport.setRate(stock.getPriceOfReceivedStock());
                    dailyQuantityReport.setClosingBalance(dailyQuantityReport.getOpeningBalance() + dailyQuantityReport.getQuantityBought() - dailyQuantityReport.getQuantityDrawn());
                    dailyQuantityReport.setStock(stock);
                    DailyQuantityReportDao.saveDailyQuantityReport(dailyQuantityReport);

                    DailyAmountReport dailyAmountReport = new DailyAmountReport();
                    dailyAmountReport.setReportDate(stock.getReceivedDate());
                    dailyAmountReport.setOpeningBalance(dailyQuantityReport.getOpeningBalance() * dailyQuantityReport.getRate());
                    dailyAmountReport.setClosingBalance(dailyAmountReport.getOpeningBalance() + dailyAmountReport.getAmountBought() - dailyAmountReport.getAmountDrawn());
                    dailyAmountReport.setItem(item);
                    DailyAmountReportDao.saveDailyAmountReport(dailyAmountReport);

                    DailyQuantityCombinedReport dailyQuantityCombinedReport = new DailyQuantityCombinedReport();
                    dailyQuantityCombinedReport.setReportDate(stock.getReceivedDate());
                    dailyQuantityCombinedReport.setOpeningBalance(stock.getQuantityOfReceivedStock());
                    dailyQuantityCombinedReport.setClosingBalance(dailyQuantityCombinedReport.getOpeningBalance() + dailyQuantityCombinedReport.getQuantityBought() - dailyQuantityCombinedReport.getQuantityDrawn());
                    dailyQuantityCombinedReport.setUnit(stock.getUnit());
                    dailyQuantityCombinedReport.getSourceDailyQuantityReports().add(dailyQuantityReport);
                    dailyQuantityCombinedReport.setItem(item);
                    DailyQuantityCombinedReportDao.saveDailyQuantityCombinedReport(dailyQuantityCombinedReport);

                    int yearMonth = Utilities.toYearMonthFromDate(stock.getReceivedDate());

                    MonthlyQuantityReport monthlyQuantityReport = new MonthlyQuantityReport();
                    monthlyQuantityReport.setReportDate(stock.getReceivedDate());
                    monthlyQuantityReport.setOpeningBalance(stock.getQuantityOfReceivedStock());
                    monthlyQuantityReport.setClosingBalance(monthlyQuantityReport.getOpeningBalance() + monthlyQuantityReport.getQuantityBought() - monthlyQuantityReport.getQuantityDrawn());
                    monthlyQuantityReport.setRate(stock.getPriceOfReceivedStock());
                    monthlyQuantityReport.setUnit(stock.getUnit());
                    monthlyQuantityReport.setStock(stock);
                    monthlyQuantityReport.setYearAndMonthInMonths(yearMonth);
                    MonthlyQuantityReportDao.saveMonthlyQuantityReport(monthlyQuantityReport);

                    MonthlyAmountReport monthlyAmountReport = new MonthlyAmountReport();
                    monthlyAmountReport.setReportDate(stock.getReceivedDate());
                    monthlyAmountReport.setOpeningBalance(monthlyQuantityReport.getOpeningBalance() * monthlyQuantityReport.getRate());
                    monthlyAmountReport.setClosingBalance(monthlyAmountReport.getOpeningBalance() + monthlyAmountReport.getAmountBought() - monthlyAmountReport.getAmountDrawn());
                    monthlyAmountReport.setItem(item);
                    monthlyAmountReport.setYearAndMonthInMonths(yearMonth);
                    MonthlyAmountReportDao.saveMonthlyAmountReport(monthlyAmountReport);

                    MonthlyQuantityCombinedReport monthlyQuantityCombinedReport = new MonthlyQuantityCombinedReport();
                    monthlyQuantityCombinedReport.setReportDate(stock.getReceivedDate());
                    monthlyQuantityCombinedReport.setOpeningBalance(stock.getQuantityOfReceivedStock());
                    monthlyQuantityCombinedReport.setUnit(stock.getUnit());
                    monthlyQuantityCombinedReport.setClosingBalance(monthlyQuantityCombinedReport.getOpeningBalance() + monthlyQuantityCombinedReport.getQuantityBought() - monthlyQuantityCombinedReport.getQuantityDrawn());
                    monthlyQuantityCombinedReport.getSourceMonthlyQuantityReports().add(monthlyQuantityReport);
                    monthlyQuantityCombinedReport.setYearAndMonthInMonths(yearMonth);
                    monthlyQuantityCombinedReport.setItem(item);
                    MonthlyQuantityCombinedReportDao.saveMonthlyQuantityCombinedReport(monthlyQuantityCombinedReport);
                } else {
                    DailyQuantityReport dailyQuantityReport = new DailyQuantityReport();
                    dailyQuantityReport.setReportDate(stock.getReceivedDate());
                    dailyQuantityReport.setQuantityBought(stock.getQuantityOfReceivedStock());
                    dailyQuantityReport.setUnit(stock.getUnit());
                    dailyQuantityReport.setRate(stock.getPriceOfReceivedStock());
                    dailyQuantityReport.setClosingBalance(dailyQuantityReport.getOpeningBalance() + dailyQuantityReport.getQuantityBought() - dailyQuantityReport.getQuantityDrawn());
                    dailyQuantityReport.setStock(stock);
                    DailyQuantityReportDao.saveDailyQuantityReport(dailyQuantityReport);

                    DailyAmountReport dailyAmountReport = new DailyAmountReport();
                    dailyAmountReport.setReportDate(stock.getReceivedDate());
                    dailyAmountReport.setAmountBought(dailyQuantityReport.getQuantityBought() * dailyQuantityReport.getRate());
                    dailyAmountReport.setClosingBalance(dailyAmountReport.getOpeningBalance() + dailyAmountReport.getAmountBought() - dailyQuantityReport.getQuantityDrawn());
                    dailyAmountReport.setItem(item);
                    DailyAmountReportDao.saveDailyAmountReport(dailyAmountReport);

                    DailyQuantityCombinedReport dailyQuantityCombinedReport = new DailyQuantityCombinedReport();
                    dailyQuantityCombinedReport.setReportDate(stock.getReceivedDate());
                    dailyQuantityCombinedReport.setQuantityBought(stock.getQuantityOfReceivedStock());
                    dailyQuantityCombinedReport.setUnit(stock.getUnit());
                    dailyQuantityCombinedReport.setClosingBalance(dailyQuantityCombinedReport.getOpeningBalance() + dailyQuantityCombinedReport.getQuantityBought() - dailyQuantityCombinedReport.getQuantityDrawn());
                    dailyQuantityCombinedReport.getSourceDailyQuantityReports().add(dailyQuantityReport);
                    dailyQuantityCombinedReport.setItem(item);
                    DailyQuantityCombinedReportDao.saveDailyQuantityCombinedReport(dailyQuantityCombinedReport);

                    int yearMonth = Utilities.toYearMonthFromDate(stock.getReceivedDate());

                    MonthlyQuantityReport monthlyQuantityReport = new MonthlyQuantityReport();
                    monthlyQuantityReport.setReportDate(stock.getReceivedDate());
                    monthlyQuantityReport.setQuantityBought(stock.getQuantityOfReceivedStock());
                    monthlyQuantityReport.setClosingBalance(monthlyQuantityReport.getOpeningBalance() + monthlyQuantityReport.getQuantityBought() - monthlyQuantityReport.getQuantityDrawn());
                    monthlyQuantityReport.setRate(stock.getPriceOfReceivedStock());
                    monthlyQuantityReport.setUnit(stock.getUnit());
                    monthlyQuantityReport.setStock(stock);
                    monthlyQuantityReport.setYearAndMonthInMonths(yearMonth);
                    MonthlyQuantityReportDao.saveMonthlyQuantityReport(monthlyQuantityReport);

                    MonthlyAmountReport monthlyAmountReport = new MonthlyAmountReport();
                    monthlyAmountReport.setReportDate(stock.getReceivedDate());
                    monthlyAmountReport.setAmountBought(monthlyQuantityReport.getQuantityBought() * monthlyQuantityReport.getRate());
                    monthlyAmountReport.setClosingBalance(monthlyAmountReport.getOpeningBalance() + monthlyAmountReport.getAmountBought() - monthlyAmountReport.getAmountDrawn());
                    monthlyAmountReport.setItem(item);
                    monthlyAmountReport.setYearAndMonthInMonths(yearMonth);
                    MonthlyAmountReportDao.saveMonthlyAmountReport(monthlyAmountReport);

                    MonthlyQuantityCombinedReport monthlyQuantityCombinedReport = new MonthlyQuantityCombinedReport();
                    monthlyQuantityCombinedReport.setReportDate(stock.getReceivedDate());
                    monthlyQuantityCombinedReport.setQuantityBought(stock.getQuantityOfReceivedStock());
                    monthlyQuantityCombinedReport.setUnit(stock.getUnit());
                    monthlyQuantityCombinedReport.setClosingBalance(monthlyQuantityCombinedReport.getOpeningBalance() + monthlyQuantityCombinedReport.getQuantityBought() - monthlyQuantityCombinedReport.getQuantityDrawn());
                    monthlyQuantityCombinedReport.getSourceMonthlyQuantityReports().add(monthlyQuantityReport);
                    monthlyQuantityCombinedReport.setYearAndMonthInMonths(yearMonth);
                    monthlyQuantityCombinedReport.setItem(item);
                    MonthlyQuantityCombinedReportDao.saveMonthlyQuantityCombinedReport(monthlyQuantityCombinedReport);
                }
            } else {

                if (isOpeningStock) {

                    int yearMonth = Utilities.toYearMonthFromDate(stock.getReceivedDate());

                    DailyQuantityReport dailyQuantityReport = new DailyQuantityReport();
                    dailyQuantityReport.setReportDate(stock.getReceivedDate());
                    dailyQuantityReport.setOpeningBalance(stock.getQuantityOfReceivedStock());
                    dailyQuantityReport.setUnit(stock.getUnit());
                    dailyQuantityReport.setRate(stock.getPriceOfReceivedStock());
                    dailyQuantityReport.setClosingBalance(dailyQuantityReport.getOpeningBalance() + dailyQuantityReport.getQuantityBought() - dailyQuantityReport.getQuantityDrawn());
                    dailyQuantityReport.setStock(stock);
                    DailyQuantityReportDao.saveDailyQuantityReport(dailyQuantityReport);

                    MonthlyQuantityReport monthlyQuantityReport = new MonthlyQuantityReport();
                    monthlyQuantityReport.setReportDate(stock.getReceivedDate());
                    monthlyQuantityReport.setOpeningBalance(stock.getQuantityOfReceivedStock());
                    monthlyQuantityReport.setClosingBalance(monthlyQuantityReport.getOpeningBalance() + monthlyQuantityReport.getQuantityBought() - monthlyQuantityReport.getQuantityDrawn());
                    monthlyQuantityReport.setRate(stock.getPriceOfReceivedStock());
                    monthlyQuantityReport.setUnit(stock.getUnit());
                    monthlyQuantityReport.setStock(stock);
                    monthlyQuantityReport.setYearAndMonthInMonths(yearMonth);
                    MonthlyQuantityReportDao.saveMonthlyQuantityReport(monthlyQuantityReport);


                    DailyAmountReport dailyAmountReport = DailyAmountReportDao.getDailyAmountReportByDateAndItem(stock.getReceivedDate(), item);
                    boolean updateDailyAmountReport = true;
                    if (dailyAmountReport == null) {
                        dailyAmountReport = new DailyAmountReport();
                        updateDailyAmountReport = false;
                    }
                    dailyAmountReport.setReportDate(stock.getReceivedDate());
                    dailyAmountReport.setItem(item);
                    if (updateDailyAmountReport) {
                        dailyAmountReport.setOpeningBalance(dailyAmountReport.getOpeningBalance() + dailyQuantityReport.getOpeningBalance() * dailyQuantityReport.getRate());
                        dailyAmountReport.setClosingBalance(dailyAmountReport.getOpeningBalance() + dailyAmountReport.getAmountBought() - dailyAmountReport.getAmountDrawn());
                        DailyAmountReportDao.updateDailyAmountReport(dailyAmountReport);
                    } else {
                        dailyAmountReport.setOpeningBalance(dailyQuantityReport.getOpeningBalance() * dailyQuantityReport.getRate());
                        dailyAmountReport.setClosingBalance(dailyAmountReport.getOpeningBalance() + dailyAmountReport.getAmountBought() - dailyAmountReport.getAmountDrawn());
                        DailyAmountReportDao.saveDailyAmountReport(dailyAmountReport);
                    }

                    DailyQuantityCombinedReport dailyQuantityCombinedReport = DailyQuantityCombinedReportDao.getDailyQuantityCombinedReportByDateAndItemAndUnit(stock.getReceivedDate(), item, stock.getUnit());
                    boolean updateDailyQuantityCombinedReport = true;
                    if (dailyQuantityCombinedReport == null) {
                        dailyQuantityCombinedReport = new DailyQuantityCombinedReport();
                        dailyQuantityCombinedReport.setUnit(stock.getUnit());
                        updateDailyQuantityCombinedReport = false;
                    }
                    dailyQuantityCombinedReport.getSourceDailyQuantityReports().add(dailyQuantityReport);
                    dailyQuantityCombinedReport.setReportDate(stock.getReceivedDate());
                    dailyQuantityCombinedReport.setItem(item);
                    if (updateDailyQuantityCombinedReport) {
                        dailyQuantityCombinedReport.setOpeningBalance(dailyQuantityCombinedReport.getOpeningBalance() + dailyQuantityReport.getOpeningBalance());
                        dailyQuantityCombinedReport.setClosingBalance(dailyQuantityCombinedReport.getOpeningBalance() + dailyQuantityCombinedReport.getQuantityBought() - dailyQuantityCombinedReport.getQuantityDrawn());
                        DailyQuantityCombinedReportDao.updateDailyQuantityCombinedReport(dailyQuantityCombinedReport);
                    } else {
                        dailyQuantityCombinedReport.setOpeningBalance(dailyQuantityReport.getOpeningBalance());
                        dailyQuantityCombinedReport.setClosingBalance(dailyQuantityCombinedReport.getOpeningBalance() + dailyQuantityCombinedReport.getQuantityBought() - dailyQuantityCombinedReport.getQuantityDrawn());
                        DailyQuantityCombinedReportDao.saveDailyQuantityCombinedReport(dailyQuantityCombinedReport);
                    }

                    MonthlyAmountReport monthlyAmountReport = MonthlyAmountReportDao.getMonthlyAmountReportByItemAndYearAndMonthInMonths(item, yearMonth);
                    boolean updateMonthlyAmountReport = true;
                    if (monthlyAmountReport == null) {
                        monthlyAmountReport = new MonthlyAmountReport();
                        updateMonthlyAmountReport = false;
                    }
                    monthlyAmountReport.setReportDate(stock.getReceivedDate());
                    monthlyAmountReport.setOpeningBalance(monthlyAmountReport.getOpeningBalance() + monthlyQuantityReport.getOpeningBalance() * monthlyQuantityReport.getRate());
                    monthlyAmountReport.setClosingBalance(monthlyAmountReport.getOpeningBalance() + monthlyAmountReport.getAmountBought() - monthlyAmountReport.getAmountDrawn());
                    monthlyAmountReport.setItem(item);
                    monthlyAmountReport.setYearAndMonthInMonths(yearMonth);
                    if (updateMonthlyAmountReport) {
                        MonthlyAmountReportDao.updateMonthlyAmountReport(monthlyAmountReport);
                    } else {
                        monthlyAmountReport.setYearAndMonthInMonths(Utilities.toYearMonthFromDate(monthlyAmountReport.getReportDate()));
                        MonthlyAmountReportDao.saveMonthlyAmountReport(monthlyAmountReport);
                    }

                    MonthlyQuantityCombinedReport monthlyQuantityCombinedReport = MonthlyQuantityCombinedReportDao.getMonthlyQuantityCombinedReportsByItemAndYearAndMonthInMonthsAndUnit(item, yearMonth, stock.getUnit());
                    boolean updateMonthlyQuantityCombinedReport = true;
                    if (monthlyQuantityCombinedReport == null) {
                        monthlyQuantityCombinedReport = new MonthlyQuantityCombinedReport();
                        updateMonthlyQuantityCombinedReport = false;
                    }
                    monthlyQuantityCombinedReport.setReportDate(stock.getReceivedDate());
                    monthlyQuantityCombinedReport.setOpeningBalance(monthlyQuantityCombinedReport.getOpeningBalance() + stock.getQuantityOfReceivedStock());
                    monthlyQuantityCombinedReport.setUnit(stock.getUnit());
                    monthlyQuantityCombinedReport.setClosingBalance(monthlyQuantityCombinedReport.getOpeningBalance() + monthlyQuantityCombinedReport.getQuantityBought() - monthlyQuantityCombinedReport.getQuantityDrawn());
                    monthlyQuantityCombinedReport.getSourceMonthlyQuantityReports().add(monthlyQuantityReport);
                    monthlyQuantityCombinedReport.setYearAndMonthInMonths(yearMonth);
                    if (updateMonthlyQuantityCombinedReport) {
                        MonthlyQuantityCombinedReportDao.updateMonthlyQuantityCombinedReport(monthlyQuantityCombinedReport);
                    } else {
                        monthlyQuantityCombinedReport.setYearAndMonthInMonths(Utilities.toYearMonthFromDate(monthlyAmountReport.getReportDate()));
                        monthlyQuantityCombinedReport.setItem(item);
                        MonthlyQuantityCombinedReportDao.saveMonthlyQuantityCombinedReport(monthlyQuantityCombinedReport);
                    }
                } else {
                    int yearMonth = Utilities.toYearMonthFromDate(stock.getReceivedDate());

                    DailyQuantityReport dailyQuantityReport = new DailyQuantityReport();
                    dailyQuantityReport.setReportDate(stock.getReceivedDate());
                    dailyQuantityReport.setQuantityBought(stock.getQuantityOfReceivedStock());
                    dailyQuantityReport.setUnit(stock.getUnit());
                    dailyQuantityReport.setRate(stock.getPriceOfReceivedStock());
                    dailyQuantityReport.setClosingBalance(dailyQuantityReport.getOpeningBalance() + dailyQuantityReport.getQuantityBought() - dailyQuantityReport.getQuantityDrawn());
                    dailyQuantityReport.setStock(stock);
                    DailyQuantityReportDao.saveDailyQuantityReport(dailyQuantityReport);

                    MonthlyQuantityReport monthlyQuantityReport = new MonthlyQuantityReport();
                    monthlyQuantityReport.setReportDate(stock.getReceivedDate());
                    monthlyQuantityReport.setQuantityBought(stock.getQuantityOfReceivedStock());
                    monthlyQuantityReport.setClosingBalance(monthlyQuantityReport.getOpeningBalance() + monthlyQuantityReport.getQuantityBought() - monthlyQuantityReport.getQuantityDrawn());
                    monthlyQuantityReport.setRate(stock.getPriceOfReceivedStock());
                    monthlyQuantityReport.setUnit(stock.getUnit());
                    monthlyQuantityReport.setStock(stock);
                    monthlyQuantityReport.setYearAndMonthInMonths(yearMonth);
                    MonthlyQuantityReportDao.saveMonthlyQuantityReport(monthlyQuantityReport);

                    DailyAmountReport dailyAmountReport = DailyAmountReportDao.getDailyAmountReportByDateAndItem(stock.getReceivedDate(), item);
                    boolean updateDailyAmountReport = true;
                    if (dailyAmountReport == null) {
                        dailyAmountReport = new DailyAmountReport();
                        updateDailyAmountReport = false;
                    }
                    dailyAmountReport.setReportDate(stock.getReceivedDate());
                    dailyAmountReport.setItem(item);
                    if (updateDailyAmountReport) {
                        dailyAmountReport.setAmountBought(dailyAmountReport.getAmountBought() + dailyQuantityReport.getQuantityBought() * dailyQuantityReport.getRate());
                        dailyAmountReport.setClosingBalance(dailyAmountReport.getOpeningBalance() + dailyAmountReport.getAmountBought() - dailyAmountReport.getAmountDrawn());
                        DailyAmountReportDao.updateDailyAmountReport(dailyAmountReport);
                    } else {
                        dailyAmountReport.setAmountBought(dailyQuantityReport.getQuantityBought() * dailyQuantityReport.getRate());
                        dailyAmountReport.setClosingBalance(dailyAmountReport.getOpeningBalance() + dailyAmountReport.getAmountBought() - dailyAmountReport.getAmountDrawn());
                        DailyAmountReportDao.saveDailyAmountReport(dailyAmountReport);
                    }

                    DailyQuantityCombinedReport dailyQuantityCombinedReport = DailyQuantityCombinedReportDao.getDailyQuantityCombinedReportByDateAndItemAndUnit(stock.getReceivedDate(), item, stock.getUnit());
                    boolean updateDailyQuantityCombinedReport = true;
                    if (dailyQuantityCombinedReport == null) {
                        dailyQuantityCombinedReport = new DailyQuantityCombinedReport();
                        dailyQuantityCombinedReport.setUnit(stock.getUnit());
                        updateDailyQuantityCombinedReport = false;
                    }
                    dailyQuantityCombinedReport.getSourceDailyQuantityReports().add(dailyQuantityReport);
                    dailyQuantityCombinedReport.setReportDate(stock.getReceivedDate());
                    dailyQuantityCombinedReport.setItem(item);
                    if (updateDailyQuantityCombinedReport) {
                        dailyQuantityCombinedReport.setQuantityBought(dailyQuantityCombinedReport.getQuantityBought() + dailyQuantityReport.getQuantityBought());
                        dailyQuantityCombinedReport.setClosingBalance(dailyQuantityCombinedReport.getOpeningBalance() + dailyQuantityCombinedReport.getQuantityBought() - dailyQuantityCombinedReport.getQuantityDrawn());
                        DailyQuantityCombinedReportDao.updateDailyQuantityCombinedReport(dailyQuantityCombinedReport);
                    } else {
                        dailyQuantityCombinedReport.setQuantityBought(dailyQuantityReport.getQuantityBought());
                        dailyQuantityCombinedReport.setClosingBalance(dailyQuantityCombinedReport.getOpeningBalance() + dailyQuantityCombinedReport.getQuantityBought() - dailyQuantityCombinedReport.getQuantityDrawn());
                        DailyQuantityCombinedReportDao.saveDailyQuantityCombinedReport(dailyQuantityCombinedReport);
                    }

                    MonthlyAmountReport monthlyAmountReport = MonthlyAmountReportDao.getMonthlyAmountReportByItemAndYearAndMonthInMonths(item, yearMonth);
                    boolean updateMonthlyAmountReport = true;
                    if (monthlyAmountReport == null) {
                        monthlyAmountReport = new MonthlyAmountReport();
                        updateMonthlyAmountReport = false;
                    }
                    monthlyAmountReport.setReportDate(stock.getReceivedDate());
                    monthlyAmountReport.setAmountBought(monthlyAmountReport.getAmountBought() + monthlyQuantityReport.getQuantityBought() * monthlyQuantityReport.getRate());
                    monthlyAmountReport.setClosingBalance(monthlyAmountReport.getOpeningBalance() + monthlyAmountReport.getAmountBought() - monthlyAmountReport.getAmountDrawn());
                    monthlyAmountReport.setItem(item);
                    monthlyAmountReport.setYearAndMonthInMonths(yearMonth);
                    if (updateMonthlyAmountReport) {
                        MonthlyAmountReportDao.updateMonthlyAmountReport(monthlyAmountReport);
                    } else {
                        monthlyAmountReport.setYearAndMonthInMonths(Utilities.toYearMonthFromDate(monthlyAmountReport.getReportDate()));
                        MonthlyAmountReportDao.saveMonthlyAmountReport(monthlyAmountReport);
                    }

                    MonthlyQuantityCombinedReport monthlyQuantityCombinedReport = MonthlyQuantityCombinedReportDao.getMonthlyQuantityCombinedReportsByItemAndYearAndMonthInMonthsAndUnit(item, yearMonth, stock.getUnit());
                    boolean updateMonthlyQuantityCombinedReport = true;
                    if (monthlyQuantityCombinedReport == null) {
                        monthlyQuantityCombinedReport = new MonthlyQuantityCombinedReport();
                        updateMonthlyQuantityCombinedReport = false;
                    }
                    monthlyQuantityCombinedReport.setQuantityBought(monthlyQuantityCombinedReport.getQuantityBought() + stock.getQuantityOfReceivedStock());
                    monthlyQuantityCombinedReport.setUnit(stock.getUnit());
                    monthlyQuantityCombinedReport.setClosingBalance(monthlyQuantityCombinedReport.getOpeningBalance() + monthlyQuantityCombinedReport.getQuantityBought() - monthlyQuantityCombinedReport.getQuantityDrawn());
                    monthlyQuantityCombinedReport.getSourceMonthlyQuantityReports().add(monthlyQuantityReport);
                    if (updateMonthlyQuantityCombinedReport) {
                        MonthlyQuantityCombinedReportDao.updateMonthlyQuantityCombinedReport(monthlyQuantityCombinedReport);
                    } else {
                        monthlyQuantityCombinedReport.setReportDate(stock.getReceivedDate());
                        monthlyQuantityCombinedReport.setYearAndMonthInMonths(Utilities.toYearMonthFromDate(monthlyAmountReport.getReportDate()));
                        monthlyQuantityCombinedReport.setItem(item);
                        MonthlyQuantityCombinedReportDao.saveMonthlyQuantityCombinedReport(monthlyQuantityCombinedReport);
                    }
                }
            }
        }
    }

    @ActionMethod("updateStock")
    public void updateStock() {
        if (validateBeforeFormSubmission()) {
            Stock stockToUpdate = currentStock;
            double previousStockQuantity = stockToUpdate.getQuantityOfReceivedStock();
            loadFormDataIntoStock(stockToUpdate);
            double newStockQuantity = stockToUpdate.getQuantityOfReceivedStock();
            double stockQuantityDifference = newStockQuantity - previousStockQuantity;
            stockToUpdate.setQuantityOfBalancedStock(stockToUpdate.getQuantityOfBalancedStock() + stockQuantityDifference);
            stockToUpdate.setValueOfReceivedStock(stockToUpdate.getQuantityOfReceivedStock() * stockToUpdate.getPriceOfReceivedStock());
            stockToUpdate.setValueOfBalancedStock(stockToUpdate.getQuantityOfBalancedStock() * stockToUpdate.getPriceOfReceivedStock());
            StockDao.updateStock(stockToUpdate);
            refreshTable();
            stockTableView.getSelectionModel().select(stockToUpdate);
            clearFields();
        }
    }

    private boolean validateBeforeFormSubmission() {
        boolean isErrorFree = true;

        if (receivedDateDatePicker.getValue() == null) {
            receivedDateDatePicker.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }
        if (quantityInput.getText().isEmpty()) {
            quantityInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }
        if (requiredQuantityInput.getText().isEmpty()) {
            requiredQuantityInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }
        if (unitInput.getText().isEmpty()) {
            unitInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }
        if (priceInput.getText().isEmpty()) {
            priceInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }
        return isErrorFree;
    }

    private boolean validateBeforeFillingStock() {
        boolean isErrorFree = true;

        if (filledDateDatePicker.getValue() == null) {
            filledDateDatePicker.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }
        if (filledQuantityInput.getText().isEmpty()) {
            filledQuantityInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }
        return isErrorFree;
    }

    @ActionMethod("createFilledStock")
    public void createFilledStock() {
        if (validateBeforeFillingStock()) {
            StockReceipt stockReceipt = new StockReceipt();
            loadFormDataIntoStockReceipt(stockReceipt);
            currentStock.getStockReceipts().add(stockReceipt);
            currentStock.setQuantityOfReceivedStock(currentStock.getQuantityOfReceivedStock() + stockReceipt.getReceivedQuantity());
            currentStock.setValueOfReceivedStock(currentStock.getQuantityOfReceivedStock() * currentStock.getPriceOfReceivedStock());
            currentStock.setQuantityOfBalancedStock(currentStock.getQuantityOfBalancedStock() + stockReceipt.getReceivedQuantity());
            currentStock.setValueOfBalancedStock(currentStock.getQuantityOfBalancedStock() * currentStock.getPriceOfReceivedStock());
            StockDao.updateStockWithStockReceipt(currentStock, stockReceipt);
            refreshTable();
            stockTableView.getSelectionModel().select(currentStock);
        }
    }

    private void clearFormWarnings() {
        receivedDateDatePicker.setStyle("-fx-border-color: null");
        quantityInput.setStyle("-fx-border-color: null");
        requiredQuantityInput.setStyle("-fx-border-color: null");
        unitInput.setStyle("-fx-border-color: null");
        priceInput.setStyle("-fx-border-color: null");
    }

    private void clearFillStockFormWarnings() {
        filledDateDatePicker.setStyle("-fx-border-color: null");
        filledQuantityInput.setStyle("-fx-border-color: null");
    }

    private void loadFormDataIntoStockReceipt(StockReceipt stockReceipt) {
        Date filledDateUtilDate = Date.from(filledDateDatePicker.getValue().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        stockReceipt.setReceivedDate(filledDateUtilDate);
        stockReceipt.setParticular(particularInput.getText());
        stockReceipt.setReceivedQuantity(Double.parseDouble(filledQuantityInput.getText()));
    }

    private void loadFormDataIntoStock(Stock stock) {
        Date receivedDateUtilDate = Date.from(receivedDateDatePicker.getValue().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        stock.setReceivedDate(receivedDateUtilDate);
        stock.setQuantityOfReceivedStock(Double.parseDouble(quantityInput.getText()));
        stock.setPriceOfReceivedStock(Double.parseDouble(priceInput.getText()));
        stock.setRequiredStockLevel(Double.parseDouble(requiredQuantityInput.getText()));
        stock.setUnit(unitInput.getText());
        stock.setRemark(remarkInput.getText());
    }

    private void loadStockDataIntoForm(Stock stock) {
        Instant instant = Instant.ofEpochMilli(stock.getReceivedDate().getTime());
        LocalDate localDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
        receivedDateDatePicker.setValue(localDate);
        quantityInput.setText(stock.getQuantityOfReceivedStock() + "");
        priceInput.setText(stock.getPriceOfReceivedStock() + "");
        requiredQuantityInput.setText(stock.getRequiredStockLevel() + "");
        unitInput.setText(stock.getUnit());
        remarkInput.setText(stock.getRemark());
    }

    private void refreshTable() {
        stockTableView.getItems().clear();
        loadTable();
    }

    @ActionMethod("setAsActiveStock")
    public void setAsActiveStock() {
        Item item = itemContainer.getItem();
        Stock activeStock = item.getActiveStock();
        item.setActiveStock(null);
        if (activeStock != null) {
            activeStock.setActiveStockOwner(null);
            StockDao.updateStock(activeStock);
        }
        ItemDao.updateItem(item);
        item.setActiveStock(currentStock);
        currentStock.setActiveStockOwner(item);
        StockDao.updateStock(currentStock);
        ItemDao.updateItem(item);
        detectNullActiveStock();
        loadStockDetails();
    }

    @ActionMethod("closeStock")
    public void closeStock() {
        OpenCloseStatus currentStockOpenCloseStatus = currentStock.getOpenCloseStatus();
        if (currentStockOpenCloseStatus == OpenCloseStatus.OPEN)
            currentStockOpenCloseStatus = OpenCloseStatus.CLOSE;
        else
            currentStockOpenCloseStatus = OpenCloseStatus.OPEN;
        currentStock.setOpenCloseStatus(currentStockOpenCloseStatus);
        StockDao.updateStock(currentStock);
    }

    @ActionMethod("loadEditStockForm")
    public void loadEditStockForm() {
        prepareEditStock();
        loadNewStockGrid();
        loadStockDataIntoForm(currentStock);
    }

    private void prepareEditStock() {
        addStockText.setText("Edit Stock");
        createButton.setVisible(false);
        saveButton.setVisible(true);
        saveButton.toFront();
    }

    @ActionMethod("loadFillStockForm")
    public void loadFillStockForm() {
        loadFillStockGrid();
    }

    private void loadFillStockGrid() {
        enableThisGrid(fillStockGrid);
        clearFillStockFields();
    }

    @ActionMethod("clearFilledStock")
    public void clearFilledStock() {
        clearFillStockFields();
    }

    private void clearFillStockFields() {
        filledDateDatePicker.setValue(null);
        particularInput.clear();
        filledQuantityInput.clear();
        clearFillStockFormWarnings();
    }

    @ActionMethod("loadDetailedReport")
    public void loadDetailedReportController() {
        itemContainer.setItem(itemContainer.getItem());
        try {
            handler.navigate(DetailedReportController.class);
        } catch (VetoException e) {
        } catch (FlowException e) {
        }
    }

    @ActionMethod("loadStockReceipts")
    public void loadStockReceiptsController() {
        stockContainer.setStock(currentStock);
        try {
            handler.navigate(ReceiptController.class);
        } catch (VetoException e) {
            e.printStackTrace();
        } catch (FlowException e) {
            e.printStackTrace();
        }
    }

    @ActionMethod("confirmDeleteStock")
    public void confirmDeleteStock() {
        if (currentStock.getActiveStockOwner() != null)
            currentStock.getActiveStockOwner().setActiveStock(null);
        currentStock.getItem().getStocks().remove(currentStock);
        StockDao.deleteStock(currentStock);
        refreshTable();
        loadNewStockForm();
    }

    @ActionMethod("cancelDeleteStock")
    public void cancelDeleteStock() {
        loadStockDetails();
    }

    private void loadStockIssueController(Stock stock) {
        stockContainer.setStock(stock);
        if (stock.getStockIssues() == null)
            stock.setStockIssues(new ArrayList<>());
        try {
            handler.navigate(IssueController.class);
        } catch (VetoException e) {
        } catch (FlowException e) {
        }
    }

    private boolean isDeletable(Stock stock) {
        return (stock.getStockIssues() == null || stock.getStockIssues().isEmpty())
                && (stock.getStockReceipts() == null || stock.getStockReceipts().isEmpty());
    }
}
