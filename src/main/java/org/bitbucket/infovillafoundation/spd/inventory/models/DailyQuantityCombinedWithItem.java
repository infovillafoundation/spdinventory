package org.bitbucket.infovillafoundation.spd.inventory.models;

import org.bitbucket.infovillafoundation.spd.inventory.entities.DailyQuantityCombinedReport;
import org.bitbucket.infovillafoundation.spd.inventory.utils.Utilities;

/**
 * Created by Sandah Aung on 31/1/15.
 */
public class DailyQuantityCombinedWithItem {

    private DailyQuantityCombinedReport dailyQuantityCombinedReport;

    public DailyQuantityCombinedWithItem(DailyQuantityCombinedReport dailyQuantityCombinedReport) {
        this.dailyQuantityCombinedReport = dailyQuantityCombinedReport;
    }

    public String getItemName() {
        return dailyQuantityCombinedReport.getItem().getName();
    }

    public String getReportDate() {
        return Utilities.fromDate(dailyQuantityCombinedReport.getReportDate());
    }

    public String getUnit() {
        return dailyQuantityCombinedReport.getUnit();
    }

    public String getOpeningBalance() {
        return dailyQuantityCombinedReport.getOpeningBalance() + " " + getUnit();
    }

    public String getQuantityBought() {
        return dailyQuantityCombinedReport.getQuantityBought() + " " + getUnit();
    }

    public String getQuantityDrawn() {
        return dailyQuantityCombinedReport.getQuantityDrawn() + " " + getUnit();
    }

    public String getClosingBalance() {
        return dailyQuantityCombinedReport.getClosingBalance() + " " + getUnit();
    }
}
