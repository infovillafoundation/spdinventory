package org.bitbucket.infovillafoundation.spd.inventory.daos;

import org.bitbucket.infovillafoundation.spd.inventory.entities.Item;
import org.bitbucket.infovillafoundation.spd.inventory.entities.MonthlyAmountReport;
import org.bitbucket.infovillafoundation.spd.inventory.entities.Stock;
import org.bitbucket.infovillafoundation.spd.inventory.entities.StockReceipt;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 17/12/14.
 */
public class MonthlyAmountReportDao extends Dao {

    public static List<MonthlyAmountReport> getAllMonthlyAmountReports() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<MonthlyAmountReport> query = em.createNamedQuery("MonthlyAmountReport.findAll", MonthlyAmountReport.class);
        List<MonthlyAmountReport> monthlyAmountReports = query.getResultList();
        return monthlyAmountReports;
    }

    public static List<MonthlyAmountReport> getMonthlyAmountReportsByYearAndMonthInMonths(int yearAndMonthInMonths) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<MonthlyAmountReport> query = em.createNamedQuery("MonthlyAmountReport.findByYearAndMonthInMonths", MonthlyAmountReport.class);
        query.setParameter("yearAndMonthInMonths", yearAndMonthInMonths);
        List<MonthlyAmountReport> monthlyAmountReports = query.getResultList();
        return monthlyAmountReports;
    }

    public static List<MonthlyAmountReport> getMonthlyAmountReportsByDate(Date reportDate) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<MonthlyAmountReport> query = em.createNamedQuery("MonthlyAmountReport.findByReportDate", MonthlyAmountReport.class);
        query.setParameter("reportDate", reportDate);
        List<MonthlyAmountReport> monthlyAmountReports = query.getResultList();
        return monthlyAmountReports;
    }

    public static MonthlyAmountReport getMonthlyAmountReportByItemAndYearAndMonthInMonths(Item item, int yearAndMonthInMonths) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<MonthlyAmountReport> query = em.createNamedQuery("MonthlyAmountReport.findByItemAndYearAndMonthInMonths", MonthlyAmountReport.class);
        query.setParameter("yearAndMonthInMonths", yearAndMonthInMonths);
        query.setParameter("item", item);
        MonthlyAmountReport monthlyAmountReport = null;
        try {
            monthlyAmountReport = query.getSingleResult();
        } catch (NoResultException e) {

        }
        return monthlyAmountReport;
    }

    public static List<Item> getItemsByNameLike(String searchTerm) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Item> query = em.createNamedQuery("Item.findByNameLike", Item.class);
        query.setParameter("name", "%" + searchTerm + "%");
        List<Item> items = query.getResultList();
        return items;
    }

    public static void saveMonthlyAmountReport(MonthlyAmountReport monthlyAmountReport) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(monthlyAmountReport);
        em.getTransaction().commit();
        em.close();
    }

    public static void saveItemWithStock(Item item, Stock stock) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stock);
        em.persist(item);
        em.getTransaction().commit();
        em.close();
    }

    public static void saveItemWithStockAndStockReceipt(Item item, Stock stock, StockReceipt stockReceipt) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stockReceipt);
        em.persist(stock);
        em.persist(item);
        em.getTransaction().commit();
        em.close();
    }

    public static void updateMonthlyAmountReport(MonthlyAmountReport monthlyAmountReport) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(monthlyAmountReport);
        em.getTransaction().commit();
        em.close();
    }

    public static void deleteItem(Item item) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.contains(item) ? item : em.merge(item));
        em.getTransaction().commit();
        em.close();
    }
}
