package org.bitbucket.infovillafoundation.spd.inventory.daos;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by Thant Zin Oo on 10/17/2014.
 */
public class Dao {
    public static EntityManagerFactory emf = Persistence.createEntityManagerFactory("SpdInventoryPU");
}
