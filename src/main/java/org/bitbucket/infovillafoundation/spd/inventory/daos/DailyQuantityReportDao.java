package org.bitbucket.infovillafoundation.spd.inventory.daos;

import org.bitbucket.infovillafoundation.spd.inventory.entities.DailyQuantityReport;
import org.bitbucket.infovillafoundation.spd.inventory.entities.Item;
import org.bitbucket.infovillafoundation.spd.inventory.entities.Stock;
import org.bitbucket.infovillafoundation.spd.inventory.entities.StockReceipt;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 17/12/14.
 */
public class DailyQuantityReportDao extends Dao {

    public static List<DailyQuantityReport> getAllDailyQuantityReports() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<DailyQuantityReport> query = em.createNamedQuery("DailyQuantityReport.findAll", DailyQuantityReport.class);
        List<DailyQuantityReport> dailyQuantityReports = query.getResultList();
        return dailyQuantityReports;
    }

    public static List<DailyQuantityReport> getDailyQuantityReportsByDate(Date reportDate) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<DailyQuantityReport> query = em.createNamedQuery("DailyQuantityReport.findByReportDate", DailyQuantityReport.class);
        query.setParameter("reportDate", reportDate);
        List<DailyQuantityReport> dailyQuantityReports = query.getResultList();
        return dailyQuantityReports;
    }

    public static DailyQuantityReport getDailyQuantityReportByStockAndDate(Stock stock, Date reportDate) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<DailyQuantityReport> query = em.createNamedQuery("DailyQuantityReport.findByStockAndReportDate", DailyQuantityReport.class);
        query.setParameter("reportDate", reportDate);
        query.setParameter("stock", stock);
        DailyQuantityReport dailyQuantityReport = null;
        try {
            dailyQuantityReport = query.getSingleResult();
        } catch (NoResultException e) {
            e.printStackTrace();
        }

        return dailyQuantityReport;
    }

    public static DailyQuantityReport getLatestDailyQuantityReport() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<DailyQuantityReport> query = em.createNamedQuery("DailyQuantityReport.findByReportDateDesc", DailyQuantityReport.class);
        query.setMaxResults(1);
        DailyQuantityReport dailyQuantityReport = null;
        try {
            dailyQuantityReport = query.getSingleResult();
        } catch (NoResultException e) {

        }
        return dailyQuantityReport;
    }

    public static List<Item> getItemsByNameLike(String searchTerm) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Item> query = em.createNamedQuery("Item.findByNameLike", Item.class);
        query.setParameter("name", "%" + searchTerm + "%");
        List<Item> items = query.getResultList();
        return items;
    }

    public static void saveDailyQuantityReport(DailyQuantityReport dailyQuantityReport) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(dailyQuantityReport);
        em.getTransaction().commit();
        em.close();
    }

    public static void saveItemWithStock(Item item, Stock stock) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stock);
        em.persist(item);
        em.getTransaction().commit();
        em.close();
    }

    public static void saveItemWithStockAndStockReceipt(Item item, Stock stock, StockReceipt stockReceipt) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stockReceipt);
        em.persist(stock);
        em.persist(item);
        em.getTransaction().commit();
        em.close();
    }

    public static void updateDailyQuantityReport(DailyQuantityReport dailyQuantityReport) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(dailyQuantityReport);
        em.getTransaction().commit();
        em.close();
    }

    public static void deleteItem(Item item) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.contains(item) ? item : em.merge(item));
        em.getTransaction().commit();
        em.close();
    }
}
