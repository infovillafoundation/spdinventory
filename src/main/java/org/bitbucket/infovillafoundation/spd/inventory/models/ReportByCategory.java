package org.bitbucket.infovillafoundation.spd.inventory.models;

import org.bitbucket.infovillafoundation.spd.inventory.entities.Item;
import org.bitbucket.infovillafoundation.spd.inventory.entities.Stock;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandah Aung on 27/1/15.
 */
public class ReportByCategory {

    private Item item;

    private List<Item> allItems;

    private QuantityRateAmount quantityRateAmount;

    public ReportByCategory() {
    }

    public ReportByCategory(Item item) {

        this.item = item;
        this.allItems = returnAllItems(item);

        boolean isUnitTheSame = true;
        boolean isRateTheSame = true;
        String unit = null;
        double rate = 0;
        double totalQuantity = 0;
        double totalAmount = 0;
        for (Item qItem : allItems) {
            for (Stock stock : qItem.getStocks()) {
                if (unit != null && !unit.equals(stock.getUnit()))
                    isUnitTheSame = false;
                if (stock.getUnit() != null)
                    unit = stock.getUnit();
                totalQuantity += stock.getQuantityOfBalancedStock();
                if (rate != 0 && rate != stock.getPriceOfReceivedStock())
                    isRateTheSame = false;
                if (stock.getPriceOfReceivedStock() != 0)
                    rate = stock.getPriceOfReceivedStock();
                totalAmount += stock.getValueOfBalancedStock();

            }
        }

        quantityRateAmount = new QuantityRateAmount();

        if (isUnitTheSame && isRateTheSame) {
            quantityRateAmount.setUnit(unit);
            quantityRateAmount.setQuantity(totalQuantity);
            quantityRateAmount.setRate(rate);
        }

        quantityRateAmount.setAmount(totalAmount);
    }

    private List<Item> returnAllItems(Item item) {
        List<Item> listOfItems = new ArrayList<Item>();
        addAllItems(item, listOfItems);
        return listOfItems;
    }

    private void addAllItems(Item item, List<Item> listOfItems) {
        if (item != null) {
            listOfItems.add(item);
            List<Item> children = item.getChildItems();
            if (children != null) {
                for (Item child : children) {
                    addAllItems(child, listOfItems);
                }
            }
        }
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public String getStockTitle() {
        return item.getName();
    }

    public String getQuantity() {
        if (quantityRateAmount.getQuantity() == 0)
            return null;
        else return quantityRateAmount.getQuantity() + "";
    }

    public String getRate() {
        if (quantityRateAmount.getRate() == 0)
            return null;
        else return quantityRateAmount.getRate() + "";
    }

    public String getAmount() {
        if (quantityRateAmount.getAmount() == 0)
            return null;
        else return quantityRateAmount.getAmount() + "";
    }

    public double getAmountDouble() {
        return quantityRateAmount.getAmount();
    }

    private class QuantityRateAmount {
        private String unit;
        private double quantity;
        private double rate;
        private double amount;

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public double getQuantity() {
            return quantity;
        }

        public void setQuantity(double quantity) {
            this.quantity = quantity;
        }

        public double getRate() {
            return rate;
        }

        public void setRate(double rate) {
            this.rate = rate;
        }

        public double getAmount() {
            return amount;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }
    }
}
