package org.bitbucket.infovillafoundation.spd.inventory.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 15/12/14.
 */

@NamedQueries({
        @NamedQuery(name = "DailyQuantityCombinedReport.findAll",
                query = "SELECT e FROM DailyQuantityCombinedReport e"),
        @NamedQuery(name = "DailyQuantityCombinedReport.findByReportDate",
                query = "SELECT e FROM DailyQuantityCombinedReport e  WHERE e.reportDate = :reportDate"),
        @NamedQuery(name = "DailyQuantityCombinedReport.findByReportDateAndItem",
                query = "SELECT e FROM DailyQuantityCombinedReport e  WHERE e.reportDate = :reportDate AND e.item = :item"),
        @NamedQuery(name = "DailyQuantityCombinedReport.findByReportDateAndItemAndUnit",
                query = "SELECT e FROM DailyQuantityCombinedReport e  WHERE e.reportDate = :reportDate AND e.item = :item AND e.unit = :unit")
})

@Entity
@Table(name = "daily_quantity_combined_reports")
public class DailyQuantityCombinedReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "report_date")
    @Temporal(TemporalType.DATE)
    private Date reportDate;

    private String unit;

    @Column(name = "opening_balance")
    private double openingBalance;

    @Column(name = "buy")
    private double quantityBought;

    @Column(name = "draw")
    private double quantityDrawn;

    @Column(name = "closing_balance")
    private double closingBalance;

    @OneToOne
    @JoinColumn(name = "item_id")
    private Item item;

    @OneToMany
    @JoinTable(
            name = "daily_quantity_reports_references",
            joinColumns = {@JoinColumn(name = "combined_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "daily_id", referencedColumnName = "id")}
    )
    private List<DailyQuantityReport> sourceDailyQuantityReports;

    public DailyQuantityCombinedReport() {
        if (sourceDailyQuantityReports == null)
            sourceDailyQuantityReports = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(double openingBalance) {
        this.openingBalance = openingBalance;
    }

    public double getQuantityBought() {
        return quantityBought;
    }

    public void setQuantityBought(double quantityBought) {
        this.quantityBought = quantityBought;
    }

    public double getQuantityDrawn() {
        return quantityDrawn;
    }

    public void setQuantityDrawn(double quantityDrawn) {
        this.quantityDrawn = quantityDrawn;
    }

    public double getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(double closingBalance) {
        this.closingBalance = closingBalance;
    }

    public List<DailyQuantityReport> getSourceDailyQuantityReports() {
        return sourceDailyQuantityReports;
    }

    public void setSourceDailyQuantityReports(List<DailyQuantityReport> sourceDailyQuantityReports) {
        this.sourceDailyQuantityReports = sourceDailyQuantityReports;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
