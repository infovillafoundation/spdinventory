package org.bitbucket.infovillafoundation.spd.inventory.models;


import io.datafx.controller.injection.scopes.FlowScoped;
import org.bitbucket.infovillafoundation.spd.inventory.entities.Item;

/**
 * Created by Sandah Aung on 26/12/14.
 */


@FlowScoped
public class ItemContainer {
    private Item item;

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
