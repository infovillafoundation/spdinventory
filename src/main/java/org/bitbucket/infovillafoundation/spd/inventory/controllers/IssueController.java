package org.bitbucket.infovillafoundation.spd.inventory.controllers;

import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.action.BackAction;
import io.datafx.controller.flow.context.ActionHandler;
import io.datafx.controller.flow.context.FlowActionHandler;
import io.datafx.controller.util.VetoException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.util.StringConverter;
import org.bitbucket.infovillafoundation.spd.inventory.daos.*;
import org.bitbucket.infovillafoundation.spd.inventory.entities.*;
import org.bitbucket.infovillafoundation.spd.inventory.models.StockContainer;
import org.bitbucket.infovillafoundation.spd.inventory.utils.Utilities;
import org.controlsfx.control.MasterDetailPane;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Created by Sandah Aung on 26/12/14.
 */

@FXMLController(value = "/fxml/issues.fxml", title = "Issue")
public class IssueController {

    @ActionHandler
    private FlowActionHandler handler;

    @FXML
    private MasterDetailPane pane;

    @FXML
    private GridPane allocateUseGrid;

    @FXML
    private GridPane addActualUseGrid;

    @FXML
    private GridPane showIssueGrid;

    @FXML
    private GridPane confirmDeleteIssueGrid;

    @Inject
    private StockContainer stockContainer;

    @FXML
    @BackAction
    private Button backButton;

    @FXML
    @ActionTrigger("loadNewIssueForm")
    private Button newIssueButton;

    @FXML
    @ActionTrigger("loadEditIssueForm")
    private Button editIssueButton;

    @FXML
    @ActionTrigger("loadDeleteIssueForm")
    private Button deleteIssueButton;

    @FXML
    @ActionTrigger("loadActualUseForm")
    private Button addActualUseButton;

    @FXML
    private Text stockReceivedDate;

    @FXML
    private TableView<StockIssue> issueTableView;

    @FXML
    @ActionTrigger("loadIssueDetails")
    private MenuItem detailsMenuItem;

    @FXML
    @ActionTrigger("setAsActiveStock")
    private MenuItem setAsActiveStockMenuItem;

    @FXML
    private Text allocateIssueText;

    @FXML
    private TextField particularInput;

    @FXML
    private TextField allocatedUseInput;

    @FXML
    private TextField actualUseInput;

    @FXML
    private DatePicker issuedDateDatePicker;

    @FXML
    @ActionTrigger("createStockIssue")
    private Button createIssueButton;

    @FXML
    @ActionTrigger("updateStockIssue")
    private Button saveIssueButton;

    @FXML
    @ActionTrigger("clearStockIssue")
    private Button clearIssueButton;

    @FXML
    private Text issuedDateText;

    @FXML
    private Text particularText;

    @FXML
    private Text allocatedUseText;

    @FXML
    private Text allocatedStockValueText;

    @FXML
    private Text actualUseText;

    @FXML
    private Text actualUseValueText;

    @FXML
    private Text incomeText;

    @FXML
    private TextField actualUseQuantityInput;

    @FXML
    @ActionTrigger("createActualUse")
    private Button createActualUseButton;

    @FXML
    @ActionTrigger("clearActualUse")
    private Button clearActualUseButton;

    @FXML
    @ActionTrigger("confirmDeleteIssue")
    private Button confirmDeleteIssueButton;

    @FXML
    @ActionTrigger("cancelDeleteIssue")
    private Button cancelDeleteIssueButton;

    private StockIssue currentStockIssue;

    @PostConstruct
    public void init() {
        stockReceivedDate.setText(stockContainer.getStock().getRemark());
        issueTableView.getItems().setAll(stockContainer.getStock().getStockIssues());
        setupBackShortcutListener();
        setupRowClickListener();
        loadNewStockIssueForm();
        setupDateConverter();
        setupValidators();
    }

    private void setupRowClickListener() {
        issueTableView
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(new ChangeListener<StockIssue>() {
                                 @Override
                                 public void changed(ObservableValue<? extends StockIssue> observable, StockIssue oldValue, StockIssue newValue) {
                                     currentStockIssue = newValue;
                                     if (currentStockIssue == null) {
                                         loadNewStockIssueForm();
                                         editIssueButton.setVisible(false);
                                         deleteIssueButton.setVisible(false);
                                         addActualUseButton.setVisible(false);
                                     } else {
                                         loadStockIssueDetails();
                                         editIssueButton.setVisible(true);
                                         deleteIssueButton.setVisible(true);
                                         if (currentStockIssue.getActualUse() == 0)
                                             addActualUseButton.setVisible(true);
                                         else
                                             addActualUseButton.setVisible(false);
                                     }

                                 }
                             }

                );
    }

    private void setupValidators() {

        issuedDateDatePicker.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (issuedDateDatePicker.getValue() == null) {
                    issuedDateDatePicker.setStyle("-fx-border-color: red");
                } else {
                    issuedDateDatePicker.setStyle("-fx-border-color: null");
                }
            }
        });

        allocatedUseInput.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (allocatedUseInput.getText().isEmpty())
                    allocatedUseInput.setStyle("-fx-border-color: red");
                else
                    allocatedUseInput.setStyle("-fx-border-color: null");
            }
        });

        actualUseQuantityInput.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (actualUseQuantityInput.getText().isEmpty())
                    actualUseQuantityInput.setStyle("-fx-border-color: red");
                else
                    actualUseQuantityInput.setStyle("-fx-border-color: null");
            }
        });
    }

    private void setupBackShortcutListener() {
        final KeyCombination ctrlB = new KeyCodeCombination(KeyCode.B, KeyCombination.CONTROL_DOWN);
        pane.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (ctrlB.match(event)) {
                    try {
                        handler.navigateBack();
                    } catch (VetoException e) {
                    } catch (FlowException e) {
                    }
                }
            }
        });
    }

    private void setupDateConverter() {
        String pattern = "dd/M/yyyy";
        issuedDateDatePicker.setPromptText(pattern.toLowerCase());
        issuedDateDatePicker.setConverter(new StringConverter<LocalDate>() {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateTimeFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateTimeFormatter);

                } else {
                    return null;
                }
            }
        });
    }


    @ActionMethod("loadStockIssueDetails")
    public void loadStockIssueDetails() {
        loadStockIssueDetailsGrid();
        loadStockIssueIntoShowGrid();
    }

    private void loadStockIssueDetailsGrid() {
        enableThisGrid(showIssueGrid);
    }

    private void enableThisGrid(GridPane grid) {
        allocateUseGrid.setVisible(false);
        addActualUseGrid.setVisible(false);
        showIssueGrid.setVisible(false);
        confirmDeleteIssueGrid.setVisible(false);
        grid.setVisible(true);
        grid.toFront();
    }

    private void loadStockIssueIntoShowGrid() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy");
        issuedDateText.setText(simpleDateFormat.format(currentStockIssue.getIssuedDate()));
        particularText.setText(currentStockIssue.getParticular());
        allocatedUseText.setText(currentStockIssue.getAllocatedUse() + "");
        allocatedStockValueText.setText(currentStockIssue.getAllocatedUseValue() + "");
        actualUseText.setText(currentStockIssue.getActualUse() + "");
        actualUseValueText.setText((currentStockIssue.getActualUseValue() + ""));
        incomeText.setText(currentStockIssue.getIncomeFromDifference() + "");
    }

    @ActionMethod("loadNewIssueForm")
    public void loadNewStockIssueForm() {
        loadNewStockIssueGrid();
        prepareNewStockIssue();
    }

    private void prepareNewStockIssue() {
        issuedDateDatePicker.setValue(Utilities.toLocalDateFromUtilDate(new Date()));
        issuedDateDatePicker.setDisable(true);
        allocateIssueText.setText("Allocate Stock Issue");
        saveIssueButton.setVisible(false);
        createIssueButton.setVisible(true);
        createIssueButton.toFront();
    }

    private void loadNewStockIssueGrid() {
        enableThisGrid(allocateUseGrid);
        clearAllocateUseFields();
    }

    @ActionMethod("clearStockIssue")
    public void clear() {
        clearAllocateUseFields();
    }

    private void clearAllocateUseFields() {
        issuedDateDatePicker.setValue(null);
        issuedDateDatePicker.setDisable(false);
        particularInput.clear();
        allocatedUseInput.clear();
        actualUseInput.clear();
        clearAllocatedUseFormWarnings();
    }

    @ActionMethod("clearActualUse")
    public void clearActualUse() {
        clearActualUseFields();
    }

    private void clearActualUseFields() {
        actualUseQuantityInput.clear();
        clearActualUseFormWarnings();
    }

    @ActionMethod("createStockIssue")
    public void createStockIssue() {

        if (validateBeforeFormSubmission()) {

            StockIssue stockIssue = new StockIssue();
            loadFormDataIntoStockIssue(stockIssue);
            Stock stock = stockContainer.getStock();
            stockIssue.setActualUseValue(stockIssue.getActualUse() * stock.getPriceOfReceivedStock());
            stockIssue.setAllocatedUseValue(stock.getPriceOfReceivedStock() * stockIssue.getAllocatedUse());
            double quantityToDeduct = 0;
            if (stockIssue.getActualUse() == 0)
                quantityToDeduct = stockIssue.getAllocatedUse();
            else {
                quantityToDeduct = stockIssue.getActualUse();
                stockIssue.setActualUseValue(stock.getPriceOfReceivedStock() * stockIssue.getActualUse());
                stockIssue.setIncomeFromDifference(stockIssue.getAllocatedUseValue() - stockIssue.getActualUseValue());
            }
            stock.setQuantityOfIssuedStock(quantityToDeduct + stock.getQuantityOfIssuedStock());
            stock.setValueOfIssuedStock(stock.getQuantityOfIssuedStock() * stock.getPriceOfReceivedStock());
            stock.setQuantityOfBalancedStock(stock.getQuantityOfBalancedStock() - quantityToDeduct);
            stock.setValueOfBalancedStock(stock.getQuantityOfBalancedStock() * stock.getPriceOfReceivedStock());
            stock.getStockIssues().add(stockIssue);
            StockIssueDao.saveStockWithStockIssue(stock, stockIssue);
            refreshTable();
            currentStockIssue = stockIssue;
            issueTableView.getSelectionModel().select(stockIssue);

            Item item = stock.getItem();

            DailyQuantityReport dailyQuantityReport = DailyQuantityReportDao.getDailyQuantityReportByStockAndDate(stock, stockIssue.getIssuedDate());
            boolean updateDailyQuantityReport = true;
            if (dailyQuantityReport == null) {
                updateDailyQuantityReport = false;
                dailyQuantityReport = new DailyQuantityReport();
                dailyQuantityReport.setReportDate(stockIssue.getIssuedDate());
                dailyQuantityReport.setUnit(stock.getUnit());
                dailyQuantityReport.setRate(stock.getPriceOfReceivedStock());
                dailyQuantityReport.setStock(stock);
            }
            dailyQuantityReport.setQuantityDrawn(dailyQuantityReport.getQuantityDrawn() + quantityToDeduct);
            dailyQuantityReport.setClosingBalance(dailyQuantityReport.getOpeningBalance() + dailyQuantityReport.getQuantityBought() - dailyQuantityReport.getQuantityDrawn());
            if (updateDailyQuantityReport)
                DailyQuantityReportDao.updateDailyQuantityReport(dailyQuantityReport);
            else
                DailyQuantityReportDao.saveDailyQuantityReport(dailyQuantityReport);

            DailyAmountReport dailyAmountReport = DailyAmountReportDao.getDailyAmountReportByDateAndItem(stockIssue.getIssuedDate(), item);
            boolean updateDailyAmountReport = true;
            if (dailyAmountReport == null) {
                updateDailyAmountReport = false;
                dailyAmountReport = new DailyAmountReport();
                dailyAmountReport.setReportDate(stockIssue.getIssuedDate());
                dailyAmountReport.setItem(item);
            }
            dailyAmountReport.setAmountDrawn(dailyAmountReport.getAmountDrawn() + quantityToDeduct * dailyQuantityReport.getRate());
            dailyAmountReport.setClosingBalance(dailyAmountReport.getOpeningBalance() + dailyAmountReport.getAmountBought() - dailyAmountReport.getAmountDrawn());
            if (updateDailyAmountReport)
                DailyAmountReportDao.updateDailyAmountReport(dailyAmountReport);
            else
                DailyAmountReportDao.updateDailyAmountReport(dailyAmountReport);

            DailyQuantityCombinedReport dailyQuantityCombinedReport = DailyQuantityCombinedReportDao.getDailyQuantityCombinedReportByDateAndItemAndUnit(stockIssue.getIssuedDate(), item, stock.getUnit());
            boolean updateDailyQuantityCombinedReport = true;
            if (dailyQuantityCombinedReport == null) {
                updateDailyQuantityCombinedReport = false;
                dailyQuantityCombinedReport = new DailyQuantityCombinedReport();
                dailyQuantityCombinedReport.setItem(item);
                dailyQuantityCombinedReport.setReportDate(stockIssue.getIssuedDate());
                dailyQuantityCombinedReport.setUnit(stock.getUnit());
            }
            if (!updateDailyQuantityReport)
                dailyQuantityCombinedReport.getSourceDailyQuantityReports().add(dailyQuantityReport);
            dailyQuantityCombinedReport.setQuantityDrawn(dailyQuantityCombinedReport.getQuantityDrawn() + quantityToDeduct);
            dailyQuantityCombinedReport.setClosingBalance(dailyQuantityCombinedReport.getOpeningBalance() + dailyQuantityCombinedReport.getQuantityBought() - dailyQuantityCombinedReport.getQuantityDrawn());
            if (updateDailyQuantityCombinedReport)
                DailyQuantityCombinedReportDao.updateDailyQuantityCombinedReport(dailyQuantityCombinedReport);
            else
                DailyQuantityCombinedReportDao.saveDailyQuantityCombinedReport(dailyQuantityCombinedReport);

            int yearMonth = Utilities.toYearMonthFromDate(stockIssue.getIssuedDate());

            MonthlyQuantityReport monthlyQuantityReport = MonthlyQuantityReportDao.getMonthlyQuantityReportsByStockAndYearAndMonthInMonths(stock, yearMonth);
            boolean updateMonthlyQuantityReport = true;
            if (monthlyQuantityReport == null) {
                updateMonthlyQuantityReport = false;
                monthlyQuantityReport = new MonthlyQuantityReport();
                monthlyQuantityReport.setReportDate(stockIssue.getIssuedDate());
                monthlyQuantityReport.setUnit(stock.getUnit());
                monthlyQuantityReport.setYearAndMonthInMonths(yearMonth);
                monthlyQuantityReport.setRate(stock.getPriceOfReceivedStock());
                monthlyQuantityReport.setStock(stock);
            }
            monthlyQuantityReport.setQuantityDrawn(monthlyQuantityReport.getQuantityDrawn() + quantityToDeduct);
            monthlyQuantityReport.setClosingBalance(monthlyQuantityReport.getOpeningBalance() + monthlyQuantityReport.getQuantityBought() - monthlyQuantityReport.getQuantityDrawn());
            if (updateMonthlyQuantityReport)
                MonthlyQuantityReportDao.updateMonthlyQuantityReport(monthlyQuantityReport);
            else
                MonthlyQuantityReportDao.saveMonthlyQuantityReport(monthlyQuantityReport);

            MonthlyAmountReport monthlyAmountReport = MonthlyAmountReportDao.getMonthlyAmountReportByItemAndYearAndMonthInMonths(item, yearMonth);
            boolean updateMonthlyAmountReport = true;
            if (monthlyAmountReport == null) {
                updateMonthlyAmountReport = false;
                monthlyAmountReport = new MonthlyAmountReport();
                monthlyAmountReport.setReportDate(stockIssue.getIssuedDate());
                monthlyAmountReport.setItem(item);
                monthlyAmountReport.setYearAndMonthInMonths(yearMonth);
            }
            monthlyAmountReport.setAmountDrawn(monthlyAmountReport.getAmountDrawn() + quantityToDeduct * dailyQuantityReport.getRate());
            monthlyAmountReport.setClosingBalance(monthlyAmountReport.getOpeningBalance() + monthlyAmountReport.getAmountBought() - monthlyAmountReport.getAmountDrawn());
            if (updateMonthlyAmountReport)
                MonthlyAmountReportDao.updateMonthlyAmountReport(monthlyAmountReport);
            else
                MonthlyAmountReportDao.saveMonthlyAmountReport(monthlyAmountReport);

            MonthlyQuantityCombinedReport monthlyQuantityCombinedReport = MonthlyQuantityCombinedReportDao.getMonthlyQuantityCombinedReportsByItemAndYearAndMonthInMonthsAndUnit(item, yearMonth, stock.getUnit());
            boolean updateMonthlyQuantityCombinedReport = true;
            if (monthlyQuantityCombinedReport == null) {
                updateMonthlyQuantityCombinedReport = false;
                monthlyQuantityCombinedReport = new MonthlyQuantityCombinedReport();
                monthlyQuantityCombinedReport.setReportDate(stockIssue.getIssuedDate());
                monthlyQuantityCombinedReport.setUnit(stock.getUnit());
                monthlyQuantityCombinedReport.setYearAndMonthInMonths(yearMonth);
                monthlyQuantityCombinedReport.setItem(item);
            }
            monthlyQuantityCombinedReport.setQuantityDrawn(monthlyQuantityCombinedReport.getQuantityDrawn() + quantityToDeduct);
            monthlyQuantityCombinedReport.setClosingBalance(monthlyQuantityCombinedReport.getOpeningBalance() + monthlyQuantityCombinedReport.getQuantityBought() - monthlyQuantityCombinedReport.getQuantityDrawn());
            if (updateMonthlyQuantityReport)
                monthlyQuantityCombinedReport.getSourceMonthlyQuantityReports().add(monthlyQuantityReport);
            if (updateMonthlyQuantityCombinedReport)
                MonthlyQuantityCombinedReportDao.updateMonthlyQuantityCombinedReport(monthlyQuantityCombinedReport);
            else
                MonthlyQuantityCombinedReportDao.saveMonthlyQuantityCombinedReport(monthlyQuantityCombinedReport);
        }
    }

    @ActionMethod("updateStockIssue")
    public void updateStockIssue() {

        if (validateBeforeFormSubmission() && validateBeforeEditFormSubmission()) {

            StockIssue stockIssueToUpdate = currentStockIssue;
            double previousStockIssueAllocatedQuantity = stockIssueToUpdate.getAllocatedUse();
            double previousStockIssueActualQuantity = stockIssueToUpdate.getActualUse();
            loadFormDataIntoStockIssue(stockIssueToUpdate);
            double newStockIssueAllocatedQuantity = stockIssueToUpdate.getAllocatedUse();
            double newStockIssueActualQuantity = stockIssueToUpdate.getActualUse();
            Stock stock = stockContainer.getStock();
            double price = stock.getPriceOfReceivedStock();
            stockIssueToUpdate.setAllocatedUse(newStockIssueAllocatedQuantity);
            stockIssueToUpdate.setAllocatedUseValue(newStockIssueAllocatedQuantity * price);
            double actualUseDifference = newStockIssueActualQuantity - previousStockIssueActualQuantity;

            Item item = stock.getItem();
            double difference = 0;

            if (newStockIssueActualQuantity == 0) {
                if (previousStockIssueActualQuantity == 0) {
                    stock.setQuantityOfIssuedStock(stock.getQuantityOfIssuedStock() + (newStockIssueAllocatedQuantity - previousStockIssueAllocatedQuantity));
                    stock.setValueOfIssuedStock(stock.getQuantityOfIssuedStock() * stock.getPriceOfReceivedStock());
                    stock.setQuantityOfBalancedStock(stock.getQuantityOfBalancedStock() - (newStockIssueAllocatedQuantity - previousStockIssueAllocatedQuantity));
                    stock.setValueOfBalancedStock(stock.getQuantityOfBalancedStock() * stock.getPriceOfReceivedStock());

                    difference = previousStockIssueAllocatedQuantity - newStockIssueAllocatedQuantity;
                } else {
                    stock.setQuantityOfIssuedStock(stock.getQuantityOfIssuedStock() - previousStockIssueAllocatedQuantity);
                    stock.setValueOfIssuedStock(stock.getQuantityOfIssuedStock() * stock.getPriceOfReceivedStock());
                    stock.setQuantityOfBalancedStock(stock.getQuantityOfBalancedStock() + previousStockIssueAllocatedQuantity);
                    stock.setValueOfBalancedStock(stock.getQuantityOfBalancedStock() * stock.getPriceOfReceivedStock());

                    stockIssueToUpdate.setActualUse(newStockIssueActualQuantity);
                    stockIssueToUpdate.setActualUseValue(newStockIssueActualQuantity * price);
                    stockIssueToUpdate.setIncomeFromDifference(stockIssueToUpdate.getAllocatedUseValue() - stockIssueToUpdate.getActualUseValue());

                    difference = previousStockIssueAllocatedQuantity;
                }
            } else {
                stockIssueToUpdate.setActualUse(newStockIssueActualQuantity);
                stockIssueToUpdate.setActualUseValue(newStockIssueActualQuantity * price);
                stockIssueToUpdate.setIncomeFromDifference(stockIssueToUpdate.getAllocatedUseValue() - stockIssueToUpdate.getActualUseValue());

                if (previousStockIssueActualQuantity == 0) {
                    stock.setQuantityOfIssuedStock(stock.getQuantityOfIssuedStock() + (newStockIssueActualQuantity - previousStockIssueAllocatedQuantity));
                    stock.setValueOfIssuedStock(stock.getQuantityOfIssuedStock() * stock.getPriceOfReceivedStock());
                    stock.setQuantityOfBalancedStock(stock.getQuantityOfBalancedStock() - (newStockIssueActualQuantity - previousStockIssueAllocatedQuantity));
                    stock.setValueOfBalancedStock(stock.getQuantityOfBalancedStock() * stock.getPriceOfReceivedStock());

                    difference = previousStockIssueAllocatedQuantity - newStockIssueActualQuantity;
                } else {
                    stock.setQuantityOfIssuedStock(stock.getQuantityOfIssuedStock() + actualUseDifference);
                    stock.setValueOfIssuedStock(stock.getQuantityOfIssuedStock() * stock.getPriceOfReceivedStock());
                    stock.setQuantityOfBalancedStock(stock.getQuantityOfBalancedStock() - actualUseDifference);
                    stock.setValueOfBalancedStock(stock.getQuantityOfBalancedStock() * stock.getPriceOfReceivedStock());

                    difference = previousStockIssueActualQuantity - newStockIssueActualQuantity;
                }

            }

            StockIssueDao.updateStockWithStockIssue(stock, stockIssueToUpdate);
            refreshTable();
            issueTableView.getSelectionModel().select(stockIssueToUpdate);
            loadStockIssueDetails();

            DailyQuantityReport dailyQuantityReport = DailyQuantityReportDao.getDailyQuantityReportByStockAndDate(stock, stockIssueToUpdate.getIssuedDate());
            dailyQuantityReport.setQuantityDrawn(dailyQuantityReport.getQuantityDrawn() - difference);
            dailyQuantityReport.setClosingBalance(dailyQuantityReport.getOpeningBalance() + dailyQuantityReport.getQuantityBought() - dailyQuantityReport.getQuantityDrawn());
            DailyQuantityReportDao.updateDailyQuantityReport(dailyQuantityReport);

            DailyAmountReport dailyAmountReport = DailyAmountReportDao.getDailyAmountReportByDateAndItem(stockIssueToUpdate.getIssuedDate(), item);
            dailyAmountReport.setAmountDrawn(dailyAmountReport.getAmountDrawn() - difference * dailyQuantityReport.getRate());
            dailyAmountReport.setClosingBalance(dailyAmountReport.getOpeningBalance() + dailyAmountReport.getAmountBought() - dailyAmountReport.getAmountDrawn());
            DailyAmountReportDao.updateDailyAmountReport(dailyAmountReport);

            DailyQuantityCombinedReport dailyQuantityCombinedReport = DailyQuantityCombinedReportDao.getDailyQuantityCombinedReportByDateAndItemAndUnit(stockIssueToUpdate.getIssuedDate(), item, stock.getUnit());
            dailyQuantityCombinedReport.setQuantityDrawn(dailyQuantityCombinedReport.getQuantityDrawn() - difference);
            dailyQuantityCombinedReport.setClosingBalance(dailyQuantityCombinedReport.getOpeningBalance() + dailyQuantityCombinedReport.getQuantityBought() - dailyQuantityCombinedReport.getQuantityDrawn());
            DailyQuantityCombinedReportDao.updateDailyQuantityCombinedReport(dailyQuantityCombinedReport);

            int yearMonth = Utilities.toYearMonthFromDate(stockIssueToUpdate.getIssuedDate());

            MonthlyQuantityReport monthlyQuantityReport = MonthlyQuantityReportDao.getMonthlyQuantityReportsByStockAndYearAndMonthInMonths(stock, yearMonth);
            monthlyQuantityReport.setQuantityDrawn(monthlyQuantityReport.getQuantityDrawn() - difference);
            monthlyQuantityReport.setClosingBalance(monthlyQuantityReport.getOpeningBalance() + monthlyQuantityReport.getQuantityBought() - monthlyQuantityReport.getQuantityDrawn());
            MonthlyQuantityReportDao.updateMonthlyQuantityReport(monthlyQuantityReport);

            MonthlyAmountReport monthlyAmountReport = MonthlyAmountReportDao.getMonthlyAmountReportByItemAndYearAndMonthInMonths(item, yearMonth);
            monthlyAmountReport.setAmountDrawn(monthlyAmountReport.getAmountDrawn() - difference * dailyQuantityReport.getRate());
            monthlyAmountReport.setClosingBalance(monthlyAmountReport.getOpeningBalance() + monthlyAmountReport.getAmountBought() - monthlyAmountReport.getAmountDrawn());
            MonthlyAmountReportDao.updateMonthlyAmountReport(monthlyAmountReport);

            MonthlyQuantityCombinedReport monthlyQuantityCombinedReport = MonthlyQuantityCombinedReportDao.getMonthlyQuantityCombinedReportsByItemAndYearAndMonthInMonthsAndUnit(item, yearMonth, stock.getUnit());
            monthlyQuantityCombinedReport.setQuantityDrawn(monthlyQuantityCombinedReport.getQuantityDrawn() - difference);
            monthlyQuantityCombinedReport.setClosingBalance(monthlyQuantityCombinedReport.getOpeningBalance() + monthlyQuantityCombinedReport.getQuantityBought() - monthlyQuantityCombinedReport.getQuantityDrawn());
            MonthlyQuantityCombinedReportDao.updateMonthlyQuantityCombinedReport(monthlyQuantityCombinedReport);
        }
    }

    @ActionMethod("createActualUse")
    public void createActualUse() {

        if (validateBeforeSettingActualUse()) {

            StockIssue stockIssueToUpdate = currentStockIssue;
            double previousStockIssueAllocatedQuantity = stockIssueToUpdate.getAllocatedUse();
            stockIssueToUpdate.setActualUse(Double.parseDouble(actualUseQuantityInput.getText()));
            double newStockIssueActualQuantity = stockIssueToUpdate.getActualUse();
            Stock stock = stockContainer.getStock();
            double price = stock.getPriceOfReceivedStock();
            stockIssueToUpdate.setActualUse(newStockIssueActualQuantity);
            stockIssueToUpdate.setActualUseValue(newStockIssueActualQuantity * price);
            stockIssueToUpdate.setIncomeFromDifference(stockIssueToUpdate.getAllocatedUseValue() - stockIssueToUpdate.getActualUseValue());
            stock.setQuantityOfIssuedStock(stock.getQuantityOfIssuedStock() + (newStockIssueActualQuantity - previousStockIssueAllocatedQuantity));
            stock.setValueOfIssuedStock(stock.getQuantityOfIssuedStock() * stock.getPriceOfReceivedStock());
            stock.setQuantityOfBalancedStock(stock.getQuantityOfBalancedStock() - (newStockIssueActualQuantity - previousStockIssueAllocatedQuantity));
            stock.setValueOfBalancedStock(stock.getQuantityOfBalancedStock() * stock.getPriceOfReceivedStock());
            StockIssueDao.updateStockWithStockIssue(stock, stockIssueToUpdate);
            refreshTable();
            issueTableView.getSelectionModel().select(stockIssueToUpdate);
            loadStockIssueDetails();

            Item item = stock.getItem();
            double difference = stockIssueToUpdate.getAllocatedUse() - stockIssueToUpdate.getActualUse();

            DailyQuantityReport dailyQuantityReport = DailyQuantityReportDao.getDailyQuantityReportByStockAndDate(stock, stockIssueToUpdate.getIssuedDate());
            dailyQuantityReport.setQuantityDrawn(dailyQuantityReport.getQuantityDrawn() - difference);
            dailyQuantityReport.setClosingBalance(dailyQuantityReport.getOpeningBalance() + dailyQuantityReport.getQuantityBought() - dailyQuantityReport.getQuantityDrawn());
            DailyQuantityReportDao.updateDailyQuantityReport(dailyQuantityReport);

            DailyAmountReport dailyAmountReport = DailyAmountReportDao.getDailyAmountReportByDateAndItem(stockIssueToUpdate.getIssuedDate(), item);
            dailyAmountReport.setAmountDrawn(dailyAmountReport.getAmountDrawn() - difference * dailyQuantityReport.getRate());
            dailyAmountReport.setClosingBalance(dailyAmountReport.getOpeningBalance() + dailyAmountReport.getAmountBought() - dailyAmountReport.getAmountDrawn());
            DailyAmountReportDao.updateDailyAmountReport(dailyAmountReport);

            DailyQuantityCombinedReport dailyQuantityCombinedReport = DailyQuantityCombinedReportDao.getDailyQuantityCombinedReportByDateAndItemAndUnit(stockIssueToUpdate.getIssuedDate(), item, stock.getUnit());
            dailyQuantityCombinedReport.setQuantityDrawn(dailyQuantityCombinedReport.getQuantityDrawn() - difference);
            dailyQuantityCombinedReport.setClosingBalance(dailyQuantityCombinedReport.getOpeningBalance() + dailyQuantityCombinedReport.getQuantityBought() - dailyQuantityCombinedReport.getQuantityDrawn());
            DailyQuantityCombinedReportDao.updateDailyQuantityCombinedReport(dailyQuantityCombinedReport);

            int yearMonth = Utilities.toYearMonthFromDate(stockIssueToUpdate.getIssuedDate());

            MonthlyQuantityReport monthlyQuantityReport = MonthlyQuantityReportDao.getMonthlyQuantityReportsByStockAndYearAndMonthInMonths(stock, yearMonth);
            monthlyQuantityReport.setQuantityDrawn(monthlyQuantityReport.getQuantityDrawn() - difference);
            monthlyQuantityReport.setClosingBalance(monthlyQuantityReport.getOpeningBalance() + monthlyQuantityReport.getQuantityBought() - monthlyQuantityReport.getQuantityDrawn());
            MonthlyQuantityReportDao.updateMonthlyQuantityReport(monthlyQuantityReport);

            MonthlyAmountReport monthlyAmountReport = MonthlyAmountReportDao.getMonthlyAmountReportByItemAndYearAndMonthInMonths(item, yearMonth);
            monthlyAmountReport.setAmountDrawn(monthlyAmountReport.getAmountDrawn() - difference * dailyQuantityReport.getRate());
            monthlyAmountReport.setClosingBalance(monthlyAmountReport.getOpeningBalance() + monthlyAmountReport.getAmountBought() - monthlyAmountReport.getAmountDrawn());
            MonthlyAmountReportDao.updateMonthlyAmountReport(monthlyAmountReport);

            MonthlyQuantityCombinedReport monthlyQuantityCombinedReport = MonthlyQuantityCombinedReportDao.getMonthlyQuantityCombinedReportsByItemAndYearAndMonthInMonthsAndUnit(item, yearMonth, stock.getUnit());
            monthlyQuantityCombinedReport.setQuantityDrawn(monthlyQuantityCombinedReport.getQuantityDrawn() - difference);
            monthlyQuantityCombinedReport.setClosingBalance(monthlyQuantityCombinedReport.getOpeningBalance() + monthlyQuantityCombinedReport.getQuantityBought() - monthlyQuantityCombinedReport.getQuantityDrawn());
            MonthlyQuantityCombinedReportDao.updateMonthlyQuantityCombinedReport(monthlyQuantityCombinedReport);
        }
    }

    private boolean validateBeforeSettingActualUse() {
        boolean isErrorFree = true;

        if (actualUseQuantityInput.getText().isEmpty()) {
            actualUseQuantityInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        if (Double.parseDouble(actualUseQuantityInput.getText()) <= 0) {
            actualUseQuantityInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        return isErrorFree;
    }

    private void clearActualUseFormWarnings() {
        actualUseQuantityInput.setStyle("-fx-border-color: null");
    }

    private void clearAllocatedUseFormWarnings() {
        issuedDateDatePicker.setStyle("-fx-border-color: null");
        allocatedUseInput.setStyle("-fx-border-color: null");
    }

    private boolean validateBeforeFormSubmission() {
        boolean isErrorFree = true;

        if (issuedDateDatePicker.getValue() == null) {
            issuedDateDatePicker.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }
        if (allocatedUseInput.getText().isEmpty()) {
            allocatedUseInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        if (Double.parseDouble(allocatedUseInput.getText()) <= 0) {
            allocatedUseInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        if (!actualUseInput.getText().isEmpty() && Double.parseDouble(actualUseInput.getText()) <= 0) {
            actualUseInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        return isErrorFree;
    }

    private boolean validateBeforeEditFormSubmission() {
        boolean isErrorFree = true;

        if (Double.parseDouble(allocatedUseInput.getText()) == currentStockIssue.getAllocatedUse() && actualUseInput.getText().isEmpty()) {
            allocatedUseInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        if (!actualUseInput.getText().isEmpty() && Double.parseDouble(actualUseInput.getText()) == currentStockIssue.getActualUse()) {
            actualUseInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        return isErrorFree;
    }

    private void loadFormDataIntoStockIssue(StockIssue stockIssue) {
        Date issuedDateUtilDate = Date.from(issuedDateDatePicker.getValue().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        stockIssue.setIssuedDate(issuedDateUtilDate);
        stockIssue.setParticular(particularInput.getText());
        stockIssue.setAllocatedUse(Double.parseDouble(allocatedUseInput.getText()));
        if (actualUseInput.getText() != null && !actualUseInput.getText().equals(""))
            stockIssue.setActualUse(Double.parseDouble(actualUseInput.getText()));
    }

    private void loadStockDataIntoForm(StockIssue stockIssue) {
        Instant instant = Instant.ofEpochMilli(stockIssue.getIssuedDate().getTime());
        LocalDate localDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
        issuedDateDatePicker.setValue(localDate);
        particularInput.setText(stockIssue.getParticular());
        allocatedUseInput.setText(stockIssue.getAllocatedUse() + "");
        actualUseInput.setText(stockIssue.getActualUse() + "");

    }

    private void refreshTable() {
        issueTableView.getItems().clear();
        issueTableView.getItems().setAll(stockContainer.getStock().getStockIssues());
    }

    @ActionMethod("loadEditIssueForm")
    public void loadEditStockIssueForm() {
        prepareEditStockIssue();
        loadNewStockIssueGrid();
        loadStockDataIntoForm(currentStockIssue);
    }

    private void prepareEditStockIssue() {
        allocateIssueText.setText("Edit Stock Issue");
        issuedDateDatePicker.setDisable(true);
        createIssueButton.setVisible(false);
        saveIssueButton.setVisible(true);
        saveIssueButton.toFront();
    }

    @ActionMethod("loadActualUseForm")
    public void loadActualUseForm() {
        loadAddActualUseGrid();
    }

    private void loadAddActualUseGrid() {
        enableThisGrid(addActualUseGrid);
        clearActualUseFields();
    }

    @ActionMethod("loadDeleteIssueForm")
    public void loadDeleteIssueForm() {
        loadDeleteIssueGrid();
    }

    private void loadDeleteIssueGrid() {
        enableThisGrid(confirmDeleteIssueGrid);
    }

    @ActionMethod("confirmDeleteIssue")
    public void confirmDeleteIssue() {
        Stock stock = stockContainer.getStock();
        stock.getStockIssues().remove(currentStockIssue);
        double valueToAdjust = currentStockIssue.getAllocatedUse();
        if (currentStockIssue.getActualUse() != 0)
            valueToAdjust = currentStockIssue.getActualUse();

        stock.setQuantityOfIssuedStock(stock.getQuantityOfIssuedStock() - valueToAdjust);
        stock.setValueOfIssuedStock(stock.getQuantityOfIssuedStock() * stock.getPriceOfReceivedStock());
        stock.setQuantityOfBalancedStock(stock.getQuantityOfBalancedStock() + valueToAdjust);
        stock.setValueOfBalancedStock(stock.getQuantityOfBalancedStock() * stock.getPriceOfReceivedStock());

        Date issuedDate = currentStockIssue.getIssuedDate();
        double issuedQuantity = currentStockIssue.getAllocatedUse();
        if (currentStockIssue.getActualUse() != 0)
            issuedQuantity = currentStockIssue.getActualUse();

        StockIssueDao.deleteStockIssueWithStock(currentStockIssue, stock);
        refreshTable();
        loadNewStockIssueForm();

        Item item = stock.getItem();

        DailyQuantityReport dailyQuantityReport = DailyQuantityReportDao.getDailyQuantityReportByStockAndDate(stock, issuedDate);
        dailyQuantityReport.setQuantityDrawn(dailyQuantityReport.getQuantityDrawn() - issuedQuantity);
        dailyQuantityReport.setClosingBalance(dailyQuantityReport.getOpeningBalance() + dailyQuantityReport.getQuantityBought() - dailyQuantityReport.getQuantityDrawn());
        DailyQuantityReportDao.updateDailyQuantityReport(dailyQuantityReport);

        DailyAmountReport dailyAmountReport = DailyAmountReportDao.getDailyAmountReportByDateAndItem(issuedDate, item);
        dailyAmountReport.setAmountDrawn(dailyAmountReport.getAmountDrawn() - issuedQuantity * dailyQuantityReport.getRate());
        dailyAmountReport.setClosingBalance(dailyAmountReport.getOpeningBalance() + dailyAmountReport.getAmountBought() - dailyAmountReport.getAmountDrawn());
        DailyAmountReportDao.updateDailyAmountReport(dailyAmountReport);

        DailyQuantityCombinedReport dailyQuantityCombinedReport = DailyQuantityCombinedReportDao.getDailyQuantityCombinedReportByDateAndItemAndUnit(issuedDate, item, stock.getUnit());
        dailyQuantityCombinedReport.setQuantityDrawn(dailyQuantityCombinedReport.getQuantityDrawn() - issuedQuantity);
        dailyQuantityCombinedReport.setClosingBalance(dailyQuantityCombinedReport.getOpeningBalance() + dailyQuantityCombinedReport.getQuantityBought() - dailyQuantityCombinedReport.getQuantityDrawn());
        DailyQuantityCombinedReportDao.updateDailyQuantityCombinedReport(dailyQuantityCombinedReport);

        int yearMonth = Utilities.toYearMonthFromDate(issuedDate);

        MonthlyQuantityReport monthlyQuantityReport = MonthlyQuantityReportDao.getMonthlyQuantityReportsByStockAndYearAndMonthInMonths(stock, yearMonth);
        monthlyQuantityReport.setQuantityDrawn(monthlyQuantityReport.getQuantityDrawn() - issuedQuantity);
        monthlyQuantityReport.setClosingBalance(monthlyQuantityReport.getOpeningBalance() + monthlyQuantityReport.getQuantityBought() - monthlyQuantityReport.getQuantityDrawn());
        MonthlyQuantityReportDao.updateMonthlyQuantityReport(monthlyQuantityReport);

        MonthlyAmountReport monthlyAmountReport = MonthlyAmountReportDao.getMonthlyAmountReportByItemAndYearAndMonthInMonths(item, yearMonth);
        monthlyAmountReport.setAmountDrawn(monthlyAmountReport.getAmountDrawn() - issuedQuantity * dailyQuantityReport.getRate());
        monthlyAmountReport.setClosingBalance(monthlyAmountReport.getOpeningBalance() + monthlyAmountReport.getAmountBought() - monthlyAmountReport.getAmountDrawn());
        MonthlyAmountReportDao.updateMonthlyAmountReport(monthlyAmountReport);

        MonthlyQuantityCombinedReport monthlyQuantityCombinedReport = MonthlyQuantityCombinedReportDao.getMonthlyQuantityCombinedReportsByItemAndYearAndMonthInMonthsAndUnit(item, yearMonth, stock.getUnit());
        monthlyQuantityCombinedReport.setQuantityDrawn(monthlyQuantityCombinedReport.getQuantityDrawn() - issuedQuantity);
        monthlyQuantityCombinedReport.setClosingBalance(monthlyQuantityCombinedReport.getOpeningBalance() + monthlyQuantityCombinedReport.getQuantityBought() - monthlyQuantityCombinedReport.getQuantityDrawn());
        MonthlyQuantityCombinedReportDao.updateMonthlyQuantityCombinedReport(monthlyQuantityCombinedReport);
    }

    @ActionMethod("cancelDeleteStockIssue")
    public void cancelDeleteStockIssue() {
        loadStockIssueDetails();
    }

    private boolean isDeletable() {
        return currentStockIssue.getIssuedDate().equals(Utilities.toUtilDateFromLocalDate(Utilities.toLocalDateFromUtilDate(new Date())));
    }
}
