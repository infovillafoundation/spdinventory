package org.bitbucket.infovillafoundation.spd.inventory.models;

import org.bitbucket.infovillafoundation.spd.inventory.entities.MonthlyQuantityReport;
import org.bitbucket.infovillafoundation.spd.inventory.utils.Utilities;

/**
 * Created by Sandah Aung on 31/1/15.
 */
public class MonthlyQuantityWithItem {

    private MonthlyQuantityReport monthlyQuantityReport;

    public MonthlyQuantityWithItem(MonthlyQuantityReport monthlyQuantityReport) {
        this.monthlyQuantityReport = monthlyQuantityReport;
    }

    public String getItemName() {
        return monthlyQuantityReport.getStock().getItem().getName();
    }

    public String getReportDate() {
        return Utilities.fromDate(monthlyQuantityReport.getReportDate());
    }

    public String getUnit() {
        return monthlyQuantityReport.getUnit();
    }

    public String getOpeningBalance() {
        return monthlyQuantityReport.getOpeningBalance() + " " + getUnit();
    }

    public String getRate() {
        return monthlyQuantityReport.getRate() + "";
    }

    public String getQuantityBought() {
        return monthlyQuantityReport.getQuantityBought() + " " + getUnit();
    }

    public String getQuantityDrawn() {
        return monthlyQuantityReport.getQuantityDrawn() + " " + getUnit();
    }

    public String getClosingBalance() {
        return monthlyQuantityReport.getClosingBalance() + " " + getUnit();
    }
}
