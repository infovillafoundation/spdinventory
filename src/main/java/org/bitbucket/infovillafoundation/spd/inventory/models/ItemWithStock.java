package org.bitbucket.infovillafoundation.spd.inventory.models;

/**
 * Created by Sandah Aung on 21/1/15.
 */
public abstract class ItemWithStock {
    public abstract String getName();

    public abstract String getPrice();

    public abstract String getReceivedDate();

    public abstract String getReceivedQuantity();

    public abstract String getReceivedValue();

    public abstract String getIssuedQuantity();

    public abstract String getIssuedValue();

    public abstract String getBalancedQuantity();

    public abstract String getBalancedValue();
}
