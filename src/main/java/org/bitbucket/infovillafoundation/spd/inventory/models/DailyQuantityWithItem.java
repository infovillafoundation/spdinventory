package org.bitbucket.infovillafoundation.spd.inventory.models;

import org.bitbucket.infovillafoundation.spd.inventory.entities.DailyQuantityReport;
import org.bitbucket.infovillafoundation.spd.inventory.utils.Utilities;

/**
 * Created by Sandah Aung on 31/1/15.
 */
public class DailyQuantityWithItem {

    private DailyQuantityReport dailyQuantityReport;

    public DailyQuantityWithItem(DailyQuantityReport dailyQuantityReport) {
        this.dailyQuantityReport = dailyQuantityReport;
    }

    public String getItemName() {
        return dailyQuantityReport.getStock().getItem().getName();
    }

    public String getReportDate() {
        return Utilities.fromDate(dailyQuantityReport.getReportDate());
    }

    public String getUnit() {
        return dailyQuantityReport.getUnit();
    }

    public String getOpeningBalance() {
        return dailyQuantityReport.getOpeningBalance() + " " + getUnit();
    }

    public String getRate() {
        return dailyQuantityReport.getRate() + "";
    }

    public String getQuantityBought() {
        return dailyQuantityReport.getQuantityBought() + " " + getUnit();
    }

    public String getQuantityDrawn() {
        return dailyQuantityReport.getQuantityDrawn() + " " + getUnit();
    }

    public String getClosingBalance() {
        return dailyQuantityReport.getClosingBalance() + " " + getUnit();
    }
}
