package org.bitbucket.infovillafoundation.spd.inventory.daos;

import org.bitbucket.infovillafoundation.spd.inventory.entities.Item;
import org.bitbucket.infovillafoundation.spd.inventory.entities.Stock;
import org.bitbucket.infovillafoundation.spd.inventory.entities.StockReceipt;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Sandah Aung on 17/12/14.
 */
public class ItemDao extends Dao {

    public static List<Item> getTopLevelItems() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Item> query = em.createNamedQuery("Item.findAllTopLevel", Item.class);
        List<Item> items = query.getResultList();
        return items;
    }

    public static List<Item> getItemsByNameLike(String searchTerm) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Item> query = em.createNamedQuery("Item.findByNameLike", Item.class);
        query.setParameter("name", "%" + searchTerm + "%");
        List<Item> items = query.getResultList();
        return items;
    }

    public static void saveItem(Item item) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(item);
        em.getTransaction().commit();
        em.close();
    }

    public static void saveItemWithStock(Item item, Stock stock) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stock);
        em.persist(item);
        em.getTransaction().commit();
        em.close();
    }

    public static void saveItemWithStockAndStockReceipt(Item item, Stock stock, StockReceipt stockReceipt) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stockReceipt);
        em.persist(stock);
        em.persist(item);
        em.getTransaction().commit();
        em.close();
    }

    public static void updateItem(Item item) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(item);
        em.getTransaction().commit();
        em.close();
    }

    public static void deleteItem(Item item) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.contains(item) ? item : em.merge(item));
        em.getTransaction().commit();
        em.close();
    }
}
