package org.bitbucket.infovillafoundation.spd.inventory.converters;

import org.bitbucket.infovillafoundation.spd.inventory.models.OpenCloseStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by Sandah Aung on 1/1/15.
 */

@Converter
public class OpenCloseStatusConverter implements AttributeConverter<OpenCloseStatus, String> {
    @Override
    public String convertToDatabaseColumn(OpenCloseStatus attribute) {
        if (attribute == OpenCloseStatus.OPEN)
            return "open";
        else
            return "close";
    }

    @Override
    public OpenCloseStatus convertToEntityAttribute(String dbData) {
        if (dbData.equals("open"))
            return OpenCloseStatus.OPEN;
        else
            return OpenCloseStatus.CLOSE;
    }
}
