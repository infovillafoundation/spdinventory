package org.bitbucket.infovillafoundation.spd.inventory.controllers;

import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.action.BackAction;
import io.datafx.controller.flow.action.LinkAction;
import io.datafx.controller.flow.context.ActionHandler;
import io.datafx.controller.flow.context.FlowActionHandler;
import io.datafx.controller.util.VetoException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.bitbucket.infovillafoundation.spd.inventory.Main;
import org.bitbucket.infovillafoundation.spd.inventory.daos.ItemDao;
import org.bitbucket.infovillafoundation.spd.inventory.entities.Item;
import org.bitbucket.infovillafoundation.spd.inventory.entities.Stock;
import org.bitbucket.infovillafoundation.spd.inventory.models.*;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandah Aung on 16/12/14.
 */

@FXMLController(value = "/fxml/detailed_report.fxml", title = "Detailed Report")
public class DetailedReportController {

    @ActionHandler
    private FlowActionHandler handler;

    @FXML
    private VBox pane;

    @Inject
    private ItemContainer itemContainer;

    @FXML
    @BackAction
    private Button backButton;

    @FXML
    @ActionTrigger("saveReport")
    private Button saveReportButton;

    @FXML
    private CheckBox allItemsCheckBox;

    @FXML
    @ActionTrigger("loadStock")
    private Button manageStockButton;

    @FXML
    private TableView<UniqueItemWithStock> stockTableView;

    @FXML
    private TableColumn<UniqueItemWithStock, String> itemNameColumn;

    @FXML
    private TableColumn<UniqueItemWithStock, String> receivedStockValueColumn;

    @FXML
    private TableColumn<UniqueItemWithStock, String> issuedStockValueColumn;

    @FXML
    private TableColumn<UniqueItemWithStock, String> balancedStockValueColumn;

    private UniqueItemWithStock currentUniqueItemWithStock;

    @FXML
    private CheckBox allItemsCheckBoxSummaryTab;

    @FXML
    @ActionTrigger("loadStockSummaryTab")
    private Button manageStockButtonSummaryTab;

    @FXML
    private TableView<RepeatableItemWithStock> stockTableViewSummaryTab;

    @FXML
    private TableColumn<RepeatableItemWithStock, String> itemNameColumnSummaryTab;

    @FXML
    private TableColumn<RepeatableItemWithStock, String> receivedStockValueColumnSummaryTab;

    @FXML
    private TableColumn<RepeatableItemWithStock, String> issuedStockValueColumnSummaryTab;

    @FXML
    private TableColumn<RepeatableItemWithStock, String> balancedStockValueColumnSummaryTab;

    private double receivedTotalSummaryTab;
    private double issuedTotalSummaryTab;
    private double balancedTotalSummaryTab;

    private RepeatableItemWithStock currentSummaryRepeatableItemWithStock;

    @FXML
    private CheckBox allItemsCheckBoxAllTab;

    @FXML
    @ActionTrigger("loadStockAllTab")
    private Button manageStockButtonAllTab;

    @FXML
    private TableView<RepeatableItemWithStock> stockTableViewAllTab;

    @FXML
    private TableColumn<RepeatableItemWithStock, String> itemNameColumnAllTab;

    @FXML
    private TableColumn<RepeatableItemWithStock, String> receivedStockValueColumnAllTab;

    @FXML
    private TableColumn<RepeatableItemWithStock, String> issuedStockValueColumnAllTab;

    @FXML
    private TableColumn<RepeatableItemWithStock, String> balancedStockValueColumnAllTab;

    private RepeatableItemWithStock currentNonValueRepeatableItemWithStock;

    @FXML
    private CheckBox allItemsCheckBoxNonValueTab;

    @FXML
    @ActionTrigger("loadStockNonValueTab")
    private Button manageStockButtonNonValueTab;

    @FXML
    private TableView<RepeatableItemWithStock> stockTableViewNonValueTab;

    @FXML
    private TableColumn<RepeatableItemWithStock, String> itemNameColumnNonValueTab;

    @FXML
    private TableColumn<RepeatableItemWithStock, String> receivedStockValueColumnNonValueTab;

    @FXML
    private TableColumn<RepeatableItemWithStock, String> issuedStockValueColumnNonValueTab;

    @FXML
    private TableColumn<RepeatableItemWithStock, String> balancedStockValueColumnNonValueTab;

    @FXML
    private TableView<ReportByCategory> stockTableViewCategoryReportTab;

    private RepeatableItemWithStock currentRepeatableItemWithStock;

    @FXML
    private TableColumn<ReportByCategory, String> stockTitle;

    @FXML
    private TableColumn<ReportByCategory, String> quantity;

    @FXML
    private TableColumn<ReportByCategory, String> rate;

    @FXML
    private TableColumn<ReportByCategory, String> amount;

    private double receivedTotalReportByCategory;
    private double issuedTotalReportByCategory;
    private double balancedTotalReportByCategory;

    @LinkAction(ReportByDateController.class)
    @FXML
    private Button reportByDateButton;

    @LinkAction(ReportByMonthController.class)
    @FXML
    private Button reportByMonthButton;

    @PostConstruct
    public void init() {
        setupBackShortcutListener();

        loadItemsWithStockIntoTable();
        loadItemsWithAllStocksIntoTable();
        loadItemsWithAllStocksIntoSummaryTable();
        loadItemsWithAllStocksIntoNonValueTable();
        loadReportByCategoryTable();

        setupRowClickListener();
        setupAllItemsListener();
        setupTableDoubleClickListener();
        setupCellFactories();

        setupRepeatableRowClickListener();
        setupRepeatableAllItemsListener();
        setupRepeatableTableDoubleClickListener();
        setupRepeatableCellFactories();

        setupSummaryRowClickListener();
        setupSummaryAllItemsListener();
        setupSummaryTableDoubleClickListener();
        setupSummaryCellFactories();

        setupNonValueAllItemsListener();

        setupReportByCategoryCellFactories();
    }

    private void setupBackShortcutListener() {
        final KeyCombination ctrlB = new KeyCodeCombination(KeyCode.B, KeyCombination.CONTROL_DOWN);
        pane.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (ctrlB.match(event)) {
                    try {
                        handler.navigateBack();
                    } catch (VetoException e) {
                    } catch (FlowException e) {
                    }
                }
            }
        });
    }

    private void setupRowClickListener() {
        stockTableView
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(new ChangeListener<UniqueItemWithStock>() {
                                 @Override
                                 public void changed(ObservableValue<? extends UniqueItemWithStock> observable, UniqueItemWithStock oldValue, UniqueItemWithStock newValue) {
                                     currentUniqueItemWithStock = newValue;
                                     if (currentUniqueItemWithStock == null) {
                                         manageStockButton.setVisible(false);
                                     } else {
                                         if (currentUniqueItemWithStock instanceof DummyItemWithStock)
                                             manageStockButton.setVisible(false);
                                         else
                                             manageStockButton.setVisible(true);
                                     }

                                 }
                             }

                );
    }

    private void setupTableDoubleClickListener() {
        stockTableView.setRowFactory(tv -> {
            TableRow<UniqueItemWithStock> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    currentUniqueItemWithStock = row.getItem();
                    if (!(currentUniqueItemWithStock instanceof DummyItemWithStock))
                        loadStockController();
                }
            });
            return row;
        });
    }

    private void setupNonValueAllItemsListener() {
        allItemsCheckBoxNonValueTab.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue)
                    loadAllItemsWithAllStocksIntoNonValueTable();
                else
                    loadItemsWithAllStocksIntoNonValueTable();
            }
        });
    }

    private void setupAllItemsListener() {
        allItemsCheckBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue)
                    loadAllItemsWithStockIntoTable();
                else
                    loadItemsWithStockIntoTable();
            }
        });
    }

    private void setupCellFactories() {
        itemNameColumn.setCellFactory(new Callback<TableColumn<UniqueItemWithStock, String>, TableCell<UniqueItemWithStock, String>>() {
            @Override
            public TableCell<UniqueItemWithStock, String> call(TableColumn<UniqueItemWithStock, String> param) {
                return new TableCell<UniqueItemWithStock, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item);
                        if (item != null && item.equals("Total")) {
                            setStyle("-fx-font-weight: bold;");
                        } else {
                            setStyle("-fx-font-weight: null;");
                        }
                    }
                };
            }
        });

        receivedStockValueColumn.setCellFactory(new Callback<TableColumn<UniqueItemWithStock, String>, TableCell<UniqueItemWithStock, String>>() {
            @Override
            public TableCell<UniqueItemWithStock, String> call(TableColumn<UniqueItemWithStock, String> param) {
                return new TableCell<UniqueItemWithStock, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item);
                        DummyItemWithStock dummyItemWithStock = null;
                        if (getTableRow().getItem() instanceof DummyItemWithStock)
                            dummyItemWithStock = (DummyItemWithStock) getTableRow().getItem();
                        if (item != null && dummyItemWithStock != null && "Total".equals(dummyItemWithStock.getName())) {
                            setStyle("-fx-font-weight: bold;");
                        } else {
                            setStyle("-fx-font-weight: null;");
                        }
                    }
                };
            }
        });

        issuedStockValueColumn.setCellFactory(new Callback<TableColumn<UniqueItemWithStock, String>, TableCell<UniqueItemWithStock, String>>() {
            @Override
            public TableCell<UniqueItemWithStock, String> call(TableColumn<UniqueItemWithStock, String> param) {
                return new TableCell<UniqueItemWithStock, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item);
                        DummyItemWithStock dummyItemWithStock = null;
                        if (getTableRow().getItem() instanceof DummyItemWithStock)
                            dummyItemWithStock = (DummyItemWithStock) getTableRow().getItem();
                        if (item != null && dummyItemWithStock != null && "Total".equals(dummyItemWithStock.getName())) {
                            setStyle("-fx-font-weight: bold;");
                        } else {
                            setStyle("-fx-font-weight: null;");
                        }
                    }
                };
            }
        });

        balancedStockValueColumn.setCellFactory(new Callback<TableColumn<UniqueItemWithStock, String>, TableCell<UniqueItemWithStock, String>>() {
            @Override
            public TableCell<UniqueItemWithStock, String> call(TableColumn<UniqueItemWithStock, String> param) {
                return new TableCell<UniqueItemWithStock, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item);
                        DummyItemWithStock dummyItemWithStock = null;
                        if (getTableRow().getItem() instanceof DummyItemWithStock)
                            dummyItemWithStock = (DummyItemWithStock) getTableRow().getItem();
                        if (item != null && dummyItemWithStock != null && "Total".equals(dummyItemWithStock.getName())) {
                            setStyle("-fx-font-weight: bold;");
                        } else {
                            setStyle("-fx-font-weight: null;");
                        }
                    }
                };
            }
        });
    }

    private void setupRepeatableCellFactories() {
        itemNameColumnAllTab.setCellFactory(new Callback<TableColumn<RepeatableItemWithStock, String>, TableCell<RepeatableItemWithStock, String>>() {
            @Override
            public TableCell<RepeatableItemWithStock, String> call(TableColumn<RepeatableItemWithStock, String> param) {
                return new TableCell<RepeatableItemWithStock, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item);
                        if (item != null && item.equals("Total")) {
                            setStyle("-fx-font-weight: bold;");
                        } else {
                            setStyle("-fx-font-weight: null;");
                        }
                    }
                };
            }
        });

        receivedStockValueColumnAllTab.setCellFactory(new Callback<TableColumn<RepeatableItemWithStock, String>, TableCell<RepeatableItemWithStock, String>>() {
            @Override
            public TableCell<RepeatableItemWithStock, String> call(TableColumn<RepeatableItemWithStock, String> param) {
                return new TableCell<RepeatableItemWithStock, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item);
                        DummyItemWithStock dummyItemWithStock = null;
                        if (getTableRow().getItem() instanceof DummyItemWithStock)
                            dummyItemWithStock = (DummyItemWithStock) getTableRow().getItem();
                        if (item != null && dummyItemWithStock != null && "Total".equals(dummyItemWithStock.getName())) {
                            setStyle("-fx-font-weight: bold;");
                        } else {
                            setStyle("-fx-font-weight: null;");
                        }
                    }
                };
            }
        });

        issuedStockValueColumnAllTab.setCellFactory(new Callback<TableColumn<RepeatableItemWithStock, String>, TableCell<RepeatableItemWithStock, String>>() {
            @Override
            public TableCell<RepeatableItemWithStock, String> call(TableColumn<RepeatableItemWithStock, String> param) {
                return new TableCell<RepeatableItemWithStock, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item);
                        DummyItemWithStock dummyItemWithStock = null;
                        if (getTableRow().getItem() instanceof DummyItemWithStock)
                            dummyItemWithStock = (DummyItemWithStock) getTableRow().getItem();
                        if (item != null && dummyItemWithStock != null && "Total".equals(dummyItemWithStock.getName())) {
                            setStyle("-fx-font-weight: bold;");
                        } else {
                            setStyle("-fx-font-weight: null;");
                        }
                    }
                };
            }
        });

        balancedStockValueColumnAllTab.setCellFactory(new Callback<TableColumn<RepeatableItemWithStock, String>, TableCell<RepeatableItemWithStock, String>>() {
            @Override
            public TableCell<RepeatableItemWithStock, String> call(TableColumn<RepeatableItemWithStock, String> param) {
                return new TableCell<RepeatableItemWithStock, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item);
                        DummyItemWithStock dummyItemWithStock = null;
                        if (getTableRow().getItem() instanceof DummyItemWithStock)
                            dummyItemWithStock = (DummyItemWithStock) getTableRow().getItem();
                        if (item != null && dummyItemWithStock != null && "Total".equals(dummyItemWithStock.getName())) {
                            setStyle("-fx-font-weight: bold;");
                        } else {
                            setStyle("-fx-font-weight: null;");
                        }
                    }
                };
            }
        });
    }

    private void setupRepeatableRowClickListener() {
        stockTableViewAllTab
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(new ChangeListener<RepeatableItemWithStock>() {
                                 @Override
                                 public void changed(ObservableValue<? extends RepeatableItemWithStock> observable, RepeatableItemWithStock oldValue, RepeatableItemWithStock newValue) {
                                     currentRepeatableItemWithStock = newValue;
                                     if (currentRepeatableItemWithStock == null) {
                                         manageStockButtonAllTab.setVisible(false);
                                     } else {
                                         if (currentRepeatableItemWithStock instanceof RepeatableDummyItemWithStock)
                                             manageStockButtonAllTab.setVisible(false);
                                         else
                                             manageStockButtonAllTab.setVisible(true);
                                     }

                                 }
                             }

                );
    }

    private void setupRepeatableTableDoubleClickListener() {
        stockTableViewAllTab.setRowFactory(tv -> {
            TableRow<RepeatableItemWithStock> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    currentRepeatableItemWithStock = row.getItem();
                    if (!(currentRepeatableItemWithStock instanceof RepeatableDummyItemWithStock))
                        loadRepeatableStockController();
                }
            });
            return row;
        });
    }

    private void setupRepeatableAllItemsListener() {
        allItemsCheckBoxAllTab.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue)
                    loadAllItemsWithAllStocksIntoTable();
                else
                    loadItemsWithAllStocksIntoTable();
            }
        });
    }

    private void setupSummaryCellFactories() {
        itemNameColumnSummaryTab.setCellFactory(new Callback<TableColumn<RepeatableItemWithStock, String>, TableCell<RepeatableItemWithStock, String>>() {
            @Override
            public TableCell<RepeatableItemWithStock, String> call(TableColumn<RepeatableItemWithStock, String> param) {
                return new TableCell<RepeatableItemWithStock, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item);
                        if (item != null && item.equals("Total")) {
                            setStyle("-fx-font-weight: bold;");
                        } else {
                            setStyle("-fx-font-weight: null;");
                        }
                    }
                };
            }
        });

        receivedStockValueColumnSummaryTab.setCellFactory(new Callback<TableColumn<RepeatableItemWithStock, String>, TableCell<RepeatableItemWithStock, String>>() {
            @Override
            public TableCell<RepeatableItemWithStock, String> call(TableColumn<RepeatableItemWithStock, String> param) {
                return new TableCell<RepeatableItemWithStock, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item);
                        DummyItemWithStock dummyItemWithStock = null;
                        if (getTableRow().getItem() instanceof DummyItemWithStock)
                            dummyItemWithStock = (DummyItemWithStock) getTableRow().getItem();
                        if (item != null && dummyItemWithStock != null && "Total".equals(dummyItemWithStock.getName())) {
                            setStyle("-fx-font-weight: bold;");
                        } else {
                            setStyle("-fx-font-weight: null;");
                        }
                    }
                };
            }
        });

        issuedStockValueColumnSummaryTab.setCellFactory(new Callback<TableColumn<RepeatableItemWithStock, String>, TableCell<RepeatableItemWithStock, String>>() {
            @Override
            public TableCell<RepeatableItemWithStock, String> call(TableColumn<RepeatableItemWithStock, String> param) {
                return new TableCell<RepeatableItemWithStock, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item);
                        DummyItemWithStock dummyItemWithStock = null;
                        if (getTableRow().getItem() instanceof DummyItemWithStock)
                            dummyItemWithStock = (DummyItemWithStock) getTableRow().getItem();
                        if (item != null && dummyItemWithStock != null && "Total".equals(dummyItemWithStock.getName())) {
                            setStyle("-fx-font-weight: bold;");
                        } else {
                            setStyle("-fx-font-weight: null;");
                        }
                    }
                };
            }
        });

        balancedStockValueColumnSummaryTab.setCellFactory(new Callback<TableColumn<RepeatableItemWithStock, String>, TableCell<RepeatableItemWithStock, String>>() {
            @Override
            public TableCell<RepeatableItemWithStock, String> call(TableColumn<RepeatableItemWithStock, String> param) {
                return new TableCell<RepeatableItemWithStock, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item);
                        DummyItemWithStock dummyItemWithStock = null;
                        if (getTableRow().getItem() instanceof DummyItemWithStock)
                            dummyItemWithStock = (DummyItemWithStock) getTableRow().getItem();
                        if (item != null && dummyItemWithStock != null && "Total".equals(dummyItemWithStock.getName())) {
                            setStyle("-fx-font-weight: bold;");
                        } else {
                            setStyle("-fx-font-weight: null;");
                        }
                    }
                };
            }
        });
    }

    private void setupSummaryRowClickListener() {
        stockTableViewSummaryTab
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(new ChangeListener<RepeatableItemWithStock>() {
                                 @Override
                                 public void changed(ObservableValue<? extends RepeatableItemWithStock> observable, RepeatableItemWithStock oldValue, RepeatableItemWithStock newValue) {
                                     currentSummaryRepeatableItemWithStock = newValue;
                                     if (currentSummaryRepeatableItemWithStock == null) {
                                         manageStockButtonSummaryTab.setVisible(false);
                                     } else {
                                         if (currentSummaryRepeatableItemWithStock instanceof RepeatableDummyItemWithStock)
                                             manageStockButtonSummaryTab.setVisible(false);
                                         else
                                             manageStockButtonSummaryTab.setVisible(true);
                                     }

                                 }
                             }

                );
    }

    private void setupSummaryTableDoubleClickListener() {
        stockTableViewSummaryTab.setRowFactory(tv -> {
            TableRow<RepeatableItemWithStock> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    currentSummaryRepeatableItemWithStock = row.getItem();
                    if (!(currentSummaryRepeatableItemWithStock instanceof RepeatableDummyItemWithStock))
                        loadSummaryStockController();
                }
            });
            return row;
        });
    }

    private void setupSummaryAllItemsListener() {
        allItemsCheckBoxSummaryTab.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue)
                    loadAllItemsWithAllStocksIntoSummaryTable();
                else
                    loadItemsWithAllStocksIntoSummaryTable();
            }
        });
    }

    private void setupReportByCategoryCellFactories() {
        stockTitle.setCellFactory(new Callback<TableColumn<ReportByCategory, String>, TableCell<ReportByCategory, String>>() {
            @Override
            public TableCell<ReportByCategory, String> call(TableColumn<ReportByCategory, String> param) {
                return new TableCell<ReportByCategory, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item);
                        if (item != null && item.equals("Total")) {
                            setStyle("-fx-font-weight: bold;");
                        } else {
                            setStyle("-fx-font-weight: null;");
                        }
                    }
                };
            }
        });

        amount.setCellFactory(new Callback<TableColumn<ReportByCategory, String>, TableCell<ReportByCategory, String>>() {
            @Override
            public TableCell<ReportByCategory, String> call(TableColumn<ReportByCategory, String> param) {
                return new TableCell<ReportByCategory, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item);
                        DummyReportByCategory dummyReportByCategory = null;
                        if (getTableRow().getItem() instanceof DummyReportByCategory)
                            dummyReportByCategory = (DummyReportByCategory) getTableRow().getItem();
                        if (item != null && dummyReportByCategory != null && "Total".equals(dummyReportByCategory.title)) {
                            setStyle("-fx-font-weight: bold;");
                        } else {
                            setStyle("-fx-font-weight: null;");
                        }
                    }
                };
            }
        });
    }

    private void loadItemsWithStockIntoTable() {
        List<Item> currentItemAndChildren = returnAllItems(itemContainer.getItem());
        loadItemsWithStockIntoTable(currentItemAndChildren);
    }

    private void loadItemsWithAllStocksIntoTable() {
        List<Item> currentItemAndChildren = returnAllItems(itemContainer.getItem());
        loadItemsWithAllStocksIntoTable(currentItemAndChildren);
    }

    private void loadItemsWithAllStocksIntoSummaryTable() {
        List<Item> currentItemAndChildren = returnAllItems(itemContainer.getItem());
        loadItemsWithAllStocksIntoSummaryTable(currentItemAndChildren);
    }

    private void loadItemsWithAllStocksIntoNonValueTable() {
        List<Item> currentItemAndChildren = returnAllItems(itemContainer.getItem());
        loadItemsWithAllStocksIntoNonValueTable(currentItemAndChildren);
    }

    public void loadAllItemsWithAllStocksIntoNonValueTable() {
        List<Item> allItemsAndChildren = new ArrayList<>();
        List<Item> topLevelItems = ItemDao.getTopLevelItems();
        for (Item item : topLevelItems) {
            allItemsAndChildren.addAll(returnAllItems(item));
        }
        loadItemsWithAllStocksIntoNonValueTable(allItemsAndChildren);
    }

    public void loadAllItemsWithStockIntoTable() {
        List<Item> allItemsAndChildren = new ArrayList<>();
        List<Item> topLevelItems = ItemDao.getTopLevelItems();
        for (Item item : topLevelItems) {
            allItemsAndChildren.addAll(returnAllItems(item));
        }
        loadItemsWithStockIntoTable(allItemsAndChildren);
    }

    public void loadAllItemsWithAllStocksIntoTable() {
        List<Item> allItemsAndChildren = new ArrayList<>();
        List<Item> topLevelItems = ItemDao.getTopLevelItems();
        for (Item item : topLevelItems) {
            allItemsAndChildren.addAll(returnAllItems(item));
        }
        loadItemsWithAllStocksIntoTable(allItemsAndChildren);
    }

    public void loadAllItemsWithAllStocksIntoSummaryTable() {
        List<Item> allItemsAndChildren = new ArrayList<>();
        List<Item> topLevelItems = ItemDao.getTopLevelItems();
        for (Item item : topLevelItems) {
            allItemsAndChildren.addAll(returnAllItems(item));
        }
        loadItemsWithAllStocksIntoSummaryTable(allItemsAndChildren);
    }

    private void loadItemsWithStockIntoTable(List<Item> itemsAndChildren) {

        double receivedTotal = 0;
        double issuedTotal = 0;
        double balancedTotal = 0;

        List<UniqueItemWithStock> itemsWithStock = new ArrayList<>();

        for (Item item : itemsAndChildren) {
            if (item.getCategoryItem() == CategoryItemDesignation.NO) {
                UniqueItemWithStock uniqueItemWithStock = new UniqueItemWithStock();
                uniqueItemWithStock.setItem(item);
                itemsWithStock.add(uniqueItemWithStock);
                if (item.getActiveStock() != null) {
                    receivedTotal += item.getActiveStock().getValueOfReceivedStock();
                    issuedTotal += item.getActiveStock().getValueOfIssuedStock();
                    balancedTotal += item.getActiveStock().getValueOfBalancedStock();
                }
            }
        }

        itemsWithStock.add(new DummyItemWithStock());
        itemsWithStock.add(new DummyItemWithStock(receivedTotal, issuedTotal, balancedTotal));

        stockTableView.getItems().setAll(itemsWithStock);
    }

    private void loadItemsWithAllStocksIntoTable(List<Item> itemsAndChildren) {

        double receivedTotalAllTab = 0;
        double issuedTotalAllTab = 0;
        double balancedTotalAllTab = 0;

        List<RepeatableItemWithStock> itemsWithStock = new ArrayList<>();

        for (Item item : itemsAndChildren) {
            if (item.getCategoryItem() == CategoryItemDesignation.NO) {
                for (Stock stock : item.getStocks()) {
                    RepeatableItemWithStock repeatableItemWithStock = new RepeatableItemWithStock();
                    repeatableItemWithStock.setStock(stock);
                    itemsWithStock.add(repeatableItemWithStock);
                    receivedTotalAllTab += stock.getValueOfReceivedStock();
                    issuedTotalAllTab += stock.getValueOfIssuedStock();
                    balancedTotalAllTab += stock.getValueOfBalancedStock();
                }
            }
        }

        itemsWithStock.add(new RepeatableDummyItemWithStock());
        itemsWithStock.add(new RepeatableDummyItemWithStock(receivedTotalAllTab, issuedTotalAllTab, balancedTotalAllTab));

        stockTableViewAllTab.getItems().setAll(itemsWithStock);
    }

    private void loadItemsWithAllStocksIntoSummaryTable(List<Item> itemsAndChildren) {

        double receivedTotalAllTab = 0;
        double issuedTotalAllTab = 0;
        double balancedTotalAllTab = 0;

        List<RepeatableItemWithStock> itemsWithStock = new ArrayList<>();

        for (Item item : itemsAndChildren) {
            if (item.getCategoryItem() == CategoryItemDesignation.NO) {
                for (Stock stock : item.getStocks()) {
                    RepeatableItemWithStock repeatableItemWithStock = new RepeatableItemWithStock();
                    repeatableItemWithStock.setStock(stock);
                    itemsWithStock.add(repeatableItemWithStock);
                    receivedTotalAllTab += stock.getValueOfReceivedStock();
                    issuedTotalAllTab += stock.getValueOfIssuedStock();
                    balancedTotalAllTab += stock.getValueOfBalancedStock();
                }
            }
        }

        itemsWithStock.add(new RepeatableDummyItemWithStock());
        itemsWithStock.add(new RepeatableDummyItemWithStock(receivedTotalAllTab, issuedTotalAllTab, balancedTotalAllTab));

        stockTableViewSummaryTab.getItems().setAll(itemsWithStock);
    }

    private void loadItemsWithAllStocksIntoNonValueTable(List<Item> itemsAndChildren) {

        List<RepeatableItemWithStock> itemsWithStock = new ArrayList<>();

        for (Item item : itemsAndChildren) {
            if (item.getCategoryItem() == CategoryItemDesignation.NO) {
                for (Stock stock : item.getStocks()) {
                    RepeatableItemWithStock repeatableItemWithStock = new RepeatableItemWithStock();
                    repeatableItemWithStock.setStock(stock);
                    itemsWithStock.add(repeatableItemWithStock);
                }
            }
        }

        stockTableViewNonValueTab.getItems().setAll(itemsWithStock);
    }

    private List<Item> returnAllItems(Item item) {
        List<Item> listOfItems = new ArrayList<Item>();
        addAllItems(item, listOfItems);
        return listOfItems;
    }

    private void addAllItems(Item item, List<Item> listOfItems) {
        if (item != null) {
            listOfItems.add(item);
            List<Item> children = item.getChildItems();
            if (children != null) {
                for (Item child : children) {
                    addAllItems(child, listOfItems);
                }
            }
        }
    }

    private void loadReportByCategoryTable() {
        List<Item> topLevelItems = ItemDao.getTopLevelItems();
        List<ReportByCategory> reportByCategoryList = new ArrayList<>();
        double total = 0;
        for (Item item : topLevelItems) {
            ReportByCategory reportByCategory = new ReportByCategory(item);
            if (reportByCategory.getAmount() != null) {
                reportByCategoryList.add(reportByCategory);
                total += reportByCategory.getAmountDouble();
            }
        }

        reportByCategoryList.add(new DummyReportByCategory());
        DummyReportByCategory dummyReportByCategory = new DummyReportByCategory("Total");
        dummyReportByCategory.setAmount(total);
        reportByCategoryList.add(dummyReportByCategory);

        stockTableViewCategoryReportTab.getItems().setAll(reportByCategoryList);
    }

    @ActionMethod("loadStock")
    public void loadStockController() {
        itemContainer.setItem(currentUniqueItemWithStock.getItem());
        try {
            handler.navigate(StockController.class);
        } catch (VetoException e) {
        } catch (FlowException e) {
        }
    }

    @ActionMethod("loadStockAllTab")
    public void loadRepeatableStockController() {
        itemContainer.setItem(currentRepeatableItemWithStock.getStock().getItem());
        try {
            handler.navigate(StockController.class);
        } catch (VetoException e) {
        } catch (FlowException e) {
        }
    }

    @ActionMethod("loadStockSummaryTab")
    public void loadSummaryStockController() {
        itemContainer.setItem(currentSummaryRepeatableItemWithStock.getStock().getItem());
        try {
            handler.navigate(StockController.class);
        } catch (VetoException e) {
        } catch (FlowException e) {
        }
    }

    public class DummyItemWithStock extends UniqueItemWithStock {

        private Double totalReceivedValue;
        private Double totalIssuedValue;
        private Double totalBalancedValue;

        public DummyItemWithStock() {
        }

        public DummyItemWithStock(double totalReceivedValue, double totalIssuedValue, double totalBalancedValue) {
            this.totalReceivedValue = totalReceivedValue;
            this.totalIssuedValue = totalIssuedValue;
            this.totalBalancedValue = totalBalancedValue;
        }

        @Override
        public void setItem(Item item) {
        }

        @Override
        public Item getItem() {
            return null;
        }

        @Override
        public String getName() {
            if (totalReceivedValue == null && totalIssuedValue == null && totalBalancedValue == null) {
                return null;
            }
            return "Total";
        }

        @Override
        public String getReceivedValue() {
            if (totalReceivedValue == null)
                return null;
            return totalReceivedValue.toString();
        }

        @Override
        public String getIssuedValue() {
            if (totalIssuedValue == null)
                return null;
            return totalIssuedValue.toString();
        }

        @Override
        public String getBalancedValue() {
            if (totalBalancedValue == null)
                return null;
            return totalBalancedValue.toString();
        }
    }

    public class RepeatableDummyItemWithStock extends RepeatableItemWithStock {

        private Double totalReceivedValue;
        private Double totalIssuedValue;
        private Double totalBalancedValue;

        public RepeatableDummyItemWithStock() {
        }

        public RepeatableDummyItemWithStock(double totalReceivedValue, double totalIssuedValue, double totalBalancedValue) {
            this.totalReceivedValue = totalReceivedValue;
            this.totalIssuedValue = totalIssuedValue;
            this.totalBalancedValue = totalBalancedValue;
        }

        @Override
        public void setStock(Stock stock) {
        }

        @Override
        public Stock getStock() {
            return null;
        }

        @Override
        public String getName() {
            if (totalReceivedValue == null && totalIssuedValue == null && totalBalancedValue == null) {
                return null;
            }
            return "Total";
        }

        @Override
        public String getReceivedValue() {
            if (totalReceivedValue == null)
                return null;
            return totalReceivedValue.toString();
        }

        @Override
        public String getIssuedValue() {
            if (totalIssuedValue == null)
                return null;
            return totalIssuedValue.toString();
        }

        @Override
        public String getBalancedValue() {
            if (totalBalancedValue == null)
                return null;
            return totalBalancedValue.toString();
        }
    }

    public class DummyReportByCategory extends ReportByCategory {

        private String title;

        private double amount;

        public DummyReportByCategory() {
        }

        public DummyReportByCategory(String title) {
            this.title = title;
        }

        public String getStockTitle() {
            return title;
        }

        public String getQuantity() {
            return null;
        }

        public String getRate() {
            return null;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }

        public String getAmount() {
            if (amount == 0)
                return null;
            else return amount + "";
        }
    }

    @ActionMethod("saveReport")
    public void saveReport() {
        HSSFWorkbook workbook = new HSSFWorkbook();
        createItemsWithRepeatableStocksSheet(workbook);
        createRepeatableStocksSheet(workbook);
        createItemsWithRepeatableStocksSummarySheet(workbook);
        createReportByCategorySheet(workbook);
        createItemsWithUniqueStocksSheet(workbook);

        final FileChooser imageFileChooser = new FileChooser();
        imageFileChooser.setTitle("Choose Location for General Reports");
        imageFileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );

        imageFileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Excel", "*.xls"));

        Stage chooserStage = Main.stage;
        File file = imageFileChooser.showSaveDialog(chooserStage);

        try {
            FileOutputStream out = new FileOutputStream(file);
            workbook.write(out);
            out.close();
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
    }

    private void createItemsWithUniqueStocksSheet(HSSFWorkbook workbook) {
        HSSFSheet sheet = workbook.createSheet("Items and Active Stocks");
        HSSFCellStyle normalStyle = workbook.createCellStyle();
        normalStyle.setAlignment(CellStyle.ALIGN_CENTER);
        sheet.setHorizontallyCenter(true);
        sheet.setVerticallyCenter(true);
        HSSFCellStyle boldStyle = workbook.createCellStyle();
        boldStyle.setAlignment(CellStyle.ALIGN_CENTER);
        HSSFFont font = workbook.createFont();
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        boldStyle.setFont(font);
        HSSFCellStyle cellStyle = boldStyle;
        createItemsAndStocksFullTitleRow(sheet, cellStyle);

        cellStyle = normalStyle;

        List<Item> itemsAndChildren = returnAllItems(itemContainer.getItem());

        if (allItemsCheckBox.selectedProperty().getValue()) {

            itemsAndChildren = new ArrayList<>();
            List<Item> topLevelItems = ItemDao.getTopLevelItems();
            for (Item item : topLevelItems) {
                itemsAndChildren.addAll(returnAllItems(item));
            }
        }

        double receivedTotal = 0;
        double issuedTotal = 0;
        double balancedTotal = 0;

        List<UniqueItemWithStock> itemsWithStock = new ArrayList<>();

        for (Item item : itemsAndChildren) {
            if (item.getCategoryItem() == CategoryItemDesignation.NO && item.getActiveStock() != null) {
                UniqueItemWithStock uniqueItemWithStock = new UniqueItemWithStock();
                uniqueItemWithStock.setItem(item);
                itemsWithStock.add(uniqueItemWithStock);
                if (item.getActiveStock() != null) {
                    receivedTotal += item.getActiveStock().getValueOfReceivedStock();
                    issuedTotal += item.getActiveStock().getValueOfIssuedStock();
                    balancedTotal += item.getActiveStock().getValueOfBalancedStock();
                }
            }
        }

        itemsWithStock.add(new DummyItemWithStock());
        itemsWithStock.add(new DummyItemWithStock(receivedTotal, issuedTotal, balancedTotal));

        for (int i = 0; i < itemsWithStock.size(); i++) {
            Row currentRow = sheet.createRow(i + 1);
            UniqueItemWithStock currentUniqueItemWithStock = itemsWithStock.get(i);
            if (!(currentUniqueItemWithStock instanceof DummyItemWithStock)) {
                org.apache.poi.ss.usermodel.Cell numberDataCell = currentRow.createCell(0);
                numberDataCell.setCellStyle(cellStyle);
                numberDataCell.setCellValue(i + 1);
            }

            if ("Total".equals(currentUniqueItemWithStock.getName())) {
                cellStyle = boldStyle;
            } else {
                cellStyle = normalStyle;
            }

            org.apache.poi.ss.usermodel.Cell itemNameDataCell = currentRow.createCell(1);
            itemNameDataCell.setCellStyle(cellStyle);
            itemNameDataCell.setCellValue(currentUniqueItemWithStock.getName());
            org.apache.poi.ss.usermodel.Cell priceDataCell = currentRow.createCell(2);
            priceDataCell.setCellStyle(cellStyle);
            priceDataCell.setCellValue(currentUniqueItemWithStock.getPrice());
            org.apache.poi.ss.usermodel.Cell receivedDateDataCell = currentRow.createCell(3);
            receivedDateDataCell.setCellStyle(cellStyle);
            receivedDateDataCell.setCellValue(currentUniqueItemWithStock.getReceivedDate());
            org.apache.poi.ss.usermodel.Cell receivedQuantityDataCell = currentRow.createCell(4);
            receivedQuantityDataCell.setCellStyle(cellStyle);
            receivedQuantityDataCell.setCellValue(currentUniqueItemWithStock.getReceivedQuantity());
            org.apache.poi.ss.usermodel.Cell receivedValueDataCell = currentRow.createCell(5);
            receivedValueDataCell.setCellStyle(cellStyle);
            receivedValueDataCell.setCellValue(currentUniqueItemWithStock.getReceivedValue());
            org.apache.poi.ss.usermodel.Cell issuedQuantityDataCell = currentRow.createCell(6);
            issuedQuantityDataCell.setCellStyle(cellStyle);
            issuedQuantityDataCell.setCellValue(currentUniqueItemWithStock.getIssuedQuantity());
            org.apache.poi.ss.usermodel.Cell issuedValueDataCell = currentRow.createCell(7);
            issuedValueDataCell.setCellStyle(cellStyle);
            issuedValueDataCell.setCellValue(currentUniqueItemWithStock.getIssuedValue());
            org.apache.poi.ss.usermodel.Cell balancedQuantityDataCell = currentRow.createCell(8);
            balancedQuantityDataCell.setCellStyle(cellStyle);
            balancedQuantityDataCell.setCellValue(currentUniqueItemWithStock.getBalancedQuantity());
            org.apache.poi.ss.usermodel.Cell balancedValueDataCell = currentRow.createCell(9);
            balancedValueDataCell.setCellStyle(cellStyle);
            balancedValueDataCell.setCellValue(currentUniqueItemWithStock.getBalancedValue());

        }
    }

    private void createRepeatableStocksSheet(HSSFWorkbook workbook) {
        HSSFSheet sheet = workbook.createSheet("Stocks");
        HSSFCellStyle normalStyle = workbook.createCellStyle();
        normalStyle.setAlignment(CellStyle.ALIGN_CENTER);
        sheet.setHorizontallyCenter(true);
        sheet.setVerticallyCenter(true);
        HSSFCellStyle boldStyle = workbook.createCellStyle();
        boldStyle.setAlignment(CellStyle.ALIGN_CENTER);
        HSSFFont font = workbook.createFont();
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        boldStyle.setFont(font);
        HSSFCellStyle cellStyle = boldStyle;

        createStocksTitleRow(sheet, cellStyle);

        cellStyle = normalStyle;

        List<Item> itemsAndChildren = returnAllItems(itemContainer.getItem());

        if (allItemsCheckBox.selectedProperty().getValue()) {
            itemsAndChildren = new ArrayList<>();
            List<Item> topLevelItems = ItemDao.getTopLevelItems();
            for (Item item : topLevelItems) {
                itemsAndChildren.addAll(returnAllItems(item));
            }
        }

        List<RepeatableItemWithStock> itemsWithStock = new ArrayList<>();

        for (Item item : itemsAndChildren) {
            if (item.getCategoryItem() == CategoryItemDesignation.NO) {
                for (Stock stock : item.getStocks()) {
                    RepeatableItemWithStock repeatableItemWithStock = new RepeatableItemWithStock();
                    repeatableItemWithStock.setStock(stock);
                    itemsWithStock.add(repeatableItemWithStock);
                }
            }
        }

        for (int i = 0; i < itemsWithStock.size(); i++) {
            Row currentRow = sheet.createRow(i + 1);
            RepeatableItemWithStock currentRepeatableItemWithStock = itemsWithStock.get(i);
            if (!(currentRepeatableItemWithStock instanceof RepeatableDummyItemWithStock)) {
                org.apache.poi.ss.usermodel.Cell numberDataCell = currentRow.createCell(0);
                numberDataCell.setCellStyle(cellStyle);
                numberDataCell.setCellValue(i + 1);
            }

            org.apache.poi.ss.usermodel.Cell itemNameDataCell = currentRow.createCell(1);
            itemNameDataCell.setCellStyle(cellStyle);
            itemNameDataCell.setCellValue(currentRepeatableItemWithStock.getName());
            org.apache.poi.ss.usermodel.Cell priceDataCell = currentRow.createCell(2);
            priceDataCell.setCellStyle(cellStyle);
            priceDataCell.setCellValue(currentRepeatableItemWithStock.getPrice());
            org.apache.poi.ss.usermodel.Cell receivedDateDataCell = currentRow.createCell(3);
            receivedDateDataCell.setCellStyle(cellStyle);
            receivedDateDataCell.setCellValue(currentRepeatableItemWithStock.getReceivedDate());
            org.apache.poi.ss.usermodel.Cell receivedQuantityDataCell = currentRow.createCell(4);
            receivedQuantityDataCell.setCellStyle(cellStyle);
            receivedQuantityDataCell.setCellValue(currentRepeatableItemWithStock.getReceivedQuantity());
            org.apache.poi.ss.usermodel.Cell issuedQuantityDataCell = currentRow.createCell(5);
            issuedQuantityDataCell.setCellStyle(cellStyle);
            issuedQuantityDataCell.setCellValue(currentRepeatableItemWithStock.getIssuedQuantity());
            org.apache.poi.ss.usermodel.Cell balancedQuantityDataCell = currentRow.createCell(6);
            balancedQuantityDataCell.setCellStyle(cellStyle);
            balancedQuantityDataCell.setCellValue(currentRepeatableItemWithStock.getBalancedQuantity());
        }
    }

    private void createItemsWithRepeatableStocksSheet(HSSFWorkbook workbook) {
        HSSFSheet sheet = workbook.createSheet("Items and Stocks");
        HSSFCellStyle normalStyle = workbook.createCellStyle();
        normalStyle.setAlignment(CellStyle.ALIGN_CENTER);
        sheet.setHorizontallyCenter(true);
        sheet.setVerticallyCenter(true);
        HSSFCellStyle boldStyle = workbook.createCellStyle();
        boldStyle.setAlignment(CellStyle.ALIGN_CENTER);
        HSSFFont font = workbook.createFont();
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        boldStyle.setFont(font);
        HSSFCellStyle cellStyle = boldStyle;

        createItemsAndStocksFullTitleRow(sheet, cellStyle);

        cellStyle = normalStyle;

        List<Item> itemsAndChildren = returnAllItems(itemContainer.getItem());

        if (allItemsCheckBox.selectedProperty().getValue()) {
            itemsAndChildren = new ArrayList<>();
            List<Item> topLevelItems = ItemDao.getTopLevelItems();
            for (Item item : topLevelItems) {
                itemsAndChildren.addAll(returnAllItems(item));
            }
        }

        double receivedTotal = 0;
        double issuedTotal = 0;
        double balancedTotal = 0;

        List<RepeatableItemWithStock> itemsWithStock = new ArrayList<>();

        for (Item item : itemsAndChildren) {
            if (item.getCategoryItem() == CategoryItemDesignation.NO) {
                for (Stock stock : item.getStocks()) {
                    RepeatableItemWithStock repeatableItemWithStock = new RepeatableItemWithStock();
                    repeatableItemWithStock.setStock(stock);
                    itemsWithStock.add(repeatableItemWithStock);
                    receivedTotal += stock.getValueOfReceivedStock();
                    issuedTotal += stock.getValueOfIssuedStock();
                    balancedTotal += stock.getValueOfBalancedStock();

                }
            }
        }

        itemsWithStock.add(new RepeatableDummyItemWithStock());
        itemsWithStock.add(new RepeatableDummyItemWithStock(receivedTotal, issuedTotal, balancedTotal));

        for (int i = 0; i < itemsWithStock.size(); i++) {
            Row currentRow = sheet.createRow(i + 1);
            RepeatableItemWithStock currentRepeatableItemWithStock = itemsWithStock.get(i);
            if (!(currentRepeatableItemWithStock instanceof RepeatableDummyItemWithStock)) {
                org.apache.poi.ss.usermodel.Cell numberDataCell = currentRow.createCell(0);
                numberDataCell.setCellStyle(cellStyle);
                numberDataCell.setCellValue(i + 1);
            }

            if ("Total".equals(currentRepeatableItemWithStock.getName())) {
                cellStyle = boldStyle;
            } else {
                cellStyle = normalStyle;
            }

            org.apache.poi.ss.usermodel.Cell itemNameDataCell = currentRow.createCell(1);
            itemNameDataCell.setCellStyle(cellStyle);
            itemNameDataCell.setCellValue(currentRepeatableItemWithStock.getName());
            org.apache.poi.ss.usermodel.Cell priceDataCell = currentRow.createCell(2);
            priceDataCell.setCellStyle(cellStyle);
            priceDataCell.setCellValue(currentRepeatableItemWithStock.getPrice());
            org.apache.poi.ss.usermodel.Cell receivedDateDataCell = currentRow.createCell(3);
            receivedDateDataCell.setCellStyle(cellStyle);
            receivedDateDataCell.setCellValue(currentRepeatableItemWithStock.getReceivedDate());
            org.apache.poi.ss.usermodel.Cell receivedQuantityDataCell = currentRow.createCell(4);
            receivedQuantityDataCell.setCellStyle(cellStyle);
            receivedQuantityDataCell.setCellValue(currentRepeatableItemWithStock.getReceivedQuantity());
            org.apache.poi.ss.usermodel.Cell receivedValueDataCell = currentRow.createCell(5);
            receivedValueDataCell.setCellStyle(cellStyle);
            receivedValueDataCell.setCellValue(currentRepeatableItemWithStock.getReceivedValue());
            org.apache.poi.ss.usermodel.Cell issuedQuantityDataCell = currentRow.createCell(6);
            issuedQuantityDataCell.setCellStyle(cellStyle);
            issuedQuantityDataCell.setCellValue(currentRepeatableItemWithStock.getIssuedQuantity());
            org.apache.poi.ss.usermodel.Cell issuedValueDataCell = currentRow.createCell(7);
            issuedValueDataCell.setCellStyle(cellStyle);
            issuedValueDataCell.setCellValue(currentRepeatableItemWithStock.getIssuedValue());
            org.apache.poi.ss.usermodel.Cell balancedQuantityDataCell = currentRow.createCell(8);
            balancedQuantityDataCell.setCellStyle(cellStyle);
            balancedQuantityDataCell.setCellValue(currentRepeatableItemWithStock.getBalancedQuantity());
            org.apache.poi.ss.usermodel.Cell balancedValueDataCell = currentRow.createCell(9);
            balancedValueDataCell.setCellStyle(cellStyle);
            balancedValueDataCell.setCellValue(currentRepeatableItemWithStock.getBalancedValue());
        }
    }

    private void createItemsWithRepeatableStocksSummarySheet(HSSFWorkbook workbook) {
        HSSFSheet sheet = workbook.createSheet("Items and Stocks Summary");

        HSSFCellStyle normalStyle = workbook.createCellStyle();
        normalStyle.setAlignment(CellStyle.ALIGN_CENTER);
        sheet.setHorizontallyCenter(true);
        sheet.setVerticallyCenter(true);
        HSSFCellStyle boldStyle = workbook.createCellStyle();
        boldStyle.setAlignment(CellStyle.ALIGN_CENTER);
        HSSFFont font = workbook.createFont();
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        boldStyle.setFont(font);
        HSSFCellStyle cellStyle = boldStyle;

        createItemsAndStocksSummaryTitleRow(sheet, cellStyle);

        cellStyle = normalStyle;

        List<Item> itemsAndChildren = returnAllItems(itemContainer.getItem());

        if (allItemsCheckBox.selectedProperty().getValue()) {
            itemsAndChildren = new ArrayList<>();
            List<Item> topLevelItems = ItemDao.getTopLevelItems();
            for (Item item : topLevelItems) {
                itemsAndChildren.addAll(returnAllItems(item));
            }
        }

        double receivedTotal = 0;
        double issuedTotal = 0;
        double balancedTotal = 0;

        List<RepeatableItemWithStock> itemsWithStock = new ArrayList<>();

        for (Item item : itemsAndChildren) {
            if (item.getCategoryItem() == CategoryItemDesignation.NO) {
                for (Stock stock : item.getStocks()) {
                    RepeatableItemWithStock repeatableItemWithStock = new RepeatableItemWithStock();
                    repeatableItemWithStock.setStock(stock);
                    itemsWithStock.add(repeatableItemWithStock);
                    receivedTotal += item.getActiveStock().getValueOfReceivedStock();
                    issuedTotal += item.getActiveStock().getValueOfIssuedStock();
                    balancedTotal += item.getActiveStock().getValueOfBalancedStock();
                }
            }
        }

        itemsWithStock.add(new RepeatableDummyItemWithStock());
        itemsWithStock.add(new RepeatableDummyItemWithStock(receivedTotal, issuedTotal, balancedTotal));

        for (int i = 0; i < itemsWithStock.size(); i++) {
            Row currentRow = sheet.createRow(i + 1);
            RepeatableItemWithStock currentRepeatableItemWithStock = itemsWithStock.get(i);
            if (!(currentRepeatableItemWithStock instanceof RepeatableDummyItemWithStock)) {
                org.apache.poi.ss.usermodel.Cell numberDataCell = currentRow.createCell(0);
                numberDataCell.setCellStyle(cellStyle);
                numberDataCell.setCellValue(i + 1);
            }

            if ("Total".equals(currentRepeatableItemWithStock.getName())) {
                cellStyle = boldStyle;
            } else {
                cellStyle = normalStyle;
            }

            org.apache.poi.ss.usermodel.Cell itemNameDataCell = currentRow.createCell(1);
            itemNameDataCell.setCellStyle(cellStyle);
            itemNameDataCell.setCellValue(currentRepeatableItemWithStock.getName());
            org.apache.poi.ss.usermodel.Cell priceDataCell = currentRow.createCell(2);
            priceDataCell.setCellStyle(cellStyle);
            priceDataCell.setCellValue(currentRepeatableItemWithStock.getPrice());
            org.apache.poi.ss.usermodel.Cell receivedDateDataCell = currentRow.createCell(3);
            receivedDateDataCell.setCellStyle(cellStyle);
            receivedDateDataCell.setCellValue(currentRepeatableItemWithStock.getReceivedDate());
            org.apache.poi.ss.usermodel.Cell receivedValueDataCell = currentRow.createCell(4);
            receivedValueDataCell.setCellStyle(cellStyle);
            receivedValueDataCell.setCellValue(currentRepeatableItemWithStock.getReceivedValue());
            org.apache.poi.ss.usermodel.Cell issuedValueDataCell = currentRow.createCell(5);
            issuedValueDataCell.setCellStyle(cellStyle);
            issuedValueDataCell.setCellValue(currentRepeatableItemWithStock.getIssuedValue());
            org.apache.poi.ss.usermodel.Cell balancedValueDataCell = currentRow.createCell(6);
            balancedValueDataCell.setCellStyle(cellStyle);
            balancedValueDataCell.setCellValue(currentRepeatableItemWithStock.getBalancedValue());

        }
    }

    private void createReportByCategorySheet(HSSFWorkbook workbook) {
        HSSFSheet sheet = workbook.createSheet("Report by Category");

        HSSFCellStyle normalStyle = workbook.createCellStyle();
        normalStyle.setAlignment(CellStyle.ALIGN_CENTER);
        sheet.setHorizontallyCenter(true);
        sheet.setVerticallyCenter(true);
        HSSFCellStyle boldStyle = workbook.createCellStyle();
        boldStyle.setAlignment(CellStyle.ALIGN_CENTER);
        HSSFFont font = workbook.createFont();
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        boldStyle.setFont(font);
        HSSFCellStyle cellStyle = boldStyle;

        createReportByCategoryTitleRow(sheet, cellStyle);

        cellStyle = normalStyle;

        List<Item> topLevelItems = ItemDao.getTopLevelItems();
        List<ReportByCategory> reportByCategoryList = new ArrayList<>();
        double total = 0;
        for (Item item : topLevelItems) {
            ReportByCategory reportByCategory = new ReportByCategory(item);
            if (reportByCategory.getAmount() != null) {
                reportByCategoryList.add(reportByCategory);
                total += reportByCategory.getAmountDouble();
            }
        }

        reportByCategoryList.add(new DummyReportByCategory());
        DummyReportByCategory dummyReportByCategory = new DummyReportByCategory("Total");
        dummyReportByCategory.setAmount(total);
        reportByCategoryList.add(dummyReportByCategory);

        for (int i = 0; i < reportByCategoryList.size(); i++) {
            Row currentRow = sheet.createRow(i + 1);
            ReportByCategory currentReportByCategory = reportByCategoryList.get(i);
            if (!(currentReportByCategory instanceof DummyReportByCategory)) {
                org.apache.poi.ss.usermodel.Cell numberDataCell = currentRow.createCell(0);
                numberDataCell.setCellStyle(cellStyle);
                numberDataCell.setCellValue(i + 1);
            }

            if ("Total".equals(currentReportByCategory.getStockTitle())) {
                cellStyle = boldStyle;
            } else {
                cellStyle = normalStyle;
            }

            org.apache.poi.ss.usermodel.Cell stockTitleDataCell = currentRow.createCell(1);
            stockTitleDataCell.setCellStyle(cellStyle);
            stockTitleDataCell.setCellValue(currentReportByCategory.getStockTitle());
            org.apache.poi.ss.usermodel.Cell quantityDataCell = currentRow.createCell(2);
            quantityDataCell.setCellStyle(cellStyle);
            quantityDataCell.setCellValue(currentReportByCategory.getQuantity());
            org.apache.poi.ss.usermodel.Cell rateDataCell = currentRow.createCell(3);
            rateDataCell.setCellStyle(cellStyle);
            rateDataCell.setCellValue(currentReportByCategory.getRate());
            org.apache.poi.ss.usermodel.Cell amountDataCell = currentRow.createCell(4);
            amountDataCell.setCellStyle(cellStyle);
            amountDataCell.setCellValue(currentReportByCategory.getAmount());
        }
    }

    private void createItemsAndStocksFullTitleRow(HSSFSheet sheet, CellStyle cellStyle) {
        Row titleRow = sheet.createRow(0);
        org.apache.poi.ss.usermodel.Cell numberCell = titleRow.createCell(0);
        numberCell.setCellStyle(cellStyle);
        numberCell.setCellValue("No");
        org.apache.poi.ss.usermodel.Cell itemNameCell = titleRow.createCell(1);
        itemNameCell.setCellStyle(cellStyle);
        itemNameCell.setCellValue("Item Name");
        org.apache.poi.ss.usermodel.Cell priceCell = titleRow.createCell(2);
        priceCell.setCellStyle(cellStyle);
        priceCell.setCellValue("Price");
        org.apache.poi.ss.usermodel.Cell receivedDateCell = titleRow.createCell(3);
        receivedDateCell.setCellStyle(cellStyle);
        receivedDateCell.setCellValue("Received Date");
        org.apache.poi.ss.usermodel.Cell receivedQuantityCell = titleRow.createCell(4);
        receivedQuantityCell.setCellStyle(cellStyle);
        receivedQuantityCell.setCellValue("Received Quantity");
        org.apache.poi.ss.usermodel.Cell receivedValueCell = titleRow.createCell(5);
        receivedValueCell.setCellStyle(cellStyle);
        receivedValueCell.setCellValue("Received Value");
        org.apache.poi.ss.usermodel.Cell issuedQuantityCell = titleRow.createCell(6);
        issuedQuantityCell.setCellStyle(cellStyle);
        issuedQuantityCell.setCellValue("Issued Quantity");
        org.apache.poi.ss.usermodel.Cell issuedValueCell = titleRow.createCell(7);
        issuedValueCell.setCellStyle(cellStyle);
        issuedValueCell.setCellValue("Issued Value");
        org.apache.poi.ss.usermodel.Cell balancedQuantityCell = titleRow.createCell(8);
        balancedQuantityCell.setCellStyle(cellStyle);
        balancedQuantityCell.setCellValue("Balanced Quantity");
        org.apache.poi.ss.usermodel.Cell balancedValueCell = titleRow.createCell(9);
        balancedValueCell.setCellStyle(cellStyle);
        balancedValueCell.setCellValue("Balanced Value");
    }

    private void createStocksTitleRow(HSSFSheet sheet, CellStyle cellStyle) {
        Row titleRow = sheet.createRow(0);
        org.apache.poi.ss.usermodel.Cell numberCell = titleRow.createCell(0);
        numberCell.setCellStyle(cellStyle);
        numberCell.setCellValue("No");
        org.apache.poi.ss.usermodel.Cell itemNameCell = titleRow.createCell(1);
        itemNameCell.setCellStyle(cellStyle);
        itemNameCell.setCellValue("Item Name");
        org.apache.poi.ss.usermodel.Cell priceCell = titleRow.createCell(2);
        priceCell.setCellStyle(cellStyle);
        priceCell.setCellValue("Price");
        org.apache.poi.ss.usermodel.Cell receivedDateCell = titleRow.createCell(3);
        receivedDateCell.setCellStyle(cellStyle);
        receivedDateCell.setCellValue("Received Date");
        org.apache.poi.ss.usermodel.Cell receivedQuantityCell = titleRow.createCell(4);
        receivedQuantityCell.setCellStyle(cellStyle);
        receivedQuantityCell.setCellValue("Received Quantity");
        org.apache.poi.ss.usermodel.Cell issuedQuantityCell = titleRow.createCell(5);
        issuedQuantityCell.setCellStyle(cellStyle);
        issuedQuantityCell.setCellValue("Issued Quantity");
        org.apache.poi.ss.usermodel.Cell balancedQuantityCell = titleRow.createCell(6);
        balancedQuantityCell.setCellStyle(cellStyle);
        balancedQuantityCell.setCellValue("Balanced Quantity");
    }

    private void createItemsAndStocksSummaryTitleRow(HSSFSheet sheet, CellStyle cellStyle) {
        Row titleRow = sheet.createRow(0);
        org.apache.poi.ss.usermodel.Cell numberCell = titleRow.createCell(0);
        numberCell.setCellStyle(cellStyle);
        numberCell.setCellValue("No");
        org.apache.poi.ss.usermodel.Cell itemNameCell = titleRow.createCell(1);
        itemNameCell.setCellStyle(cellStyle);
        itemNameCell.setCellValue("Item Name");
        org.apache.poi.ss.usermodel.Cell priceCell = titleRow.createCell(2);
        priceCell.setCellStyle(cellStyle);
        priceCell.setCellValue("Price");
        org.apache.poi.ss.usermodel.Cell receivedDateCell = titleRow.createCell(3);
        receivedDateCell.setCellStyle(cellStyle);
        receivedDateCell.setCellValue("Received Date");
        org.apache.poi.ss.usermodel.Cell receivedValueCell = titleRow.createCell(4);
        receivedValueCell.setCellStyle(cellStyle);
        receivedValueCell.setCellValue("Received Value");
        org.apache.poi.ss.usermodel.Cell issuedValueCell = titleRow.createCell(5);
        issuedValueCell.setCellStyle(cellStyle);
        issuedValueCell.setCellValue("Issued Value");
        org.apache.poi.ss.usermodel.Cell balancedValueCell = titleRow.createCell(6);
        balancedValueCell.setCellStyle(cellStyle);
        balancedValueCell.setCellValue("Balanced Value");
    }

    private void createReportByCategoryTitleRow(HSSFSheet sheet, CellStyle cellStyle) {
        Row titleRow = sheet.createRow(0);
        org.apache.poi.ss.usermodel.Cell numberCell = titleRow.createCell(0);
        numberCell.setCellStyle(cellStyle);
        numberCell.setCellValue("No");
        org.apache.poi.ss.usermodel.Cell stockTitleCell = titleRow.createCell(1);
        stockTitleCell.setCellStyle(cellStyle);
        stockTitleCell.setCellValue("Stock Title");
        org.apache.poi.ss.usermodel.Cell quantityCell = titleRow.createCell(2);
        quantityCell.setCellStyle(cellStyle);
        quantityCell.setCellValue("Quantity");
        org.apache.poi.ss.usermodel.Cell rateCell = titleRow.createCell(3);
        rateCell.setCellStyle(cellStyle);
        rateCell.setCellValue("Rate");
        org.apache.poi.ss.usermodel.Cell amountCell = titleRow.createCell(4);
        amountCell.setCellStyle(cellStyle);
        amountCell.setCellValue("Amount");
    }
}
