package org.bitbucket.infovillafoundation.spd.inventory.controllers;

import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.action.BackAction;
import io.datafx.controller.flow.context.ActionHandler;
import io.datafx.controller.flow.context.FlowActionHandler;
import io.datafx.controller.util.VetoException;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.bitbucket.infovillafoundation.spd.inventory.Main;
import org.bitbucket.infovillafoundation.spd.inventory.daos.DailyAmountReportDao;
import org.bitbucket.infovillafoundation.spd.inventory.daos.DailyQuantityCombinedReportDao;
import org.bitbucket.infovillafoundation.spd.inventory.daos.DailyQuantityReportDao;
import org.bitbucket.infovillafoundation.spd.inventory.entities.DailyAmountReport;
import org.bitbucket.infovillafoundation.spd.inventory.entities.DailyQuantityCombinedReport;
import org.bitbucket.infovillafoundation.spd.inventory.entities.DailyQuantityReport;
import org.bitbucket.infovillafoundation.spd.inventory.models.DailyAmountWithItem;
import org.bitbucket.infovillafoundation.spd.inventory.models.DailyQuantityCombinedWithItem;
import org.bitbucket.infovillafoundation.spd.inventory.models.DailyQuantityWithItem;
import org.bitbucket.infovillafoundation.spd.inventory.utils.Utilities;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 16/12/14.
 */

@FXMLController(value = "/fxml/report_by_date.fxml", title = "Report By Date")
public class ReportByDateController {

    @ActionHandler
    private FlowActionHandler handler;

    @FXML
    private VBox pane;

    @FXML
    @BackAction
    private Button backButton;

    @FXML
    @ActionTrigger("saveReport")
    private Button saveReportButton;

    @FXML
    private DatePicker dailyQuantityDatePicker;

    @FXML
    @ActionTrigger("loadDailyQuantitiesByDate")
    private Button dailyQuantitiesByDateButton;

    @FXML
    @ActionTrigger("loadAllDailyQuantities")
    private Button allDailyQuantitiesButton;

    @FXML
    private TableView<DailyQuantityWithItem> dailyQuantityTable;

    @FXML
    private DatePicker dailyCombinedQuantityDatePicker;

    @FXML
    @ActionTrigger("loadDailyCombinedQuantitiesByDate")
    private Button dailyCombinedQuantitiesByDateButton;

    @FXML
    @ActionTrigger("loadAllDailyCombinedQuantities")
    private Button allDailyCombinedQuantitiesButton;

    @FXML
    private TableView<DailyQuantityCombinedWithItem> dailyCombinedQuantityTable;

    @FXML
    private DatePicker dailyAmountDatePicker;

    @FXML
    @ActionTrigger("loadDailyAmountsByDate")
    private Button dailyAmountsByDateButton;

    @FXML
    @ActionTrigger("loadAllDailyAMounts")
    private Button allDailyAmountsButton;

    @FXML
    private TableView<DailyAmountWithItem> dailyAmountTable;

    @PostConstruct
    public void init() {
        setupBackShortcutListener();

        loadAllDailyQuantities();
        loadAllDailyCombinedQuantities();
        loadAllDailyAmounts();
    }

    private void setupBackShortcutListener() {
        final KeyCombination ctrlB = new KeyCodeCombination(KeyCode.B, KeyCombination.CONTROL_DOWN);
        pane.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (ctrlB.match(event)) {
                    try {
                        handler.navigateBack();
                    } catch (VetoException e) {
                    } catch (FlowException e) {
                    }
                }
            }
        });
    }

    private List<DailyQuantityWithItem> getDailyQuantitiesByDate(Date date) {
        List<DailyQuantityReport> dailyQuantityReports = DailyQuantityReportDao.getDailyQuantityReportsByDate(date);
        List<DailyQuantityWithItem> dailyQuantities = new ArrayList<>();
        for (DailyQuantityReport dailyQuantityReport : dailyQuantityReports) {
            DailyQuantityWithItem dailyQuantityWithItem = new DailyQuantityWithItem(dailyQuantityReport);
            dailyQuantities.add(dailyQuantityWithItem);
        }
        return dailyQuantities;
    }

    @ActionMethod("loadDailyQuantitiesByDate")
    public void loadDailyQuantitiesByDate() {
        Date date = Utilities.toUtilDateFromLocalDate(dailyQuantityDatePicker.getValue());
        List<DailyQuantityWithItem> dailyQuantities = getDailyQuantitiesByDate(date);
        loadDailyQuantitiesIntoTable(dailyQuantities);
    }

    private List<DailyQuantityWithItem> getAllDailyQuantities() {
        List<DailyQuantityReport> dailyQuantityReports = DailyQuantityReportDao.getAllDailyQuantityReports();
        List<DailyQuantityWithItem> dailyQuantities = new ArrayList<>();
        for (DailyQuantityReport dailyQuantityReport : dailyQuantityReports) {
            DailyQuantityWithItem dailyQuantityWithItem = new DailyQuantityWithItem(dailyQuantityReport);
            dailyQuantities.add(dailyQuantityWithItem);
        }
        return dailyQuantities;
    }

    @ActionMethod("loadAllDailyQuantities")
    public void loadAllDailyQuantities() {
        List<DailyQuantityWithItem> dailyQuantities = getAllDailyQuantities();
        loadDailyQuantitiesIntoTable(dailyQuantities);
        dailyQuantityDatePicker.setValue(null);
    }

    private void loadDailyQuantitiesIntoTable(List<DailyQuantityWithItem> dailyQuantities) {
        dailyQuantityTable.getItems().setAll(dailyQuantities);
    }

    private List<DailyQuantityCombinedWithItem> getDailyCombinedQuantitiesByDate(Date date) {
        List<DailyQuantityCombinedReport> dailyQuantityCombinedReports = DailyQuantityCombinedReportDao.getDailyQuantityCombinedReportsByDate(date);
        List<DailyQuantityCombinedWithItem> dailyCombinedQuantities = new ArrayList<>();
        for (DailyQuantityCombinedReport dailyQuantityCombinedReport : dailyQuantityCombinedReports) {
            DailyQuantityCombinedWithItem dailyQuantityCombinedWithItem = new DailyQuantityCombinedWithItem(dailyQuantityCombinedReport);
            dailyCombinedQuantities.add(dailyQuantityCombinedWithItem);
        }
        return dailyCombinedQuantities;
    }

    @ActionMethod("loadDailyCombinedQuantitiesByDate")
    public void loadDailyCombinedQuantitiesByDate() {
        Date date = Utilities.toUtilDateFromLocalDate(dailyCombinedQuantityDatePicker.getValue());
        List<DailyQuantityCombinedWithItem> dailyCombinedQuantities = getDailyCombinedQuantitiesByDate(date);
        loadDailyCombinedQuantitiesIntoTable(dailyCombinedQuantities);
    }

    private List<DailyQuantityCombinedWithItem> getAllDailyCombinedQuantities() {
        List<DailyQuantityCombinedReport> dailyQuantityCombinedReports = DailyQuantityCombinedReportDao.getAllDailyQuantityCombinedReports();
        List<DailyQuantityCombinedWithItem> dailyCombinedQuantities = new ArrayList<>();
        for (DailyQuantityCombinedReport dailyQuantityCombinedReport : dailyQuantityCombinedReports) {
            DailyQuantityCombinedWithItem dailyQuantityCombinedWithItem = new DailyQuantityCombinedWithItem(dailyQuantityCombinedReport);
            dailyCombinedQuantities.add(dailyQuantityCombinedWithItem);
        }
        return dailyCombinedQuantities;
    }

    @ActionMethod("loadAllDailyCombinedQuantities")
    public void loadAllDailyCombinedQuantities() {
        List<DailyQuantityCombinedWithItem> dailyCombinedQuantities = getAllDailyCombinedQuantities();
        loadDailyCombinedQuantitiesIntoTable(dailyCombinedQuantities);
        dailyCombinedQuantityDatePicker.setValue(null);
    }

    private List<DailyAmountWithItem> getDailyAmountsByDate(Date date) {
        List<DailyAmountReport> dailyAmountReports = DailyAmountReportDao.getDailyAmountReportsByDate(date);
        List<DailyAmountWithItem> dailyAmounts = new ArrayList<>();
        for (DailyAmountReport dailyAmountReport : dailyAmountReports) {
            DailyAmountWithItem dailyAmountWithItem = new DailyAmountWithItem(dailyAmountReport);
            dailyAmounts.add(dailyAmountWithItem);
        }
        return dailyAmounts;
    }

    @ActionMethod("loadDailyAmountsByDate")
    public void loadDailyAmountsByDate() {
        Date date = Utilities.toUtilDateFromLocalDate(dailyAmountDatePicker.getValue());
        List<DailyAmountWithItem> dailyAmounts = getDailyAmountsByDate(date);
        loadDailyAmountsIntoTable(dailyAmounts);
    }

    private void loadDailyAmountsIntoTable(List<DailyAmountWithItem> dailyAmounts) {
        dailyAmountTable.getItems().setAll(dailyAmounts);
    }

    private List<DailyAmountWithItem> getAllDailyAmounts() {
        List<DailyAmountReport> dailyAmountReports = DailyAmountReportDao.getAllDailyAmountReports();
        List<DailyAmountWithItem> dailyAmounts = new ArrayList<>();
        for (DailyAmountReport dailyAmountReport : dailyAmountReports) {
            DailyAmountWithItem dailyAmountWithItem = new DailyAmountWithItem(dailyAmountReport);
            dailyAmounts.add(dailyAmountWithItem);
        }
        return dailyAmounts;
    }

    @ActionMethod("loadAllDailyAmounts")
    public void loadAllDailyAmounts() {
        List<DailyAmountWithItem> dailyAmounts = getAllDailyAmounts();
        loadDailyAmountsIntoTable(dailyAmounts);
        dailyAmountDatePicker.setValue(null);
    }

    private void loadDailyCombinedQuantitiesIntoTable(List<DailyQuantityCombinedWithItem> dailyCombinedQuantities) {
        dailyCombinedQuantityTable.getItems().setAll(dailyCombinedQuantities);
    }

    @ActionMethod("saveReport")
    public void saveReport() {
        HSSFWorkbook workbook = new HSSFWorkbook();

        createDailyQuantitySheet(workbook);
        createDailyQuantityCombinedSheet(workbook);
        createDailyAmountSheet(workbook);

        final FileChooser imageFileChooser = new FileChooser();
        imageFileChooser.setTitle("Choose Location for Daily Reports");
        imageFileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );

        imageFileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Excel", "*.xls"));

        Stage chooserStage = Main.stage;
        File file = imageFileChooser.showOpenDialog(chooserStage);

        try {
            FileOutputStream out = new FileOutputStream(file);
            workbook.write(out);
            out.close();
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
    }

    private void createDailyQuantitySheet(HSSFWorkbook workbook) {
        HSSFSheet sheet = workbook.createSheet("Daily Quantities Per Stock");
        HSSFCellStyle normalStyle = workbook.createCellStyle();
        normalStyle.setAlignment(CellStyle.ALIGN_CENTER);
        sheet.setHorizontallyCenter(true);
        sheet.setVerticallyCenter(true);
        HSSFCellStyle boldStyle = workbook.createCellStyle();
        boldStyle.setAlignment(CellStyle.ALIGN_CENTER);
        HSSFFont font = workbook.createFont();
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        boldStyle.setFont(font);
        HSSFCellStyle cellStyle = boldStyle;
        createDailyQuantityPerStockTitleRow(sheet, cellStyle);

        cellStyle = normalStyle;

        List<DailyQuantityWithItem> dailyQuantities;

        if (dailyQuantityDatePicker.getValue() == null) {
            dailyQuantities = getAllDailyQuantities();
        } else {
            Date date = Utilities.toUtilDateFromLocalDate(dailyQuantityDatePicker.getValue());
            dailyQuantities = getDailyQuantitiesByDate(date);
        }

        for (int i = 0; i < dailyQuantities.size(); i++) {
            Row currentRow = sheet.createRow(i + 1);
            org.apache.poi.ss.usermodel.Cell numberDataCell = currentRow.createCell(0);
            numberDataCell.setCellStyle(cellStyle);
            numberDataCell.setCellValue(i + 1);

            DailyQuantityWithItem dailyQuantityWithItem = dailyQuantities.get(i);

            org.apache.poi.ss.usermodel.Cell itemNameDataCell = currentRow.createCell(1);
            itemNameDataCell.setCellStyle(cellStyle);
            itemNameDataCell.setCellValue(dailyQuantityWithItem.getItemName());
            org.apache.poi.ss.usermodel.Cell reportDateDataCell = currentRow.createCell(2);
            reportDateDataCell.setCellStyle(cellStyle);
            reportDateDataCell.setCellValue(dailyQuantityWithItem.getReportDate());
            org.apache.poi.ss.usermodel.Cell unitDataCell = currentRow.createCell(3);
            unitDataCell.setCellStyle(cellStyle);
            unitDataCell.setCellValue(dailyQuantityWithItem.getUnit());
            org.apache.poi.ss.usermodel.Cell openingBalanceDataCell = currentRow.createCell(4);
            openingBalanceDataCell.setCellStyle(cellStyle);
            openingBalanceDataCell.setCellValue(dailyQuantityWithItem.getOpeningBalance());
            org.apache.poi.ss.usermodel.Cell rateDataCell = currentRow.createCell(5);
            rateDataCell.setCellStyle(cellStyle);
            rateDataCell.setCellValue(dailyQuantityWithItem.getRate());
            org.apache.poi.ss.usermodel.Cell buyDataCell = currentRow.createCell(6);
            buyDataCell.setCellStyle(cellStyle);
            buyDataCell.setCellValue(dailyQuantityWithItem.getQuantityBought());
            org.apache.poi.ss.usermodel.Cell drawDataCell = currentRow.createCell(7);
            drawDataCell.setCellStyle(cellStyle);
            drawDataCell.setCellValue(dailyQuantityWithItem.getQuantityDrawn());
            org.apache.poi.ss.usermodel.Cell closingBalanceDataCell = currentRow.createCell(8);
            closingBalanceDataCell.setCellStyle(cellStyle);
            closingBalanceDataCell.setCellValue(dailyQuantityWithItem.getClosingBalance());
        }
    }

    private void createDailyQuantityPerStockTitleRow(HSSFSheet sheet, CellStyle cellStyle) {
        Row titleRow = sheet.createRow(0);
        org.apache.poi.ss.usermodel.Cell numberCell = titleRow.createCell(0);
        numberCell.setCellStyle(cellStyle);
        numberCell.setCellValue("No");
        org.apache.poi.ss.usermodel.Cell itemNameCell = titleRow.createCell(1);
        itemNameCell.setCellStyle(cellStyle);
        itemNameCell.setCellValue("Item Name");
        org.apache.poi.ss.usermodel.Cell reportDateCell = titleRow.createCell(2);
        reportDateCell.setCellStyle(cellStyle);
        reportDateCell.setCellValue("Report Date");
        org.apache.poi.ss.usermodel.Cell unitCell = titleRow.createCell(3);
        unitCell.setCellStyle(cellStyle);
        unitCell.setCellValue("Unit");
        org.apache.poi.ss.usermodel.Cell openingBalanceCell = titleRow.createCell(4);
        openingBalanceCell.setCellStyle(cellStyle);
        openingBalanceCell.setCellValue("Opening Balance");
        org.apache.poi.ss.usermodel.Cell rateCell = titleRow.createCell(5);
        rateCell.setCellStyle(cellStyle);
        rateCell.setCellValue("Rate");
        org.apache.poi.ss.usermodel.Cell buyCell = titleRow.createCell(6);
        buyCell.setCellStyle(cellStyle);
        buyCell.setCellValue("Buy");
        org.apache.poi.ss.usermodel.Cell drawCell = titleRow.createCell(7);
        drawCell.setCellStyle(cellStyle);
        drawCell.setCellValue("Draw");
        org.apache.poi.ss.usermodel.Cell closingBalanceCell = titleRow.createCell(8);
        closingBalanceCell.setCellStyle(cellStyle);
        closingBalanceCell.setCellValue("Closing Balance");
    }

    private void createDailyQuantityCombinedSheet(HSSFWorkbook workbook) {
        HSSFSheet sheet = workbook.createSheet("Daily Quantities Per Item");
        HSSFCellStyle normalStyle = workbook.createCellStyle();
        normalStyle.setAlignment(CellStyle.ALIGN_CENTER);
        sheet.setHorizontallyCenter(true);
        sheet.setVerticallyCenter(true);
        HSSFCellStyle boldStyle = workbook.createCellStyle();
        boldStyle.setAlignment(CellStyle.ALIGN_CENTER);
        HSSFFont font = workbook.createFont();
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        boldStyle.setFont(font);
        HSSFCellStyle cellStyle = boldStyle;
        createDailyQuantityPerItemTitleRow(sheet, cellStyle);

        cellStyle = normalStyle;

        List<DailyQuantityCombinedWithItem> dailyCombinedQuantities;

        if (dailyCombinedQuantityDatePicker.getValue() == null) {
            dailyCombinedQuantities = getAllDailyCombinedQuantities();
        } else {
            Date date = Utilities.toUtilDateFromLocalDate(dailyCombinedQuantityDatePicker.getValue());
            dailyCombinedQuantities = getDailyCombinedQuantitiesByDate(date);
        }

        for (int i = 0; i < dailyCombinedQuantities.size(); i++) {
            Row currentRow = sheet.createRow(i + 1);
            org.apache.poi.ss.usermodel.Cell numberDataCell = currentRow.createCell(0);
            numberDataCell.setCellStyle(cellStyle);
            numberDataCell.setCellValue(i + 1);

            DailyQuantityCombinedWithItem dailyQuantityCombinedWithItem = dailyCombinedQuantities.get(i);

            org.apache.poi.ss.usermodel.Cell itemNameDataCell = currentRow.createCell(1);
            itemNameDataCell.setCellStyle(cellStyle);
            itemNameDataCell.setCellValue(dailyQuantityCombinedWithItem.getItemName());
            org.apache.poi.ss.usermodel.Cell reportDateDataCell = currentRow.createCell(2);
            reportDateDataCell.setCellStyle(cellStyle);
            reportDateDataCell.setCellValue(dailyQuantityCombinedWithItem.getReportDate());
            org.apache.poi.ss.usermodel.Cell unitDataCell = currentRow.createCell(3);
            unitDataCell.setCellStyle(cellStyle);
            unitDataCell.setCellValue(dailyQuantityCombinedWithItem.getUnit());
            org.apache.poi.ss.usermodel.Cell openingBalanceDataCell = currentRow.createCell(4);
            openingBalanceDataCell.setCellStyle(cellStyle);
            openingBalanceDataCell.setCellValue(dailyQuantityCombinedWithItem.getOpeningBalance());
            org.apache.poi.ss.usermodel.Cell buyDataCell = currentRow.createCell(5);
            buyDataCell.setCellStyle(cellStyle);
            buyDataCell.setCellValue(dailyQuantityCombinedWithItem.getQuantityBought());
            org.apache.poi.ss.usermodel.Cell drawDataCell = currentRow.createCell(6);
            drawDataCell.setCellStyle(cellStyle);
            drawDataCell.setCellValue(dailyQuantityCombinedWithItem.getQuantityDrawn());
            org.apache.poi.ss.usermodel.Cell closingBalanceDataCell = currentRow.createCell(7);
            closingBalanceDataCell.setCellStyle(cellStyle);
            closingBalanceDataCell.setCellValue(dailyQuantityCombinedWithItem.getClosingBalance());
        }
    }

    private void createDailyQuantityPerItemTitleRow(HSSFSheet sheet, CellStyle cellStyle) {
        Row titleRow = sheet.createRow(0);
        org.apache.poi.ss.usermodel.Cell numberCell = titleRow.createCell(0);
        numberCell.setCellStyle(cellStyle);
        numberCell.setCellValue("No");
        org.apache.poi.ss.usermodel.Cell itemNameCell = titleRow.createCell(1);
        itemNameCell.setCellStyle(cellStyle);
        itemNameCell.setCellValue("Item Name");
        org.apache.poi.ss.usermodel.Cell reportDateCell = titleRow.createCell(2);
        reportDateCell.setCellStyle(cellStyle);
        reportDateCell.setCellValue("Report Date");
        org.apache.poi.ss.usermodel.Cell unitCell = titleRow.createCell(3);
        unitCell.setCellStyle(cellStyle);
        unitCell.setCellValue("Unit");
        org.apache.poi.ss.usermodel.Cell openingBalanceCell = titleRow.createCell(4);
        openingBalanceCell.setCellStyle(cellStyle);
        openingBalanceCell.setCellValue("Opening Balance");
        org.apache.poi.ss.usermodel.Cell buyCell = titleRow.createCell(5);
        buyCell.setCellStyle(cellStyle);
        buyCell.setCellValue("Buy");
        org.apache.poi.ss.usermodel.Cell drawCell = titleRow.createCell(6);
        drawCell.setCellStyle(cellStyle);
        drawCell.setCellValue("Draw");
        org.apache.poi.ss.usermodel.Cell closingBalanceCell = titleRow.createCell(7);
        closingBalanceCell.setCellStyle(cellStyle);
        closingBalanceCell.setCellValue("Closing Balance");
    }

    private void createDailyAmountSheet(HSSFWorkbook workbook) {
        HSSFSheet sheet = workbook.createSheet("Daily Amounts");
        HSSFCellStyle normalStyle = workbook.createCellStyle();
        normalStyle.setAlignment(CellStyle.ALIGN_CENTER);
        sheet.setHorizontallyCenter(true);
        sheet.setVerticallyCenter(true);
        HSSFCellStyle boldStyle = workbook.createCellStyle();
        boldStyle.setAlignment(CellStyle.ALIGN_CENTER);
        HSSFFont font = workbook.createFont();
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        boldStyle.setFont(font);
        HSSFCellStyle cellStyle = boldStyle;
        createDailyAmountTitleRow(sheet, cellStyle);

        cellStyle = normalStyle;

        List<DailyAmountWithItem> dailyAmounts;

        if (dailyAmountDatePicker.getValue() == null) {
            dailyAmounts = getAllDailyAmounts();
        } else {
            Date date = Utilities.toUtilDateFromLocalDate(dailyAmountDatePicker.getValue());
            dailyAmounts = getDailyAmountsByDate(date);
        }

        for (int i = 0; i < dailyAmounts.size(); i++) {
            Row currentRow = sheet.createRow(i + 1);
            org.apache.poi.ss.usermodel.Cell numberDataCell = currentRow.createCell(0);
            numberDataCell.setCellStyle(cellStyle);
            numberDataCell.setCellValue(i + 1);

            DailyAmountWithItem dailyAmountWithItem = dailyAmounts.get(i);

            org.apache.poi.ss.usermodel.Cell itemNameDataCell = currentRow.createCell(1);
            itemNameDataCell.setCellStyle(cellStyle);
            itemNameDataCell.setCellValue(dailyAmountWithItem.getItemName());
            org.apache.poi.ss.usermodel.Cell reportDateDataCell = currentRow.createCell(2);
            reportDateDataCell.setCellStyle(cellStyle);
            reportDateDataCell.setCellValue(dailyAmountWithItem.getReportDate());
            org.apache.poi.ss.usermodel.Cell openingBalanceDataCell = currentRow.createCell(3);
            openingBalanceDataCell.setCellStyle(cellStyle);
            openingBalanceDataCell.setCellValue(dailyAmountWithItem.getOpeningBalance());
            org.apache.poi.ss.usermodel.Cell buyDataCell = currentRow.createCell(4);
            buyDataCell.setCellStyle(cellStyle);
            buyDataCell.setCellValue(dailyAmountWithItem.getAmountBought());
            org.apache.poi.ss.usermodel.Cell drawDataCell = currentRow.createCell(5);
            drawDataCell.setCellStyle(cellStyle);
            drawDataCell.setCellValue(dailyAmountWithItem.getAmountDrawn());
            org.apache.poi.ss.usermodel.Cell closingBalanceDataCell = currentRow.createCell(6);
            closingBalanceDataCell.setCellStyle(cellStyle);
            closingBalanceDataCell.setCellValue(dailyAmountWithItem.getClosingBalance());
        }
    }

    private void createDailyAmountTitleRow(HSSFSheet sheet, CellStyle cellStyle) {
        Row titleRow = sheet.createRow(0);
        org.apache.poi.ss.usermodel.Cell numberCell = titleRow.createCell(0);
        numberCell.setCellStyle(cellStyle);
        numberCell.setCellValue("No");
        org.apache.poi.ss.usermodel.Cell itemNameCell = titleRow.createCell(1);
        itemNameCell.setCellStyle(cellStyle);
        itemNameCell.setCellValue("Item Name");
        org.apache.poi.ss.usermodel.Cell reportDateCell = titleRow.createCell(2);
        reportDateCell.setCellStyle(cellStyle);
        reportDateCell.setCellValue("Report Date");
        org.apache.poi.ss.usermodel.Cell openingBalanceCell = titleRow.createCell(3);
        openingBalanceCell.setCellStyle(cellStyle);
        openingBalanceCell.setCellValue("Opening Balance");
        org.apache.poi.ss.usermodel.Cell buyCell = titleRow.createCell(4);
        buyCell.setCellStyle(cellStyle);
        buyCell.setCellValue("Buy");
        org.apache.poi.ss.usermodel.Cell drawCell = titleRow.createCell(5);
        drawCell.setCellStyle(cellStyle);
        drawCell.setCellValue("Draw");
        org.apache.poi.ss.usermodel.Cell closingBalanceCell = titleRow.createCell(6);
        closingBalanceCell.setCellStyle(cellStyle);
        closingBalanceCell.setCellValue("Closing Balance");
    }
}
