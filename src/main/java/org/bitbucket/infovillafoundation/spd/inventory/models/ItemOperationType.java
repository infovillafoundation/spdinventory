package org.bitbucket.infovillafoundation.spd.inventory.models;

/**
 * Created by Sandah Aung on 13/1/15.
 */
public enum ItemOperationType {
    LEVEL, TOP, SUB, EDIT;
}
