package org.bitbucket.infovillafoundation.spd.inventory.daos;

import org.bitbucket.infovillafoundation.spd.inventory.entities.Item;
import org.bitbucket.infovillafoundation.spd.inventory.entities.MonthlyQuantityReport;
import org.bitbucket.infovillafoundation.spd.inventory.entities.Stock;
import org.bitbucket.infovillafoundation.spd.inventory.entities.StockReceipt;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 17/12/14.
 */
public class MonthlyQuantityReportDao extends Dao {

    public static List<MonthlyQuantityReport> getAllMonthlyQuantityReports() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<MonthlyQuantityReport> query = em.createNamedQuery("MonthlyQuantityReport.findAll", MonthlyQuantityReport.class);
        List<MonthlyQuantityReport> monthlyQuantityReports = query.getResultList();
        return monthlyQuantityReports;
    }

    public static List<MonthlyQuantityReport> getMonthlyQuantityReportsByDate(Date reportDate) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<MonthlyQuantityReport> query = em.createNamedQuery("MonthlyQuantityReport.findByReportDate", MonthlyQuantityReport.class);
        query.setParameter("reportDate", reportDate);
        List<MonthlyQuantityReport> monthlyQuantityReports = query.getResultList();
        return monthlyQuantityReports;
    }

    public static List<MonthlyQuantityReport> getMonthlyQuantityReportsByYearAndMonthInMonths(int yearAndMonthInMonths) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<MonthlyQuantityReport> query = em.createNamedQuery("MonthlyQuantityReport.findByYearAndMonthInMonths", MonthlyQuantityReport.class);
        query.setParameter("yearAndMonthInMonths", yearAndMonthInMonths);
        List<MonthlyQuantityReport> monthlyQuantityReports = query.getResultList();
        return monthlyQuantityReports;
    }

    public static MonthlyQuantityReport getMonthlyQuantityReportsByStockAndYearAndMonthInMonths(Stock stock, int yearAndMonthInMonths) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<MonthlyQuantityReport> query = em.createNamedQuery("MonthlyQuantityReport.findByStockAndYearAndMonthInMonths", MonthlyQuantityReport.class);
        query.setParameter("stock", stock);
        query.setParameter("yearAndMonthInMonths", yearAndMonthInMonths);
        MonthlyQuantityReport monthlyQuantityReport = null;
        try {
            monthlyQuantityReport = query.getSingleResult();
        } catch (NoResultException e) {

        }
        return monthlyQuantityReport;
    }

    public static MonthlyQuantityReport getLatestMonthlyQuantityReport() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<MonthlyQuantityReport> query = em.createNamedQuery("MonthlyQuantityReport.findByYearAndMonthInMonthsDesc", MonthlyQuantityReport.class);
        query.setMaxResults(1);
        MonthlyQuantityReport monthlyQuantityReport = null;
        try {
            monthlyQuantityReport = query.getSingleResult();
        } catch (NoResultException e) {

        }
        return monthlyQuantityReport;
    }

    public static List<Item> getItemsByNameLike(String searchTerm) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Item> query = em.createNamedQuery("Item.findByNameLike", Item.class);
        query.setParameter("name", "%" + searchTerm + "%");
        List<Item> items = query.getResultList();
        return items;
    }

    public static void saveMonthlyQuantityReport(MonthlyQuantityReport monthlyQuantityReport) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(monthlyQuantityReport);
        em.getTransaction().commit();
        em.close();
    }

    public static void saveItemWithStock(Item item, Stock stock) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stock);
        em.persist(item);
        em.getTransaction().commit();
        em.close();
    }

    public static void saveItemWithStockAndStockReceipt(Item item, Stock stock, StockReceipt stockReceipt) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stockReceipt);
        em.persist(stock);
        em.persist(item);
        em.getTransaction().commit();
        em.close();
    }

    public static void updateMonthlyQuantityReport(MonthlyQuantityReport monthlyQuantityReport) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(monthlyQuantityReport);
        em.getTransaction().commit();
        em.close();
    }

    public static void deleteItem(Item item) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.contains(item) ? item : em.merge(item));
        em.getTransaction().commit();
        em.close();
    }
}
