package org.bitbucket.infovillafoundation.spd.inventory.daos;

import org.bitbucket.infovillafoundation.spd.inventory.entities.Item;
import org.bitbucket.infovillafoundation.spd.inventory.entities.MonthlyQuantityCombinedReport;
import org.bitbucket.infovillafoundation.spd.inventory.entities.Stock;
import org.bitbucket.infovillafoundation.spd.inventory.entities.StockReceipt;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 17/12/14.
 */
public class MonthlyQuantityCombinedReportDao extends Dao {

    public static List<MonthlyQuantityCombinedReport> getAllMonthlyQuantityCombinedReports() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<MonthlyQuantityCombinedReport> query = em.createNamedQuery("MonthlyQuantityCombinedReport.findAll", MonthlyQuantityCombinedReport.class);
        List<MonthlyQuantityCombinedReport> monthlyQuantityCombinedReports = query.getResultList();
        return monthlyQuantityCombinedReports;
    }

    public static List<MonthlyQuantityCombinedReport> getMonthlyQuantityCombinedReportsByDate(Date reportDate) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<MonthlyQuantityCombinedReport> query = em.createNamedQuery("MonthlyQuantityCombinedReport.findByReportDate", MonthlyQuantityCombinedReport.class);
        query.setParameter("reportDate", reportDate);
        List<MonthlyQuantityCombinedReport> monthlyQuantityCombinedReports = query.getResultList();
        return monthlyQuantityCombinedReports;
    }

    public static MonthlyQuantityCombinedReport getMonthlyQuantityCombinedReportsByItemAndYearAndMonthInMonthsAndUnit(Item item, int yearAndMonthInMonths, String unit) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<MonthlyQuantityCombinedReport> query = em.createNamedQuery("MonthlyQuantityCombinedReport.findByItemAndYearAndMonthInMonthsAndUnit", MonthlyQuantityCombinedReport.class);
        query.setParameter("yearAndMonthInMonths", yearAndMonthInMonths);
        query.setParameter("item", item);
        query.setParameter("unit", unit);
        MonthlyQuantityCombinedReport monthlyQuantityCombinedReport = null;
        try {
            monthlyQuantityCombinedReport = query.getSingleResult();
        } catch (NoResultException e) {

        }
        return monthlyQuantityCombinedReport;
    }

    public static List<MonthlyQuantityCombinedReport> getMonthlyQuantityCombinedReportsByYearAndMonthInMonths(int yearAndMonthInMonths) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<MonthlyQuantityCombinedReport> query = em.createNamedQuery("MonthlyQuantityCombinedReport.findByYearAndMonthInMonths", MonthlyQuantityCombinedReport.class);
        query.setParameter("yearAndMonthInMonths", yearAndMonthInMonths);
        List<MonthlyQuantityCombinedReport> monthlyQuantityCombinedReports = query.getResultList();
        return monthlyQuantityCombinedReports;
    }

    public static List<Item> getItemsByNameLike(String searchTerm) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Item> query = em.createNamedQuery("Item.findByNameLike", Item.class);
        query.setParameter("name", "%" + searchTerm + "%");
        List<Item> items = query.getResultList();
        return items;
    }

    public static void saveMonthlyQuantityCombinedReport(MonthlyQuantityCombinedReport monthlyQuantityCombinedReport) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(monthlyQuantityCombinedReport);
        em.getTransaction().commit();
        em.close();
    }

    public static void saveItemWithStock(Item item, Stock stock) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stock);
        em.persist(item);
        em.getTransaction().commit();
        em.close();
    }

    public static void saveItemWithStockAndStockReceipt(Item item, Stock stock, StockReceipt stockReceipt) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(stockReceipt);
        em.persist(stock);
        em.persist(item);
        em.getTransaction().commit();
        em.close();
    }

    public static void updateMonthlyQuantityCombinedReport(MonthlyQuantityCombinedReport monthlyQuantityCombinedReport) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(monthlyQuantityCombinedReport);
        em.getTransaction().commit();
        em.close();
    }

    public static void deleteItem(Item item) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.contains(item) ? item : em.merge(item));
        em.getTransaction().commit();
        em.close();
    }
}
