package org.bitbucket.infovillafoundation.spd.inventory;

import io.datafx.controller.flow.Flow;
import javafx.application.Application;
import javafx.stage.Stage;
import org.bitbucket.infovillafoundation.spd.inventory.controllers.ItemController;
import org.bitbucket.infovillafoundation.spd.inventory.daos.*;
import org.bitbucket.infovillafoundation.spd.inventory.entities.*;
import org.bitbucket.infovillafoundation.spd.inventory.utils.Utilities;

import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 16/12/14.
 */
public class Main extends Application {

    public static Stage stage;

    private String dateRegex = "^((((0?[1-9]|[12]\\d|3[01])[\\.\\-\\/](0?[13578]|1[02])[\\.\\-\\/]((1[6-9]|[2-9]\\d)?\\d{2}))|((0?[1-9]|[12]\\d|30)[\\.\\-\\/](0?[13456789]|1[012])[\\.\\-\\/]((1[6-9]|[2-9]\\d)?\\d{2}))|((0?[1-9]|1\\d|2[0-8])[\\.\\-\\/]0?2[\\.\\-\\/]((1[6-9]|[2-9]\\d)?\\d{2}))|(29[\\.\\-\\/]0?2[\\.\\-\\/]((1[6-9]|[2-9]\\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00)))|(((0[1-9]|[12]\\d|3[01])(0[13578]|1[02])((1[6-9]|[2-9]\\d)?\\d{2}))|((0[1-9]|[12]\\d|30)(0[13456789]|1[012])((1[6-9]|[2-9]\\d)?\\d{2}))|((0[1-9]|1\\d|2[0-8])02((1[6-9]|[2-9]\\d)?\\d{2}))|(2902((1[6-9]|[2-9]\\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00))))$";

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        stage = primaryStage;

        new Flow(ItemController.class)
                .startInStage(primaryStage);

        Date today = new Date();

        if (DailyQuantityReportDao.getDailyQuantityReportsByDate(today).isEmpty()) {

            DailyQuantityReport latestDailyQuantityReport = DailyQuantityReportDao.getLatestDailyQuantityReport();
            Date lastDate = null;
            if (latestDailyQuantityReport != null) {
                lastDate = latestDailyQuantityReport.getReportDate();

                List<DailyQuantityReport> dailyQuantityReports = DailyQuantityReportDao.getDailyQuantityReportsByDate(lastDate);
                for (DailyQuantityReport dailyQuantityReport : dailyQuantityReports) {
                    DailyQuantityReport newDailyQuantityReport = new DailyQuantityReport();
                    newDailyQuantityReport.setOpeningBalance(dailyQuantityReport.getClosingBalance());
                    newDailyQuantityReport.setUnit(dailyQuantityReport.getUnit());
                    newDailyQuantityReport.setReportDate(today);
                    newDailyQuantityReport.setRate(dailyQuantityReport.getRate());
                    newDailyQuantityReport.setStock(dailyQuantityReport.getStock());
                    newDailyQuantityReport.setClosingBalance(newDailyQuantityReport.getOpeningBalance() + newDailyQuantityReport.getQuantityBought() - newDailyQuantityReport.getQuantityDrawn());

                    DailyQuantityReportDao.saveDailyQuantityReport(newDailyQuantityReport);
                }

                List<DailyQuantityCombinedReport> dailyQuantityCombinedReports = DailyQuantityCombinedReportDao.getDailyQuantityCombinedReportsByDate(lastDate);
                for (DailyQuantityCombinedReport dailyQuantityCombinedReport : dailyQuantityCombinedReports) {
                    DailyQuantityCombinedReport newDailyQuantityCombinedReport = new DailyQuantityCombinedReport();
                    newDailyQuantityCombinedReport.setOpeningBalance(dailyQuantityCombinedReport.getClosingBalance());
                    newDailyQuantityCombinedReport.setUnit(dailyQuantityCombinedReport.getUnit());
                    newDailyQuantityCombinedReport.setReportDate(today);
                    newDailyQuantityCombinedReport.setItem(dailyQuantityCombinedReport.getItem());
                    newDailyQuantityCombinedReport.setClosingBalance(newDailyQuantityCombinedReport.getOpeningBalance() + newDailyQuantityCombinedReport.getQuantityBought() - newDailyQuantityCombinedReport.getQuantityDrawn());

                    DailyQuantityCombinedReportDao.saveDailyQuantityCombinedReport(newDailyQuantityCombinedReport);
                }

                List<DailyAmountReport> dailyAmountReports = DailyAmountReportDao.getDailyAmountReportsByDate(lastDate);
                for (DailyAmountReport dailyAmountReport : dailyAmountReports) {
                    DailyAmountReport newDailyAmountReport = new DailyAmountReport();
                    newDailyAmountReport.setOpeningBalance(dailyAmountReport.getClosingBalance());
                    newDailyAmountReport.setReportDate(today);
                    newDailyAmountReport.setItem(dailyAmountReport.getItem());
                    newDailyAmountReport.setClosingBalance(newDailyAmountReport.getOpeningBalance() + newDailyAmountReport.getAmountBought() - newDailyAmountReport.getAmountDrawn());

                    DailyAmountReportDao.saveDailyAmountReport(newDailyAmountReport);
                }
            }

            int yearMonth = Utilities.toYearMonthFromDate(today);

            if (MonthlyQuantityReportDao.getMonthlyQuantityReportsByYearAndMonthInMonths(yearMonth).isEmpty()) {

                MonthlyQuantityReport latestMonthlyQuantityReport = MonthlyQuantityReportDao.getLatestMonthlyQuantityReport();
                if (latestMonthlyQuantityReport != null) {
                    int lastYearMonth = latestMonthlyQuantityReport.getYearAndMonthInMonths();

                    List<MonthlyQuantityReport> monthlyQuantityReports = MonthlyQuantityReportDao.getMonthlyQuantityReportsByYearAndMonthInMonths(lastYearMonth);
                    for (MonthlyQuantityReport monthlyQuantityReport : monthlyQuantityReports) {
                        MonthlyQuantityReport newMonthlyQuantityReport = new MonthlyQuantityReport();
                        newMonthlyQuantityReport.setOpeningBalance(monthlyQuantityReport.getClosingBalance());
                        newMonthlyQuantityReport.setYearAndMonthInMonths(yearMonth);
                        newMonthlyQuantityReport.setUnit(monthlyQuantityReport.getUnit());
                        newMonthlyQuantityReport.setReportDate(today);
                        newMonthlyQuantityReport.setRate(monthlyQuantityReport.getRate());
                        newMonthlyQuantityReport.setStock(monthlyQuantityReport.getStock());
                        newMonthlyQuantityReport.setClosingBalance(newMonthlyQuantityReport.getOpeningBalance() + newMonthlyQuantityReport.getQuantityBought() - newMonthlyQuantityReport.getQuantityDrawn());

                        MonthlyQuantityReportDao.saveMonthlyQuantityReport(newMonthlyQuantityReport);
                    }

                    List<MonthlyQuantityCombinedReport> monthlyQuantityCombinedReports = MonthlyQuantityCombinedReportDao.getMonthlyQuantityCombinedReportsByYearAndMonthInMonths(lastYearMonth);
                    for (MonthlyQuantityCombinedReport monthlyQuantityCombinedReport : monthlyQuantityCombinedReports) {
                        MonthlyQuantityCombinedReport newMonthlyQuantityCombinedReport = new MonthlyQuantityCombinedReport();
                        newMonthlyQuantityCombinedReport.setYearAndMonthInMonths(yearMonth);
                        newMonthlyQuantityCombinedReport.setOpeningBalance(monthlyQuantityCombinedReport.getClosingBalance());
                        newMonthlyQuantityCombinedReport.setUnit(monthlyQuantityCombinedReport.getUnit());
                        newMonthlyQuantityCombinedReport.setReportDate(today);
                        newMonthlyQuantityCombinedReport.setItem(monthlyQuantityCombinedReport.getItem());
                        newMonthlyQuantityCombinedReport.setClosingBalance(newMonthlyQuantityCombinedReport.getOpeningBalance() + newMonthlyQuantityCombinedReport.getQuantityBought() - newMonthlyQuantityCombinedReport.getQuantityDrawn());

                        MonthlyQuantityCombinedReportDao.saveMonthlyQuantityCombinedReport(newMonthlyQuantityCombinedReport);
                    }

                    List<MonthlyAmountReport> monthlyAmountReports = MonthlyAmountReportDao.getMonthlyAmountReportsByYearAndMonthInMonths(lastYearMonth);
                    for (MonthlyAmountReport monthlyAmountReport : monthlyAmountReports) {
                        MonthlyAmountReport newMonthlyAmountReport = new MonthlyAmountReport();
                        newMonthlyAmountReport.setYearAndMonthInMonths(yearMonth);
                        newMonthlyAmountReport.setOpeningBalance(monthlyAmountReport.getClosingBalance());
                        newMonthlyAmountReport.setReportDate(today);
                        newMonthlyAmountReport.setItem(monthlyAmountReport.getItem());
                        newMonthlyAmountReport.setClosingBalance(newMonthlyAmountReport.getOpeningBalance() + newMonthlyAmountReport.getAmountBought() - newMonthlyAmountReport.getAmountDrawn());

                        MonthlyAmountReportDao.saveMonthlyAmountReport(newMonthlyAmountReport);
                    }
                }
            }
        }
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        Dao.emf.close();
    }
}
