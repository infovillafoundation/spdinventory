package org.bitbucket.infovillafoundation.spd.inventory.controllers;

import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.context.ActionHandler;
import io.datafx.controller.flow.context.FlowActionHandler;
import io.datafx.controller.util.VetoException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.bitbucket.infovillafoundation.spd.inventory.daos.*;
import org.bitbucket.infovillafoundation.spd.inventory.entities.*;
import org.bitbucket.infovillafoundation.spd.inventory.models.*;
import org.bitbucket.infovillafoundation.spd.inventory.utils.Utilities;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created by Sandah Aung on 16/12/14.
 */

@FXMLController(value = "/fxml/items.fxml", title = "Items and Stocks")
public class ItemController {

    @ActionHandler
    private FlowActionHandler handler;

    @Inject
    private ItemContainer itemContainer;

    @FXML
    private GridPane addNewItemGrid;

    @FXML
    private GridPane showItemGrid;

    @FXML
    private GridPane confirmDeleteItemGrid;

    @FXML
    private TableView<UniqueItemWithStock> itemWithStockTableView;

    @FXML
    private TreeView<Item> itemTreeView;

    @FXML
    private TextField searchName;

    @FXML
    private TableView<Item> searchResultTableView;

    @FXML
    @ActionTrigger("loadItemDetails")
    private MenuItem detailsMenuItem;

    @FXML
    @ActionTrigger("loadDetailedReport")
    private MenuItem detailedReportMenuItem;

    @FXML
    private Text addNewItemText;

    @FXML
    private TextField nameInput;

    @FXML
    private TextField associatedUnitInput;

    @FXML
    private TextArea descriptionInput;

    @FXML
    private CheckBox categoryItemCheckBox;

    @FXML
    private HBox withoutStockBar;

    @FXML
    private CheckBox withoutStockCheckBox;

    @FXML
    private TextField quantityInput;

    @FXML
    private TextField requiredQuantityInput;

    @FXML
    private TextField unitInput;

    @FXML
    private TextField priceInput;

    @FXML
    private TextField remarkInput;

    @FXML
    private CheckBox isOpeningStockCheckBox;

    @FXML
    private Separator initialStockSeparator;

    @FXML
    private Text initialStockText;

    @FXML
    private Label receivedDateLabel;

    @FXML
    private DatePicker receivedDateDatePicker;

    @FXML
    private Label initialStockQuantityLabel;

    @FXML
    private Label initialStockUnitLabel;

    @FXML
    private Label initialStockPriceLabel;

    @FXML
    private Label initialStockRequiredQuantityLabel;

    @FXML
    private Label remarkLabel;

    @FXML
    private Label isOpeningStockLabel;

    @FXML
    private HBox saveEditButtonBar;

    @FXML
    private Text itemNameShowText;

    @FXML
    private Text associatedUnitShowText;

    @FXML
    private Text descriptionShowText;

    @FXML
    private Text isCategoryShowText;

    @FXML
    private Text receivedDateShowText;

    @FXML
    private Text quantityShowText;

    @FXML
    private Text requiredQuantityShowText;

    @FXML
    private Text priceShowText;

    private TreeItem<Item> passedItem;

    @FXML
    @ActionTrigger("loadNewTopItemForm")
    private Button createNewTopItemButton;

    @FXML
    @ActionTrigger("loadNewItemForm")
    private Button createNewItemButton;

    @FXML
    @ActionTrigger("loadNewSubItemForm")
    private MenuItem createSubItemMenuItem;

    @FXML
    @ActionTrigger("loadEditItemForm")
    private Button editItemButton;

    @FXML
    @ActionTrigger("loadDeleteItemForm")
    private Button deleteItemButton;

    @FXML
    private Label parentItemLabel;

    @FXML
    private Text parentItemText;

    @FXML
    @ActionTrigger("updateItem")
    private Button saveButton;

    @FXML
    @ActionTrigger("createTopItem")
    private Button createTopButton;

    @FXML
    @ActionTrigger("createLevelItem")
    private Button createButton;

    @FXML
    @ActionTrigger("createSubItem")
    private Button createSubButton;

    @FXML
    @ActionTrigger("confirmDeleteItem")
    private Button confirmDeleteItemButton;

    @FXML
    @ActionTrigger("cancelDeleteItem")
    private Button cancelDeleteItemButton;

    @FXML
    @ActionTrigger("clear")
    private Button clearButton;

    private TreeItem<Item> currentTreeItem;

    private boolean isCategoryItem;

    private boolean withoutStock;

    private ItemOperationType itemOperationType;

    private Map<Item, TreeItem<Item>> itemToTreeItemMap;

    private boolean searchMode;

    @PostConstruct
    public void init() {
        itemToTreeItemMap = new HashMap<>();
        loadItemTreeView();
        prepareNewItem();
        setupItemTableDoubleClickListener();
        setupDateConverter();
        setupCategoryItemListener();
        setupWithoutStockListener();
        setupFormShortcutListeners();
        setupTreeShortcutListeners();
        setupTableShortcutListeners();
        setupSearchTableDoubleClickListener();
        setupUnitFillListener();
        setupValidators();
        setupSearchListeners();
    }

    private void setupFormShortcutListeners() {
        final KeyCombination ctrlP = new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN);
        final KeyCombination ctrlM = new KeyCodeCombination(KeyCode.M, KeyCombination.CONTROL_DOWN);
        final KeyCombination ctrlJ = new KeyCodeCombination(KeyCode.J, KeyCombination.CONTROL_DOWN);
        final KeyCombination ctrlK = new KeyCodeCombination(KeyCode.K, KeyCombination.CONTROL_DOWN);
        final KeyCombination ctrlEnter = new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN);
        addNewItemGrid.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (ctrlP.match(event)) {
                    categoryItemCheckBox.setSelected(true);
                } else if (ctrlM.match(event)) {
                    categoryItemCheckBox.setSelected(false);
                }
                if (ctrlJ.match(event)) {
                    withoutStockCheckBox.setSelected(true);
                } else if (ctrlK.match(event)) {
                    withoutStockCheckBox.setSelected(false);
                } else if (ctrlEnter.match(event)) {
                    switch (itemOperationType) {
                        case LEVEL:
                            createLevelItem();
                            break;
                        case TOP:
                            createTopItem();
                            break;
                        case SUB:
                            createSubItem();
                            break;
                        case EDIT:
                            updateItem();
                            break;
                        default:
                            break;
                    }
                }
            }
        });
    }

    private void setupTreeShortcutListeners() {
        final KeyCombination ctrlN = new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN);
        final KeyCombination ctrlT = new KeyCodeCombination(KeyCode.T, KeyCombination.CONTROL_DOWN);
        final KeyCombination ctrlS = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN);
        final KeyCombination ctrlE = new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN);
        final KeyCombination ctrlL = new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN);
        final KeyCombination ctrlD = new KeyCodeCombination(KeyCode.D, KeyCombination.CONTROL_DOWN);
        final KeyCombination ctrlR = new KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN);
        itemTreeView.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (ctrlN.match(event)) {
                    clearFields();
                    if (itemTreeView.getRoot().getValue().getId() == -1L)
                        loadNewTopItemForm();
                    else
                        loadNewItemForm();
                    nameInput.requestFocus();
                } else if (ctrlT.match(event)) {
                    clearFields();
                    if (itemTreeView.getRoot().getValue().getId() == -1L)
                        loadNewTopItemForm();
                    else
                        loadNewTopItemForm();
                    nameInput.requestFocus();
                } else if (ctrlS.match(event)) {
                    clearFields();
                    if (itemTreeView.getRoot().getValue().getId() == -1L)
                        loadNewTopItemForm();
                    else
                        loadNewSubItemForm();
                    nameInput.requestFocus();
                } else if (ctrlE.match(event)) {
                    if (currentTreeItem.getChildren() != null && !currentTreeItem.getChildren().isEmpty()) {
                        currentTreeItem.setExpanded(!currentTreeItem.isExpanded());
                    }
                } else if (ctrlL.match(event)) {
                    loadStockController(currentTreeItem.getValue());
                } else if (ctrlR.match(event)) {
                    loadDetailedReportController();
                } else if (ctrlD.match(event)) {
                    Item itemToBeDeleted = itemWithStockTableView.getSelectionModel().getSelectedItem().getItem();
                    if (isDeletable(itemToBeDeleted)) {
                        itemTreeView.getSelectionModel().select(itemToTreeItemMap.get(itemToBeDeleted));
                        loadDeleteItemGrid();
                        cancelDeleteItemButton.requestFocus();
                    }
                }
            }
        });
    }

    private void setupUnitFillListener() {
        unitInput.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue && unitInput.getText().isEmpty()) {
                    unitInput.setText(associatedUnitInput.getText());
                }
            }
        });

        requiredQuantityInput.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue && unitInput.getText().isEmpty()) {
                    requiredQuantityInput.setText("0");
                }
            }
        });
    }

    private void setupSearchListeners() {
        searchName.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue.isEmpty()) {
                    searchMode = false;
                    searchResultTableView.setVisible(false);
                    itemTreeView.setVisible(true);
                    if (currentTreeItem == null || currentTreeItem.getValue().getId() == -1L)
                        loadNewItemForm();
                    else
                        loadItemDetails();
                } else {
                    searchMode = true;
                    List<Item> searchedItems = ItemDao.getItemsByNameLike(newValue);
                    searchResultTableView.setVisible(true);
                    itemTreeView.setVisible(false);
                    searchResultTableView.getItems().setAll(searchedItems);
                }
            }
        });
    }

    private void setupTableShortcutListeners() {
        final KeyCombination ctrlL = new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN);
        final KeyCombination ctrlD = new KeyCodeCombination(KeyCode.D, KeyCombination.CONTROL_DOWN);
        itemWithStockTableView.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (ctrlL.match(event)) {
                    loadStockController(itemWithStockTableView.getSelectionModel().getSelectedItem().getItem());
                } else if (ctrlD.match(event)) {
                    itemTreeView.getSelectionModel().select(itemToTreeItemMap.get(itemWithStockTableView.getSelectionModel().getSelectedItem().getItem()));
                    loadDeleteItemGrid();
                    cancelDeleteItemButton.requestFocus();
                }
            }
        });
    }

    private void setupValidators() {
        nameInput.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (nameInput.getText().isEmpty())
                    nameInput.setStyle("-fx-border-color: red");
                else
                    nameInput.setStyle("-fx-border-color: null");
            }
        });

        receivedDateDatePicker.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (receivedDateDatePicker.getValue() == null) {
                    receivedDateDatePicker.setStyle("-fx-border-color: red");
                } else {
                    receivedDateDatePicker.setStyle("-fx-border-color: null");
                }
            }
        });

        quantityInput.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (quantityInput.getText().isEmpty())
                    quantityInput.setStyle("-fx-border-color: red");
                else
                    quantityInput.setStyle("-fx-border-color: null");
            }
        });

        requiredQuantityInput.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (requiredQuantityInput.getText().isEmpty())
                    requiredQuantityInput.setStyle("-fx-border-color: red");
                else
                    requiredQuantityInput.setStyle("-fx-border-color: null");
            }
        });

        unitInput.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (unitInput.getText().isEmpty())
                    unitInput.setStyle("-fx-border-color: red");
                else
                    unitInput.setStyle("-fx-border-color: null");
            }
        });

        priceInput.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (priceInput.getText().isEmpty())
                    priceInput.setStyle("-fx-border-color: red");
                else
                    priceInput.setStyle("-fx-border-color: null");
            }
        });
    }

    private void loadItemTreeView() {
        Item dummyItem = new Item();
        dummyItem.setName("Items");
        dummyItem.setId(-1L);
        TreeItem<Item> rootItem = new TreeItem<>();
        rootItem.setValue(dummyItem);
        List<Item> topLevelItems = ItemDao.getTopLevelItems();
        for (Item item : topLevelItems) {
            rootItem.getChildren().add(getTreeItemHierarchyOfItems(item));
        }
        itemTreeView.setRoot(rootItem);
        currentTreeItem = rootItem;
        setupTreeCellFactory();
        setupTreeItemClickListener();
        setupTreeItemDoubleClickListener();
        if (itemContainer.getItem() != null)
            itemTreeView.getSelectionModel().select(passedItem);
    }

    private TreeItem<Item> getTreeItemHierarchyOfItems(Item item) {
        TreeItem<Item> itemTreeItem = new TreeItem<>(item);
        itemToTreeItemMap.put(item, itemTreeItem);
        if (itemContainer.getItem() != null && item.getId() == itemContainer.getItem().getId())
            passedItem = itemTreeItem;
        if (item.getChildItems() != null && !item.getChildItems().isEmpty()) {
            for (Item i : item.getChildItems()) {
                itemTreeItem.getChildren().add(getTreeItemHierarchyOfItems(i));
            }
        }
        return itemTreeItem;
    }

    private void setupTreeCellFactory() {
        itemTreeView.setCellFactory(new Callback<TreeView<Item>, TreeCell<Item>>() {
            @Override
            public TreeCell<Item> call(TreeView<Item> p) {
                return new TreeCell<Item>() {
                    @Override
                    protected void updateItem(Item item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            setText(item.getName());
                            if (item.getCategoryItem() == CategoryItemDesignation.YES) {
                                ImageView imageView = new ImageView(new Image("/img/rectangle.png"));
                                imageView.setFitHeight(20);
                                imageView.setFitWidth(20);
                                setGraphic(imageView);
                            } else if (item.isStockLow()) {
                                ImageView imageView = new ImageView(new Image("/img/circle.png"));
                                imageView.setFitHeight(20);
                                imageView.setFitWidth(20);
                                setGraphic(imageView);
                            } else {
                                ImageView imageView = new ImageView(new Image("/img/tick.png"));
                                imageView.setFitHeight(20);
                                imageView.setFitWidth(20);
                                setGraphic(imageView);
                            }
                            if (item.getCategoryItem() == CategoryItemDesignation.NO && item.getActiveStock() == null)
                                setStyle("-fx-text-fill: red");
                            else
                                setStyle("-fx-text-fill: default");

                        } else {
                            setText(null);
                            setGraphic(null);
                            setStyle("-fx-text-fill: null");
                        }
                    }
                };
            }
        });
    }

    private void setupTreeItemClickListener() {
        itemTreeView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TreeItem<Item>>() {

            @Override
            public void changed(ObservableValue<? extends TreeItem<Item>> observable, TreeItem<Item> oldValue, TreeItem<Item> newValue) {
                currentTreeItem = newValue;
                if (currentTreeItem != null) {
                    if (currentTreeItem.getValue() == null) {
                        loadNewItemForm();
                        editItemButton.setVisible(false);
                    } else {
                        loadItemDetails();
                        editItemButton.setVisible(true);
                        enableDisableDeleteButton(currentTreeItem.getValue());
                    }
                }
            }
        });
    }

    private void enableDisableDeleteButton(Item item) {
        if (isDeletable(item)) {
            deleteItemButton.setVisible(true);
        } else {
            deleteItemButton.setVisible(false);
        }
    }

    private void setupTreeItemDoubleClickListener() {
        itemTreeView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getClickCount() == 2) {
                    TreeItem<Item> item = itemTreeView.getSelectionModel().getSelectedItem();
                    loadStockController(item.getValue());
                }
            }
        });
    }

    private void setupItemTableDoubleClickListener() {
        itemWithStockTableView.setRowFactory(tv -> {
            TableRow<UniqueItemWithStock> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    UniqueItemWithStock rowData = row.getItem();
                    loadStockController(rowData.getItem());
                }
            });
            return row;
        });
    }

    private void setupSearchTableDoubleClickListener() {
        searchResultTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Item selectedItem = searchResultTableView.getSelectionModel().getSelectedItem();
                loadItemDetails(selectedItem);
            }
        });
    }

    private void setupCategoryItemListener() {
        categoryItemCheckBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                isCategoryItem = newValue;
                withoutStockBar.setVisible(!newValue);
                prepareEnableDisableInitialStock(!newValue && !withoutStock);
                if (newValue)
                    clearInitialStockFormWarnings();
            }
        });
    }

    private void setupWithoutStockListener() {
        withoutStockCheckBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                withoutStock = newValue;
                prepareEnableDisableInitialStock(!newValue);
                if (newValue)
                    clearInitialStockFormWarnings();
            }
        });
    }

    private void setupDateConverter() {
        String pattern = "dd/M/yyyy";
        receivedDateDatePicker.setPromptText(pattern.toLowerCase());
        receivedDateDatePicker.setConverter(new StringConverter<LocalDate>() {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateTimeFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateTimeFormatter);

                } else {
                    return null;
                }
            }
        });
    }

    private void loadStockController(Item item) {
        itemContainer.setItem(item);
        if (item.getCategoryItem() == CategoryItemDesignation.NO) {
            try {
                handler.navigate(StockController.class);
            } catch (VetoException e) {
            } catch (FlowException e) {
            }
        }
    }

    @ActionMethod("createTopItem")
    public void createTopItem() {
        createItem(itemTreeView.getRoot());
        itemOperationType = null;
    }

    @ActionMethod("createLevelItem")
    public void createLevelItem() {
        if (currentTreeItem.getValue().getId() == -1L)
            createItem(currentTreeItem);
        else
            createItem(currentTreeItem.getParent());
        itemOperationType = null;
    }

    @ActionMethod("createSubItem")
    public void createSubItem() {
        createItem(currentTreeItem);
        itemOperationType = null;
    }

    @ActionMethod("updateItem")
    public void updateItem() {
        if (nameInput.getText().isEmpty()) {
            nameInput.setStyle("-fx-border-color: red");
        } else {
            Item itemToUpdate = currentTreeItem.getValue();
            loadFormDataIntoItem(itemToUpdate);
            ItemDao.updateItem(itemToUpdate);
            itemTreeView.getSelectionModel().select(currentTreeItem);
            itemOperationType = null;
        }
    }

    @ActionMethod("clear")
    public void clear() {
        clearFields();
    }

    private void createItem(TreeItem<Item> parentTreeItem) {
        if (validateBeforeFormSubmission()) {
            Item itemToCreate = new Item();
            Item currentItem;
            if (parentTreeItem != null) {
                currentItem = parentTreeItem.getValue();
            } else {
                currentItem = currentTreeItem.getValue();
            }
            if (currentItem.getId() == -1L)
                currentItem = null;
            itemToCreate.setParentItem(currentItem);
            if (currentItem != null) {
                if (currentItem.getChildItems() == null)
                    currentItem.setChildItems(new ArrayList<>());
                currentItem.getChildItems().add(itemToCreate);
            }
            loadFormDataIntoItem(itemToCreate);
            if (isCategoryItem || withoutStock) {
                ItemDao.saveItem(itemToCreate);
            } else {
                boolean isOpeningStock = isOpeningStockCheckBox.selectedProperty().getValue();
                Stock initialStock = new Stock();
                loadInitialFormDataIntoStock(initialStock);
                initialStock.setItem(itemToCreate);
                Date receivedDateUtilDate = Date.from(receivedDateDatePicker.getValue().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
                initialStock.setReceivedDate(receivedDateUtilDate);
                List<Stock> stocks = new ArrayList<>();
                stocks.add(initialStock);
                itemToCreate.setActiveStock(initialStock);
                initialStock.setActiveStockOwner(itemToCreate);
                itemToCreate.setStocks(stocks);
                StockReceipt stockReceipt = new StockReceipt();
                if (isOpeningStock) {
                    stockReceipt.setStockReceiptStatus(StockReceiptStatus.OPENING);
                    stockReceipt.setParticular("Opening Balance");
                } else {
                    stockReceipt.setParticular("Initial Stock");
                }
                stockReceipt.setReceivedDate(receivedDateUtilDate);
                stockReceipt.setReceivedQuantity(initialStock.getQuantityOfReceivedStock());
                initialStock.getStockReceipts().add(stockReceipt);
                ItemDao.saveItemWithStockAndStockReceipt(itemToCreate, initialStock, stockReceipt);

                if (isOpeningStock) {
                    DailyQuantityReport dailyQuantityReport = new DailyQuantityReport();
                    dailyQuantityReport.setReportDate(initialStock.getReceivedDate());
                    dailyQuantityReport.setOpeningBalance(initialStock.getQuantityOfReceivedStock());
                    dailyQuantityReport.setUnit(initialStock.getUnit());
                    dailyQuantityReport.setRate(initialStock.getPriceOfReceivedStock());
                    dailyQuantityReport.setClosingBalance(dailyQuantityReport.getOpeningBalance() + dailyQuantityReport.getQuantityBought() - dailyQuantityReport.getQuantityDrawn());
                    dailyQuantityReport.setStock(initialStock);
                    DailyQuantityReportDao.saveDailyQuantityReport(dailyQuantityReport);

                    DailyAmountReport dailyAmountReport = new DailyAmountReport();
                    dailyAmountReport.setReportDate(initialStock.getReceivedDate());
                    dailyAmountReport.setOpeningBalance(dailyQuantityReport.getOpeningBalance() * dailyQuantityReport.getRate());
                    dailyAmountReport.setClosingBalance(dailyAmountReport.getOpeningBalance() + dailyAmountReport.getAmountBought() - dailyAmountReport.getAmountDrawn());
                    dailyAmountReport.setItem(itemToCreate);
                    DailyAmountReportDao.saveDailyAmountReport(dailyAmountReport);

                    DailyQuantityCombinedReport dailyQuantityCombinedReport = new DailyQuantityCombinedReport();
                    dailyQuantityCombinedReport.setReportDate(initialStock.getReceivedDate());
                    dailyQuantityCombinedReport.setOpeningBalance(initialStock.getQuantityOfReceivedStock());
                    dailyQuantityCombinedReport.setClosingBalance(dailyQuantityCombinedReport.getOpeningBalance() + dailyQuantityCombinedReport.getQuantityBought() - dailyQuantityCombinedReport.getQuantityDrawn());
                    dailyQuantityCombinedReport.setUnit(initialStock.getUnit());
                    dailyQuantityCombinedReport.getSourceDailyQuantityReports().add(dailyQuantityReport);
                    dailyQuantityCombinedReport.setItem(itemToCreate);
                    DailyQuantityCombinedReportDao.saveDailyQuantityCombinedReport(dailyQuantityCombinedReport);

                    int yearMonth = Utilities.toYearMonthFromDate(initialStock.getReceivedDate());

                    MonthlyQuantityReport monthlyQuantityReport = new MonthlyQuantityReport();
                    monthlyQuantityReport.setReportDate(initialStock.getReceivedDate());
                    monthlyQuantityReport.setOpeningBalance(initialStock.getQuantityOfReceivedStock());
                    monthlyQuantityReport.setClosingBalance(monthlyQuantityReport.getOpeningBalance() + monthlyQuantityReport.getQuantityBought() - monthlyQuantityReport.getQuantityDrawn());
                    monthlyQuantityReport.setRate(initialStock.getPriceOfReceivedStock());
                    monthlyQuantityReport.setUnit(initialStock.getUnit());
                    monthlyQuantityReport.setStock(initialStock);
                    monthlyQuantityReport.setYearAndMonthInMonths(yearMonth);
                    MonthlyQuantityReportDao.saveMonthlyQuantityReport(monthlyQuantityReport);

                    MonthlyAmountReport monthlyAmountReport = new MonthlyAmountReport();
                    monthlyAmountReport.setReportDate(initialStock.getReceivedDate());
                    monthlyAmountReport.setOpeningBalance(monthlyQuantityReport.getOpeningBalance() * monthlyQuantityReport.getRate());
                    monthlyAmountReport.setClosingBalance(monthlyAmountReport.getOpeningBalance() + monthlyAmountReport.getAmountBought() - monthlyAmountReport.getAmountDrawn());
                    monthlyAmountReport.setItem(itemToCreate);
                    monthlyAmountReport.setYearAndMonthInMonths(yearMonth);
                    MonthlyAmountReportDao.saveMonthlyAmountReport(monthlyAmountReport);

                    MonthlyQuantityCombinedReport monthlyQuantityCombinedReport = new MonthlyQuantityCombinedReport();
                    monthlyQuantityCombinedReport.setReportDate(initialStock.getReceivedDate());
                    monthlyQuantityCombinedReport.setOpeningBalance(initialStock.getQuantityOfReceivedStock());
                    monthlyQuantityCombinedReport.setUnit(initialStock.getUnit());
                    monthlyQuantityCombinedReport.setYearAndMonthInMonths(yearMonth);
                    monthlyQuantityCombinedReport.setClosingBalance(monthlyQuantityCombinedReport.getOpeningBalance() + monthlyQuantityCombinedReport.getQuantityBought() - monthlyQuantityCombinedReport.getQuantityDrawn());
                    monthlyQuantityCombinedReport.getSourceMonthlyQuantityReports().add(monthlyQuantityReport);
                    monthlyQuantityCombinedReport.setItem(itemToCreate);
                    MonthlyQuantityCombinedReportDao.saveMonthlyQuantityCombinedReport(monthlyQuantityCombinedReport);
                } else {
                    DailyQuantityReport dailyQuantityReport = new DailyQuantityReport();
                    dailyQuantityReport.setReportDate(initialStock.getReceivedDate());
                    dailyQuantityReport.setQuantityBought(initialStock.getQuantityOfReceivedStock());
                    dailyQuantityReport.setUnit(initialStock.getUnit());
                    dailyQuantityReport.setRate(initialStock.getPriceOfReceivedStock());
                    dailyQuantityReport.setClosingBalance(dailyQuantityReport.getOpeningBalance() + dailyQuantityReport.getQuantityBought() - dailyQuantityReport.getQuantityDrawn());
                    dailyQuantityReport.setStock(initialStock);
                    DailyQuantityReportDao.saveDailyQuantityReport(dailyQuantityReport);

                    DailyAmountReport dailyAmountReport = new DailyAmountReport();
                    dailyAmountReport.setReportDate(initialStock.getReceivedDate());
                    dailyAmountReport.setAmountBought(dailyQuantityReport.getQuantityBought() * dailyQuantityReport.getRate());
                    dailyAmountReport.setClosingBalance(dailyAmountReport.getOpeningBalance() + dailyAmountReport.getAmountBought() - dailyAmountReport.getAmountDrawn());
                    dailyAmountReport.setItem(itemToCreate);
                    DailyAmountReportDao.saveDailyAmountReport(dailyAmountReport);

                    DailyQuantityCombinedReport dailyQuantityCombinedReport = new DailyQuantityCombinedReport();
                    dailyQuantityCombinedReport.setReportDate(initialStock.getReceivedDate());
                    dailyQuantityCombinedReport.setQuantityBought(initialStock.getQuantityOfReceivedStock());
                    dailyQuantityCombinedReport.setUnit(initialStock.getUnit());
                    dailyQuantityCombinedReport.setClosingBalance(dailyQuantityCombinedReport.getOpeningBalance() + dailyQuantityCombinedReport.getQuantityBought() - dailyQuantityCombinedReport.getQuantityDrawn());
                    dailyQuantityCombinedReport.getSourceDailyQuantityReports().add(dailyQuantityReport);
                    dailyQuantityCombinedReport.setItem(itemToCreate);
                    DailyQuantityCombinedReportDao.saveDailyQuantityCombinedReport(dailyQuantityCombinedReport);

                    int yearMonth = Utilities.toYearMonthFromDate(initialStock.getReceivedDate());

                    MonthlyQuantityReport monthlyQuantityReport = new MonthlyQuantityReport();
                    monthlyQuantityReport.setReportDate(initialStock.getReceivedDate());
                    monthlyQuantityReport.setQuantityBought(initialStock.getQuantityOfReceivedStock());
                    monthlyQuantityReport.setClosingBalance(monthlyQuantityReport.getOpeningBalance() + monthlyQuantityReport.getQuantityBought() - monthlyQuantityReport.getQuantityDrawn());
                    monthlyQuantityReport.setRate(initialStock.getPriceOfReceivedStock());
                    monthlyQuantityReport.setUnit(initialStock.getUnit());
                    monthlyQuantityReport.setYearAndMonthInMonths(yearMonth);
                    monthlyQuantityReport.setStock(initialStock);
                    MonthlyQuantityReportDao.saveMonthlyQuantityReport(monthlyQuantityReport);

                    MonthlyAmountReport monthlyAmountReport = new MonthlyAmountReport();
                    monthlyAmountReport.setReportDate(initialStock.getReceivedDate());
                    monthlyAmountReport.setAmountBought(monthlyQuantityReport.getQuantityBought() * monthlyQuantityReport.getRate());
                    monthlyAmountReport.setClosingBalance(monthlyAmountReport.getOpeningBalance() + monthlyAmountReport.getAmountBought() - monthlyAmountReport.getAmountDrawn());
                    monthlyAmountReport.setItem(itemToCreate);
                    monthlyAmountReport.setYearAndMonthInMonths(yearMonth);
                    MonthlyAmountReportDao.saveMonthlyAmountReport(monthlyAmountReport);

                    MonthlyQuantityCombinedReport monthlyQuantityCombinedReport = new MonthlyQuantityCombinedReport();
                    monthlyQuantityCombinedReport.setReportDate(initialStock.getReceivedDate());
                    monthlyQuantityCombinedReport.setQuantityBought(initialStock.getQuantityOfReceivedStock());
                    monthlyQuantityCombinedReport.setUnit(initialStock.getUnit());
                    monthlyQuantityCombinedReport.setClosingBalance(monthlyQuantityCombinedReport.getOpeningBalance() + monthlyQuantityCombinedReport.getQuantityBought() - monthlyQuantityCombinedReport.getQuantityDrawn());
                    monthlyQuantityCombinedReport.getSourceMonthlyQuantityReports().add(monthlyQuantityReport);
                    monthlyQuantityCombinedReport.setYearAndMonthInMonths(yearMonth);
                    monthlyQuantityCombinedReport.setItem(itemToCreate);
                    MonthlyQuantityCombinedReportDao.saveMonthlyQuantityCombinedReport(monthlyQuantityCombinedReport);
                }
            }
            TreeItem<Item> item = new TreeItem<>();
            item.setValue(itemToCreate);
            itemToTreeItemMap.put(itemToCreate, item);
            if (parentTreeItem != null)
                parentTreeItem.getChildren().add(item);
            else
                currentTreeItem.getChildren().add(item);
            clearFields();
            itemTreeView.getSelectionModel().select(item);
        }
    }

    @ActionMethod("loadNewItemForm")
    public void loadNewItemForm() {
        if (currentTreeItem.getValue().getId() == -1L) {
            parentItemLabel.setVisible(false);
            parentItemText.setVisible(false);
        } else {
            parentItemLabel.setVisible(true);
            parentItemText.setVisible(true);
            if (currentTreeItem.getValue().getParentItem() != null)
                parentItemText.setText(currentTreeItem.getValue().getParentItem().getName());
        }
        prepareNewItem();
        loadNewItemGrid();
    }

    @ActionMethod("loadNewTopItemForm")
    public void loadNewTopItemForm() {
        parentItemLabel.setVisible(false);
        parentItemText.setVisible(false);
        prepareNewTopItem();
        loadNewItemGrid();
    }

    @ActionMethod("loadEditItemForm")
    public void loadEditItemForm() {
        prepareEditItem();
        if (currentTreeItem.getValue().getId() == -1L) {
            parentItemLabel.setVisible(false);
            parentItemText.setVisible(false);
        } else {
            parentItemLabel.setVisible(true);
            parentItemText.setVisible(true);
            if (currentTreeItem.getValue().getParentItem() != null)
                parentItemText.setText(currentTreeItem.getValue().getParentItem().getName());
        }
        loadNewItemGrid();
        loadItemDataIntoForm(currentTreeItem.getValue());
    }

    @ActionMethod("loadNewSubItemForm")
    public void loadNewSubItemForm() {
        parentItemLabel.setVisible(true);
        parentItemText.setVisible(true);
        prepareNewSubItem();
        parentItemText.setText(currentTreeItem.getValue().getName());
        loadNewItemGrid();
    }

    @ActionMethod("loadDeleteItemForm")
    public void loadDeleteItemForm() {
        loadDeleteItemGrid();
    }

    @ActionMethod("loadItemDetails")
    public void loadItemDetails() {
        loadItemDetailsGrid();
        loadItemsWithStockIntoTable();
        loadItemWithInitialStockIntoShowGrid();
    }

    @ActionMethod("loadDetailedReport")
    public void loadDetailedReportController() {
        itemContainer.setItem(currentTreeItem.getValue());
        try {
            handler.navigate(DetailedReportController.class);
        } catch (VetoException e) {
        } catch (FlowException e) {
        }
    }

    public void loadItemDetails(Item item) {
        loadItemDetailsGrid();
        loadItemsWithStockIntoTable(item);
    }

    private void loadFormDataIntoItem(Item item) {
        item.setName(nameInput.getText());
        item.setAssociatedUnit(associatedUnitInput.getText());
        item.setDescription(descriptionInput.getText());
        item.setCategoryItem(isCategoryItem ? CategoryItemDesignation.YES : CategoryItemDesignation.NO);
    }

    private void loadInitialFormDataIntoStock(Stock stock) {
        if (quantityInput.getText() != null && !quantityInput.getText().equals("")) {
            stock.setQuantityOfReceivedStock(Double.parseDouble(quantityInput.getText()));
            stock.setQuantityOfBalancedStock(Double.parseDouble(quantityInput.getText()));
        }
        if (priceInput.getText() != null && !priceInput.getText().equals(""))
            stock.setPriceOfReceivedStock(Double.parseDouble(priceInput.getText()));
        stock.setValueOfReceivedStock(stock.getPriceOfReceivedStock() * stock.getQuantityOfReceivedStock());
        stock.setValueOfBalancedStock(stock.getPriceOfReceivedStock() * stock.getQuantityOfBalancedStock());
        if (requiredQuantityInput.getText() != null && !requiredQuantityInput.getText().equals(""))
            stock.setRequiredStockLevel(Double.parseDouble(requiredQuantityInput.getText()));
        stock.setUnit(unitInput.getText());
        stock.setUnit(unitInput.getText());
        stock.setRemark(remarkInput.getText());
    }

    private void loadItemDataIntoForm(Item item) {
        nameInput.setText(item.getName());
        associatedUnitInput.setText(item.getAssociatedUnit());
        descriptionInput.setText(item.getDescription());
        if (item.getCategoryItem() == CategoryItemDesignation.YES)
            categoryItemCheckBox.setSelected(true);
        else
            categoryItemCheckBox.setSelected(false);
    }

    private void clearFields() {
        receivedDateDatePicker.setDisable(false);
        nameInput.clear();
        associatedUnitInput.clear();
        descriptionInput.clear();
        categoryItemCheckBox.setSelected(false);
        withoutStockCheckBox.setSelected(false);
        receivedDateDatePicker.setValue(null);
        quantityInput.clear();
        requiredQuantityInput.clear();
        unitInput.clear();
        priceInput.clear();
        remarkInput.clear();
        isOpeningStockCheckBox.setSelected(false);
        receivedDateDatePicker.setValue(Utilities.toLocalDateFromUtilDate(new Date()));
        clearFormWarnings();
    }

    private boolean validateBeforeFormSubmission() {
        boolean isErrorFree = true;
        if (nameInput.getText().isEmpty()) {
            nameInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }
        if (!isCategoryItem && !withoutStock && receivedDateDatePicker.getValue() == null) {
            receivedDateDatePicker.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }
        if (!isCategoryItem && !withoutStock && quantityInput.getText().isEmpty()) {
            quantityInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }
        if (!isCategoryItem && !withoutStock && requiredQuantityInput.getText().isEmpty()) {
            requiredQuantityInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }
        if (!isCategoryItem && !withoutStock && unitInput.getText().isEmpty()) {
            unitInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }
        if (!isCategoryItem && !withoutStock && priceInput.getText().isEmpty()) {
            priceInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }
        return isErrorFree;
    }

    private void clearFormWarnings() {
        nameInput.setStyle("-fx-border-color: null");
        clearInitialStockFormWarnings();
    }

    private void clearInitialStockFormWarnings() {
        receivedDateDatePicker.setStyle("-fx-border-color: null");
        quantityInput.setStyle("-fx-border-color: null");
        requiredQuantityInput.setStyle("-fx-border-color: null");
        unitInput.setStyle("-fx-border-color: null");
        priceInput.setStyle("-fx-border-color: null");
    }

    private void prepareNewItem() {
        itemOperationType = ItemOperationType.LEVEL;
        parentItemText.setText("");
        addNewItemText.setText("Add New Item");
        receivedDateDatePicker.setValue(Utilities.toLocalDateFromUtilDate(new Date()));
        receivedDateDatePicker.setDisable(true);
        nameInput.setText("");
        associatedUnitInput.setText("");
        prepareEnableDisableInitialStock(true);
        saveButton.setVisible(false);
        createSubButton.setVisible(false);
        createTopButton.setVisible(false);
        createButton.setVisible(true);
        createButton.toFront();
    }

    private void prepareNewTopItem() {
        itemOperationType = ItemOperationType.TOP;
        parentItemText.setText("");
        addNewItemText.setText("Add New Top Item");
        receivedDateDatePicker.setValue(Utilities.toLocalDateFromUtilDate(new Date()));
        receivedDateDatePicker.setDisable(true);
        nameInput.setText("");
        associatedUnitInput.setText("");
        prepareEnableDisableInitialStock(true);
        saveButton.setVisible(false);
        createSubButton.setVisible(false);
        createButton.setVisible(false);
        createTopButton.setVisible(true);
        createTopButton.toFront();
    }

    private void prepareNewSubItem() {
        itemOperationType = ItemOperationType.SUB;
        Item parentItem = currentTreeItem.getValue();
        parentItemText.setText("");
        addNewItemText.setText("Add New Sub Item");
        receivedDateDatePicker.setValue(Utilities.toLocalDateFromUtilDate(new Date()));
        receivedDateDatePicker.setDisable(true);
        nameInput.setText(parentItem.getName());
        associatedUnitInput.setText(parentItem.getAssociatedUnit());
        prepareEnableDisableInitialStock(true);
        saveButton.setVisible(false);
        createButton.setVisible(false);
        createTopButton.setVisible(false);
        createSubButton.setVisible(true);
        createButton.toFront();
    }

    private void prepareEditItem() {
        itemOperationType = ItemOperationType.EDIT;
        parentItemText.setText("");
        addNewItemText.setText("Edit Item");
        prepareEnableDisableInitialStock(false);
        createButton.setVisible(false);
        createSubButton.setVisible(false);
        saveButton.setVisible(true);
        saveButton.toFront();
    }

    private void prepareEnableDisableInitialStock(boolean enable) {
        initialStockSeparator.setVisible(enable);
        initialStockText.setVisible(enable);
        receivedDateLabel.setVisible(enable);
        receivedDateDatePicker.setVisible(enable);
        initialStockQuantityLabel.setVisible(enable);
        quantityInput.setVisible(enable);
        initialStockRequiredQuantityLabel.setVisible(enable);
        requiredQuantityInput.setVisible(enable);
        initialStockUnitLabel.setVisible(enable);
        unitInput.setVisible(enable);
        initialStockPriceLabel.setVisible(enable);
        priceInput.setVisible(enable);
        remarkLabel.setVisible(enable);
        remarkInput.setVisible(enable);
        isOpeningStockCheckBox.setVisible(enable);
        isOpeningStockLabel.setVisible(enable);

        if (enable)
            GridPane.setRowIndex(saveEditButtonBar, 15);
        else
            GridPane.setRowIndex(saveEditButtonBar, 6);
    }

    private void loadNewItemGrid() {
        enableThisGrid(addNewItemGrid);
    }

    private void loadItemDetailsGrid() {
        enableThisGrid(showItemGrid);
    }

    private void loadDeleteItemGrid() {
        enableThisGrid(confirmDeleteItemGrid);
    }

    private void enableThisGrid(GridPane grid) {
        confirmDeleteItemGrid.setVisible(false);
        addNewItemGrid.setVisible(false);
        showItemGrid.setVisible(false);
        grid.setVisible(true);
        grid.toFront();
    }

    private List<Item> returnAllItems(Item item) {
        List<Item> listOfItems = new ArrayList<Item>();
        addAllItems(item, listOfItems);
        return listOfItems;
    }

    private void addAllItems(Item item, List<Item> listOfItems) {
        if (item != null) {
            listOfItems.add(item);
            List<Item> children = item.getChildItems();
            if (children != null) {
                for (Item child : children) {
                    addAllItems(child, listOfItems);
                }
            }
        }
    }

    private void loadItemWithInitialStockIntoShowGrid() {
        itemNameShowText.setText(currentTreeItem.getValue().getName());
        associatedUnitShowText.setText(currentTreeItem.getValue().getAssociatedUnit());
        descriptionShowText.setText(currentTreeItem.getValue().getDescription());
        if (currentTreeItem.getValue().getCategoryItem() == CategoryItemDesignation.YES)
            isCategoryShowText.setText("Yes");
        else
            isCategoryShowText.setText("No");
        if (currentTreeItem.getValue().getCategoryItem() == CategoryItemDesignation.NO && !currentTreeItem.getValue().getStocks().isEmpty()) {
            Stock stock = currentTreeItem.getValue().getStocks().get(0);
            receivedDateShowText.setText(Utilities.fromDate(stock.getReceivedDate()));
            quantityShowText.setText(stock.getQuantityOfReceivedStock() + "");
            requiredQuantityShowText.setText(stock.getRequiredStockLevel() + "");
            priceShowText.setText(stock.getPriceOfReceivedStock() + "");
        } else {
            receivedDateShowText.setText("");
            quantityShowText.setText("");
            requiredQuantityShowText.setText("");
            priceShowText.setText("");
        }
    }

    private void loadItemsWithStockIntoTable() {
        loadItemsWithStockIntoTable(currentTreeItem.getValue());
    }

    private void loadItemsWithStockIntoTable(Item providedItem) {
        List<Item> currentItemAndChildren = returnAllItems(providedItem);
        List<UniqueItemWithStock> itemsWithStock = new ArrayList<>();

        for (Item item : currentItemAndChildren) {
            if (item.getCategoryItem() == CategoryItemDesignation.NO) {
                UniqueItemWithStock uniqueItemWithStock = new UniqueItemWithStock();
                uniqueItemWithStock.setItem(item);
                itemsWithStock.add(uniqueItemWithStock);
            }
        }

        itemWithStockTableView.getItems().setAll(itemsWithStock);
    }

    private boolean isDeletable(Item item) {
        return (item.getChildItems() == null || item.getChildItems().isEmpty())
                || item.getCategoryItem() == CategoryItemDesignation.YES;
    }

    @ActionMethod("confirmDeleteItem")
    public void confirmDeleteItem() {
        Item currentItem = currentTreeItem.getValue();
        if (currentItem.getParentItem() != null)
            currentItem.getParentItem().getChildItems().remove(currentItem);
        ItemDao.deleteItem(currentTreeItem.getValue());
        currentTreeItem.getParent().getChildren().remove(currentTreeItem);
        itemToTreeItemMap.remove(currentTreeItem.getValue());
        loadNewItemForm();
    }

    @ActionMethod("cancelDeleteItem")
    public void cancelDeleteItem() {
        loadItemDetails();
    }
}
