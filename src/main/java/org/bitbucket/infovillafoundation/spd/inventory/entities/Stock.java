package org.bitbucket.infovillafoundation.spd.inventory.entities;

import org.bitbucket.infovillafoundation.spd.inventory.converters.OpenCloseStatusConverter;
import org.bitbucket.infovillafoundation.spd.inventory.models.OpenCloseStatus;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 15/12/14.
 */

@Entity
@Table(name = "stocks")
public class Stock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String unit;

    @Column(name = "required_stock_level")
    private double requiredStockLevel;

    @Column(name = "stock_received_date")
    @Temporal(TemporalType.DATE)
    private Date receivedDate;

    @Column(name = "received_stock_quantity")
    private double quantityOfReceivedStock;

    @Column(name = "received_stock_price")
    private double priceOfReceivedStock;

    @Column(name = "received_stock_value")
    private double valueOfReceivedStock;

    @Column(name = "issued_stock_quantity")
    private double quantityOfIssuedStock;

    @Column(name = "issued_stock_value")
    private double valueOfIssuedStock;

    @Column(name = "balanced_stock_quantity")
    private double quantityOfBalancedStock;

    @Column(name = "balanced_stock_value")
    private double valueOfBalancedStock;

    @Column(name = "open_close_status")
    @Convert(converter = OpenCloseStatusConverter.class)
    private OpenCloseStatus openCloseStatus;

    private String remark;

    @ManyToOne
    @JoinColumn(name = "item_id")
    private Item item;

    @OneToOne
    @JoinColumn(name = "active_stock_item_id")
    private Item activeStockOwner;

    @OneToMany
    @JoinColumn(name = "stock_id")
    private List<StockIssue> stockIssues;

    @OneToMany
    @JoinColumn(name = "stock_id")
    private List<StockReceipt> stockReceipts;

    public Stock() {
        openCloseStatus = OpenCloseStatus.OPEN;
        if (stockIssues == null)
            stockIssues = new ArrayList<>();
        if (stockReceipts == null)
            stockReceipts = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getRequiredStockLevel() {
        return requiredStockLevel;
    }

    public void setRequiredStockLevel(double requiredStockLevel) {
        this.requiredStockLevel = requiredStockLevel;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public double getQuantityOfReceivedStock() {
        return quantityOfReceivedStock;
    }

    public void setQuantityOfReceivedStock(double quantityOfReceivedStock) {
        this.quantityOfReceivedStock = quantityOfReceivedStock;
    }

    public double getPriceOfReceivedStock() {
        return priceOfReceivedStock;
    }

    public void setPriceOfReceivedStock(double priceOfReceivedStock) {
        this.priceOfReceivedStock = priceOfReceivedStock;
    }

    public double getValueOfReceivedStock() {
        return valueOfReceivedStock;
    }

    public void setValueOfReceivedStock(double valueOfReceivedStock) {
        this.valueOfReceivedStock = valueOfReceivedStock;
    }

    public double getQuantityOfIssuedStock() {
        return quantityOfIssuedStock;
    }

    public void setQuantityOfIssuedStock(double quantityOfIssuedStock) {
        this.quantityOfIssuedStock = quantityOfIssuedStock;
    }

    public double getValueOfIssuedStock() {
        return valueOfIssuedStock;
    }

    public void setValueOfIssuedStock(double valueOfIssuedStock) {
        this.valueOfIssuedStock = valueOfIssuedStock;
    }

    public double getQuantityOfBalancedStock() {
        return quantityOfBalancedStock;
    }

    public void setQuantityOfBalancedStock(double quantityOfBalancedStock) {
        this.quantityOfBalancedStock = quantityOfBalancedStock;
    }

    public double getValueOfBalancedStock() {
        return valueOfBalancedStock;
    }

    public void setValueOfBalancedStock(double valueOfBalancedStock) {
        this.valueOfBalancedStock = valueOfBalancedStock;
    }

    public OpenCloseStatus getOpenCloseStatus() {
        return openCloseStatus;
    }

    public void setOpenCloseStatus(OpenCloseStatus openCloseStatus) {
        this.openCloseStatus = openCloseStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Item getActiveStockOwner() {
        return activeStockOwner;
    }

    public void setActiveStockOwner(Item activeStockOwner) {
        this.activeStockOwner = activeStockOwner;
    }

    public List<StockIssue> getStockIssues() {
        return stockIssues;
    }

    public void setStockIssues(List<StockIssue> stockIssues) {
        this.stockIssues = stockIssues;
    }

    public List<StockReceipt> getStockReceipts() {
        return stockReceipts;
    }

    public void setStockReceipts(List<StockReceipt> stockReceipts) {
        this.stockReceipts = stockReceipts;
    }

    public boolean isStockLow() {
        return quantityOfBalancedStock < requiredStockLevel && openCloseStatus == OpenCloseStatus.OPEN;
    }
}
